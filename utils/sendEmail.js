let nodemailer = require("nodemailer");
const handlebars = require("handlebars");

const sendEmail = async (
  link,
  name,
  companyName,
  email,
  emailTemplateSource,
  body
) => {
  try {
    // compile email handlebars email template
    const template = handlebars.compile(emailTemplateSource);

    var mailConfig;

    if (process.env.NODE_ENV === "production") {
      // all emails are delivered to destination
      mailConfig = {
        host: "smtp.gmail.com",
        port: 587,
        auth: {
          user: process.env.EMAIL,
          pass: process.env.EMAIL_PASS,
        },
        connectionTimeout: 5 * 60 * 1000, // 5 min
      };
    } else {
      // Generate test SMTP service account from ethereal.email
      // Only needed if you don't have a real mail account for testing
      let testAccount = await nodemailer.createTestAccount();
      // create reusable transporter object using the default SMTP transport
      // all emails are catched by ethereal.email
      mailConfig = {
        host: "smtp.ethereal.email",
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
          user: testAccount.user, // generated ethereal user
          pass: testAccount.pass, // generated ethereal password
        },
      };
    }

    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport(mailConfig);

    // send mail with defined transport object
    let info = await transporter.sendMail({
      from: '"BrandExpress.ae" <team@brandexpress.ae>', // sender address
      to: email,
      subject: `Message From BrandExpress.ae`,
      html: template({
        name,
        link,
        companyName,
        body,
      }),
    });

    console.log("Message sent: %s", info.messageId);
    // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

    // Preview only available when sending through an Ethereal account
    console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
    // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...};

    return "success";
  } catch (err) {
    console.log(err);
    return "fail";
  }
};

export default sendEmail;
