import axios from "axios";

export const getCloudinaryUrl = async (image, userName) => {
  const url = "https://api.cloudinary.com/v1_1/brandexpress/image/upload";
  const formData = new FormData();

  var timestamp = Math.round(new Date().getTime() / 1000);
  let api_key = process.env.NEXT_PUBLIC_CLOUDINARY_KEY;
  let api_secret = process.env.NEXT_PUBLIC_CLOUDINARY_SECRET;

  const public_id = `users/${userName}`;

  // creating hashed signature from sent data and unique timestamp (order is important)
  let signature = `invalidate=true&public_id=${public_id}&timestamp=${timestamp}${api_secret}`;
  const encoder = new TextEncoder().encode(signature);

  let hashed_signature = await crypto.subtle.digest("SHA-1", encoder);
  const hashArray = Array.from(new Uint8Array(hashed_signature)); // convert buffer to byte array
  const hashHex = hashArray
    .map((b) => b.toString(16).padStart(2, "0"))
    .join("");

  // creating form data to send to cloudinary (order is important)
  formData.append("file", image);
  formData.append("api_key", api_key);
  formData.append("public_id", public_id);
  formData.append("timestamp", timestamp);
  formData.append("invalidate", true);
  formData.append("signature", hashHex);

  const cloudinary = await axios.post(url, formData);
  const imageUrl = cloudinary.data.secure_url;

  return imageUrl;
};
