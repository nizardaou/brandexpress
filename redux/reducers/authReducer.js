import {
  AUTHENTICATE,
  DEAUTHENTICATE,
  USER,
  PACKAGES,
  FEEDBACKS,
  BRANDS,
  UPDATE_USER,
  UPDATE_FEEDBACKS,
  SELECT_DIRECTION,
} from "../Types";
import { removeCookie, setCookie } from "../../utils/cookie";

const initialState = {
  token: null,
  user: null,
  isAuthenticated: null,
  loading: false,
  packages: null,
  feedbacks: null,
  brands: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case AUTHENTICATE:
      return Object.assign({}, state, {
        token: action.payload,
        isAuthenticated: true,
        loading: true,
      });
    case DEAUTHENTICATE:
      removeCookie("token");
      return Object.assign({}, state, {
        token: null,
        isAuthenticated: false,
        loading: false,
      });
    case USER:
      return Object.assign({}, state, { user: action.payload, loading: false });
    case PACKAGES:
      return {
        ...state,
        packages: action.payload,
      };
    case FEEDBACKS:
      return {
        ...state,
        feedbacks: action.payload,
      };
    case BRANDS:
      return {
        ...state,
        brands: action.payload,
      };
    case SELECT_DIRECTION:
      return {
        ...state,
        packages: action.payload,
      };
    case UPDATE_USER:
      setCookie("token", action.payload.token);
      return Object.assign({}, state, {
        token: action.payload.token,
        user: action.payload.updatedUser,
        loading: false,
      });
    case UPDATE_FEEDBACKS:
      return {
        ...state,
        packages: action.payload.packagesList,
        feedbacks: [...state.feedbacks, action.payload.feedback],
      };
    default:
      return state;
  }
};
