import Router from "next/router";
import axios from "axios";
import {
  AUTHENTICATE,
  DEAUTHENTICATE,
  USER,
  PACKAGES,
  FEEDBACKS,
  BRANDS,
  UPDATE_USER,
  UPDATE_FEEDBACKS,
  SELECT_DIRECTION,
} from "../Types";
import { setCookie } from "../../utils/cookie";

// gets token from the api and stores it in the redux store and in cookie
export const authenticate = ({ token }, type) => {
  if (type !== "login") {
    throw new Error("Wrong API call!");
  }
  return (dispatch) => {
    setCookie("token", token);
    dispatch({
      type: AUTHENTICATE,
      payload: token,
    });
  };
};

// gets the token from the cookie and saves it in the store
export const reauthenticate = (token) => {
  return (dispatch) => {
    dispatch({ type: AUTHENTICATE, payload: token });
  };
};

// send token to the backend to decode it and send user data back
export const getUser = ({ token }, type) => {
  if (type !== "profile") {
    throw new Error("Wrong API call!");
  }

  return (dispatch) => {
    axios
      .get("/api/user/details", {
        headers: {
          Authorization: "bearer " + token,
        },
      })
      .then((response) => {
        dispatch({ type: USER, payload: response.data });
      })
      .catch((error) => {
        console.log("error", error.response.data);
        switch (error.response.status) {
          case 401:
            Router.push("/");
            break;
          case 422:
            alert(error.response.data.errors.message);
            break;
          case 500:
            alert("Interval server error! Try again!");
          case 503:
            alert(error.response.data.errors.message);
            Router.push("/");
            break;
          default:
            alert(error.response.data.errors.message);
            Router.push("/");
            break;
        }
      });
  };
};

// send token to the backend to decode it and send user data back
export const getPackages = ({ token }, type) => {
  if (type !== "packages") {
    throw new Error("Wrong API call!");
  }
  return (dispatch) => {
    axios
      .get("/api/user/packages", {
        headers: {
          Authorization: "bearer " + token,
        },
      })
      .then((response) => {
        dispatch({ type: PACKAGES, payload: response.data });
      })
      .catch((error) => {
        console.log("error", error);
        switch (error.response.status) {
          case 401:
            Router.push("/");
            break;
          case 422:
            alert(error.response.data.errors.message);
            break;
          case 500:
            alert("Interval server error! Try again!");
          case 503:
            alert(error.response.data.errors.message);
            Router.push("/");
            break;
          default:
            alert(error.response.data.errors.message);
            Router.push("/");
            break;
        }
      });
  };
};

// send token to the backend to decode it and send user data back
export const getFeedbacks = ({ token, packageID }, type) => {
  if (type !== "feedbacks") {
    throw new Error("Wrong API call!");
  }
  return (dispatch) => {
    axios
      .get(`/api/user/feedbacks?packageID=${packageID}`, {
        headers: {
          Authorization: "bearer " + token,
        },
      })
      .then((response) => {
        dispatch({ type: FEEDBACKS, payload: response.data });
      })
      .catch((error) => {
        console.log("error", error);
        switch (error.response.status) {
          case 401:
            Router.push("/");
            break;
          case 422:
            alert(error.response.data.errors.message);
            break;
          case 500:
            alert("Interval server error! Try again!");
          case 503:
            alert(error.response.data.errors.message);
            Router.push("/");
            break;
          default:
            alert(error.response.data.errors.message);
            Router.push("/");
            break;
        }
      });
  };
};

// send token to the backend to decode it and send user data back
export const updateFeedbacks = ({ details, token }, type) => {
  if (type !== "update feedbacks") {
    console.log(type);
    throw new Error("Wrong API call!");
  }
  console.log("details", details);
  return (dispatch) => {
    axios
      .post(
        "/api/user/feedbacks",
        { details },
        {
          headers: {
            Authorization: "bearer " + token,
          },
        }
      )
      .then((response) => {
        console.log("response data", response.data);
        dispatch({ type: UPDATE_FEEDBACKS, payload: response.data });
      })
      .catch((error) => {
        console.log("error", error);
        switch (error.response.status) {
          case 401:
            Router.push("/");
            break;
          case 422:
            alert(error.response.data.message);
            break;
          case 500:
            alert("Internal server error! Try again!");
          case 503:
            alert(error.response.data.message);
            Router.push("/");
            break;
          default:
            alert(error.response.data.message);
            break;
        }
      });
  };
};

// set package direction
export const selectDirection = (
  { attachment, packageID, expressUserID, token },
  type
) => {
  if (type !== "select direction") {
    console.log(type);
    throw new Error("Wrong API call!");
  }

  return (dispatch) => {
    axios
      .post(
        "/api/user/direction",
        { attachment, packageID, expressUserID },
        {
          headers: {
            Authorization: "bearer " + token,
          },
        }
      )
      .then((response) => {
        dispatch({ type: SELECT_DIRECTION, payload: response.data });
      })
      .catch((error) => {
        console.log("error", error);
        switch (error.response.status) {
          case 401:
            Router.push("/");
            break;
          case 422:
            alert(error.response.data.message);
            break;
          case 500:
            alert("Internal server error! Try again!");
          case 503:
            alert(error.response.data.message);
            Router.push("/");
            break;
          default:
            alert(error.response.data.message);
            break;
        }
      });
  };
};

// send token to the backend to decode it and send user data back
export const getBrands = ({ token }, type) => {
  if (type !== "brands") {
    throw new Error("Wrong API call!");
  }
  return (dispatch) => {
    axios
      .get("/api/user/brands", {
        headers: {
          Authorization: "bearer " + token,
        },
      })
      .then((response) => {
        dispatch({ type: BRANDS, payload: response.data });
      })
      .catch((error) => {
        console.log("error", error);
        switch (error.response.status) {
          case 401:
            Router.push("/");
            break;
          case 422:
            alert(error.response.data.errors.message);
            break;
          case 500:
            alert("Interval server error! Try again!");
          case 503:
            alert(error.response.data.errors.message);
            Router.push("/");
            break;
          default:
            alert(error.response.data.errors.message);
            Router.push("/");
            break;
        }
      });
  };
};

// send new user details to api to update user record and user redux state
export const updateUser = ({ userDetails, token }, type) => {
  if (type !== "update user") {
    throw new Error("Wrong API call!");
  }
  return (dispatch) => {
    axios
      .post(
        "/api/user/update",
        { userDetails },
        {
          headers: {
            Authorization: "bearer " + token,
          },
        }
      )
      .then((response) => {
        console.log("response data", response.data);
        dispatch({ type: UPDATE_USER, payload: response.data });
      })
      .catch((error) => {
        console.log("error", error);
        switch (error.response.status) {
          case 401:
            Router.push("/");
            break;
          case 422:
            alert(error.response.data.message);
            break;
          case 500:
            alert("Internal server error! Try again!");
          case 503:
            alert(error.response.data.message);
            Router.push("/");
            break;
          default:
            alert(error.response.data.message);
            break;
        }
      });
  };
};
// remove token from cookies
export const deauthenticate = () => (dispatch) => {
  dispatch({
    type: DEAUTHENTICATE,
  });
};
export default {
  authenticate,
  deauthenticate,
  reauthenticate,
  getPackages,
  getUser,
};
