const companyCategories = [
  {
    id: 1,
    name: "E-commerce",
  },
  {
    id: 2,
    name: "Beauty",
  },
  {
    id: 3,
    name: "Corporate",
  },
  {
    id: 4,
    name: "Education",
  },
  {
    id: 5,
    name: "Entertainment",
  },
  {
    id: 6,
    name: "Environment",
  },
  {
    id: 7,
    name: "Food Industry",
  },
  {
    id: 8,
    name: "Gaming",
  },
  {
    id: 9,
    name: "Medicine",
  },
  {
    id: 10,
    name: "Retail",
  },
  {
    id: 11,
    name: "Services",
  },
  {
    id: 12,
    name: "Sports",
  },
  {
    id: 13,
    name: "Technology",
  },
  {
    id: 14,
    name: "Other",
  },
];

export default companyCategories;
