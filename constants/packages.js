//core
export const coreName = "Brand Foundation";
export const coreDescription =
  "This core package includes the basics of your branding needs";
export const coreImage =
  "https://res.cloudinary.com/brandexpress/image/upload/v1635510301/onboarding/packages/1_p6uyqn.jpg";
//add-on 1
export const addon1Name = "Branded Pitch Deck Template";
export const addon1Description =
  "For when you want to wow during your investor meetings";
export const addon1Image =
  "https://res.cloudinary.com/brandexpress/image/upload/v1635510301/onboarding/packages/2_iul0cf.jpg";
//add-on 2
export const addon2Name = "Branded Landing Page Template";
export const addon2Description =
  "Make your brand stand out with a landing page customized for your brand";
export const addon2Image =
  "https://res.cloudinary.com/brandexpress/image/upload/v1635510301/onboarding/packages/3_zjgxal.jpg";
//add-on 3
export const addon3Name = "Branded Social Media Assets & Posts Template";
export const addon3Description =
  "Create a seamless look and feel for your brand across social media";
export const addon3Image =
  "https://res.cloudinary.com/brandexpress/image/upload/v1635510301/onboarding/packages/4_ahc5ka.jpg";
//add-on 4
export const addon4Name = "Branded Shopify E-store Theme";
export const addon4Description =
  "Get your own branded Shopify e-store theme, customized as per your brand guidelines";
export const addon4Image =
  "https://res.cloudinary.com/brandexpress/image/upload/v1637054688/onboarding/packages/5_erprl5.jpg";
