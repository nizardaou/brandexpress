const companyMood = [
  {
    id: 1,
    name: "vintageModern",
    label1: "Vintage",
    label2: "Modern",
  },
  {
    id: 2,
    name: "simpleFancy",
    label1: "Simple",
    label2: "Fancy",
  },
  {
    id: 3,
    name: "playfulFormal",
    label1: "Playful",
    label2: "Formal",
  },
  {
    id: 4,
    name: "geometricScript",
    label1: "Geometric",
    label2: "Script",
  },
  {
    id: 5,
    name: "trendyClassic",
    label1: "Trendy",
    label2: "Classic",
  },
  {
    id: 6,
    name: "abstractPictorial",
    label1: "Abstract",
    label2: "Pictorial",
  },
];

export default companyMood;
