export const userEnums = {
  status: { pending: "PENDING", verified: "VERIFIED", deleted: "DELETED" },
};

export const paymentEnums = {
  status: {
    pending: "PENDING",
    completed: "COMPLETED",
    refunded: "REFUNDED",
    failed: "FAILED",
    canceled: "CANCELED",
  },
};

export const packageEnums = {
  coreStatus: {
    started: "STARTED",
    directionSubmitted: "VISUAL DIRECTION SUBMITTED",
    directionChosen: "VISUAL DIRECTION CHOSEN",
    feedbackSubmitted: "FEEDBACK SUBMITTED",
    artworkReady: "ARTWORK READY",
  },
  addonStatus: {
    pending: "PENDING",
    started: "STARTED",
    inProgress: "IN PROGRESS",
    ready: "READY",
  },
};
