export const colorPallets = [
  {
    id: "1",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429455/onboarding/colors/1_mgzxpa.jpg",
  },
  {
    id: "2",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429455/onboarding/colors/2_ssorgn.jpg",
  },
  {
    id: "3",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429455/onboarding/colors/3_ttsu4s.jpg",
  },
  {
    id: "4",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429455/onboarding/colors/4_ob5dxf.jpg",
  },
  {
    id: "5",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429455/onboarding/colors/5_o9jpnf.jpg",
  },
  {
    id: "6",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429455/onboarding/colors/6_giept4.jpg",
  },
  {
    id: "7",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429455/onboarding/colors/7_tbglaa.jpg",
  },
  {
    id: "8",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429455/onboarding/colors/8_w4gg6s.jpg",
  },
  {
    id: "9",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429455/onboarding/colors/9_iksqvj.jpg",
  },
  {
    id: "10",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429455/onboarding/colors/10_rmlkkk.jpg",
  },
  {
    id: "11",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429455/onboarding/colors/11_f8m0u6.jpg",
  },
  {
    id: "12",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429456/onboarding/colors/12_lepcyy.jpg",
  },
  {
    id: "13",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429456/onboarding/colors/13_zlltr5.jpg",
  },
  {
    id: "14",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429456/onboarding/colors/14_vuiakd.jpg",
  },
  {
    id: "15",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429456/onboarding/colors/15_purzjg.jpg",
  },
  {
    id: "16",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429456/onboarding/colors/16_vdsxp2.jpg",
  },
  {
    id: "17",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429456/onboarding/colors/17_bps8wg.jpg",
  },
  {
    id: "18",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429456/onboarding/colors/18_oxc6uj.jpg",
  },
  {
    id: "19",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429456/onboarding/colors/19_izgs9c.jpg",
  },
  {
    id: "20",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429456/onboarding/colors/20_hx54my.jpg",
  },
  {
    id: "21",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429456/onboarding/colors/21_fem3bh.jpg",
  },
  {
    id: "22",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429456/onboarding/colors/22_qow6hs.jpg",
  },
  {
    id: "23",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429457/onboarding/colors/23_bb7k67.jpg",
  },
  {
    id: "24",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429457/onboarding/colors/24_oocnz9.jpg",
  },
  {
    id: "25",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429457/onboarding/colors/25_wl8d4h.jpg",
  },
  {
    id: "26",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429457/onboarding/colors/26_adeq6d.jpg",
  },
  {
    id: "27",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429457/onboarding/colors/27_wxjcg3.jpg",
  },
  {
    id: "28",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429457/onboarding/colors/28_okzhvn.jpg",
  },
  {
    id: "29",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429457/onboarding/colors/29_kcenjn.jpg",
  },
  {
    id: "30",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429457/onboarding/colors/30_ro3tot.jpg",
  },
  {
    id: "31",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429457/onboarding/colors/31_ozdkuf.jpg",
  },
  {
    id: "32",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429457/onboarding/colors/32_e2pxtn.jpg",
  },
  {
    id: "33",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429457/onboarding/colors/33_eq0vdy.jpg",
  },
  {
    id: "34",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429458/onboarding/colors/34_vtfcut.jpg",
  },
  {
    id: "35",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429457/onboarding/colors/35_rfbz59.jpg",
  },
  {
    id: "36",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429458/onboarding/colors/36_jknuk6.jpg",
  },
  {
    id: "37",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429458/onboarding/colors/37_ftyp14.jpg",
  },
  {
    id: "38",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429458/onboarding/colors/38_mppvhy.jpg",
  },
  {
    id: "39",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429458/onboarding/colors/39_endhme.jpg",
  },
  {
    id: "40",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429458/onboarding/colors/40_in60sc.jpg",
  },
  {
    id: "41",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429458/onboarding/colors/41_tkunfu.jpg",
  },
  {
    id: "42",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429458/onboarding/colors/42_cxfkbg.jpg",
  },
  {
    id: "43",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429458/onboarding/colors/43_s4iqtr.jpg",
  },
  {
    id: "44",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429458/onboarding/colors/44_ltp3a6.jpg",
  },
  {
    id: "45",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429458/onboarding/colors/45_xuclhi.jpg",
  },
  {
    id: "46",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429459/onboarding/colors/46_h8bjgu.jpg",
  },
  {
    id: "47",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429459/onboarding/colors/47_ik3krf.jpg",
  },
  {
    id: "48",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429459/onboarding/colors/48_os5jle.jpg",
  },
  {
    id: "49",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429459/onboarding/colors/49_do6zmr.jpg",
  },
  {
    id: "50",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429459/onboarding/colors/50_tc9z6y.jpg",
  },
  {
    id: "51",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429459/onboarding/colors/51_fxh46a.jpg",
  },
  {
    id: "52",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429459/onboarding/colors/52_jvynbm.jpg",
  },
  {
    id: "53",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637429459/onboarding/colors/53_r95l6q.jpg",
  },
];
