import { useState, createContext, useContext, useEffect } from "react";

export const FormContext = createContext();

export default function FormProvider({ children }) {
  const [data, setData] = useState({});
  const [submitCounter, setSubmitCounter] = useState(0);

  const setFormValues = (values) => {
    setData((prevValues) => ({
      ...prevValues,
      ...values,
    }));
  };

  const deleteFormValues = (value1, value2) => {
    let newState = { ...data };
    delete newState[value1];
    delete newState[value2];

    setData(newState);
  };

  return (
    <FormContext.Provider
      value={{
        data,
        setData,
        submitCounter,
        setSubmitCounter,
        setFormValues,
        deleteFormValues,
      }}
    >
      {children}
    </FormContext.Provider>
  );
}

export const useFormData = () => useContext(FormContext);
