// tailwind.config.js
module.exports = {
  purge: ["./pages/**/*.{js,ts,jsx,tsx}", "./components/**/*.{js,ts,jsx,tsx}"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      fontFamily: {
        Poppins: ["Poppins", "sans-serif"],
      },
      fontSize: {
        banner: ["13px", "20px"],
      },
      textColor: {
        "BE-pink": "#FF13EB",
        "BE-blue": "#4FC6FA",
        "BE-yellow": "#FFC416",
        "BE-purple": "#8835FD",
      },
      borderColor: {
        "BE-pink": "#FF13EB",
        "BE-blue": "#4FC6FA",
        "BE-yellow": "#FFC416",
        "BE-purple": "#8835FD",
      },
      ringColor: {
        "BE-purple": "#8835FD",
      },
      boxShadow: {
        inverse:
          "0 -1px 3px 0 rgba(0, 0, 0, 0.1), 0 -1px 2px 0 rgba(0, 0, 0, 0.06)",
      },
      backgroundColor: (theme) => ({
        ...theme("colors"),
        "BE-purple": "#8835FD",
        "BE-pink": "#FF13EB",
      }),
    },
  },
  variants: {
    extend: {},
  },
  plugins: [require("@tailwindcss/forms")],
};
