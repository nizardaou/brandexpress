import { useState } from "react";
import { PlusIcon, XIcon } from "@heroicons/react/outline";
import axios from "axios";
import joinCssClassNames from "../utils/joinCssClassNames";

//component to render results
const SearchPreview = ({
  item,
  type,
  index,
  updateText,
  updateField,
  keyword,
  modify,
}) => {
  const [attachment, setAttachment] = useState({
    link: "",
    description: "",
    type: "direction",
    selected: false,
  });
  const handleSubmit = async (e) => {
    e.preventDefault();
    // add link to package
    await axios.post("/api/admin/attachments/add", {
      packageID: item._id,
      attachment,
    });
    // get new list of packages and update results state
    const newResults = await axios.get(
      `/api/admin/packages?package=${keyword}`
    );

    updateField("results", newResults.data, false);
  };

  const changeStatus = async (packageID, stepName, status) => {
    // add link to package
    await axios.post("/api/admin/timelinestatus", {
      packageID,
      stepName,
      status,
    });

    // get new list of packages and update results state
    const newResults = await axios.get(
      `/api/admin/packages?package=${keyword}`
    );

    updateField("results", newResults.data, false);
  };

  const deleteAttachment = async (attachment, packageID) => {
    await axios.post("/api/admin/attachments/delete", {
      attachment,
      packageID,
    });

    // get new list of packages and update results state
    const newResults = await axios.get(
      `/api/admin/packages?package=${keyword}`
    );

    updateField("results", newResults.data, false);
  };

  return (
    <li
      onClick={() => (type === "packages" ? "" : updateText(item))}
      key={index}
      className={joinCssClassNames(
        type === "packages" ? "" : "cursor-pointer",
        "py-5"
      )}
    >
      <div className="relative focus-within:ring-2 focus-within:ring-indigo-500">
        <h3 className="text-sm font-semibold text-gray-800">
          {/* Extend touch target to entire panel */}
          <span className="absolute inset-0" aria-hidden="true" />
          {type === "packages" ? item.name : item}
        </h3>
        {type === "packages" && (
          <p className="mt-1 text-sm text-gray-600 line-clamp-2">
            status: {item.status}
          </p>
        )}
      </div>
      {type === "packages" && modify === "attachments" && (
        <form onSubmit={handleSubmit}>
          <div className="mt-1 relative flex flex-col items-center">
            <input
              onChange={(e) => {
                e.persist();
                setAttachment((prevValue) => {
                  return {
                    ...prevValue,
                    link: e.target.value,
                  };
                });
              }}
              type="text"
              name="attachment"
              required
              placeholder="add attachment link"
              className="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full pr-12 sm:text-sm border-gray-300 rounded-md"
            />
            <select
              name="type"
              id="type"
              className="my-1 block w-full pl-3 pr-10 py-2 text-base border-gray-300 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm rounded-md"
              defaultValue="direction"
              onChange={(e) => {
                e.persist();
                setAttachment((prevValue) => {
                  return {
                    ...prevValue,
                    type: e.target.value,
                  };
                });
              }}
            >
              <option value="direction">Direction</option>
              <option value="final">Final</option>
            </select>
            <input
              onChange={(e) => {
                e.persist();
                setAttachment((prevValue) => {
                  return {
                    ...prevValue,
                    description: e.target.value,
                  };
                });
              }}
              type="text"
              name="description"
              required
              placeholder="add attachment description"
              className="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full pr-12 sm:text-sm border-gray-300 rounded-md"
            />

            <div className="absolute right-0 bottom-0 flex py-1.5 pr-1.5">
              <button
                type="submit"
                className="inline-flex items-center rounded px-2 text-sm font-sans font-medium"
              >
                <PlusIcon className="h-5 w-5 text-BE-purple" />
              </button>
            </div>
          </div>
        </form>
      )}
      {type === "packages" &&
        modify === "attachments" &&
        item.attachments &&
        item.attachments.length > 0 && (
          <ul className="text-BE-purple px-2  pl-2 mt-2">
            {item.attachments.map((attachment, index) => (
              <div className="flex flex-row space-x-4 items-center">
                <pre>{index + 1 + "-"}</pre>
                <p className="flex-grow">link: {attachment.link}</p>
                <p className="flex-grow">
                  description: {attachment.description}
                </p>
                <button
                  className="py-1 cursor-pointer"
                  onClick={() => deleteAttachment(attachment, item._id)}
                >
                  <XIcon className="h-5 w-5 text-BE-purple" />
                </button>
              </div>
            ))}
          </ul>
        )}

      {type === "packages" &&
        modify === "steps" &&
        item.steps &&
        item.steps.length > 0 && (
          <ul className="text-BE-purple list-inside pl-2 mt-2">
            {item.steps.map((step, index) => (
              <li key={index} className="my-2">
                <div className="flex flex-row space-x-4">
                  <p> {step.type}</p>
                  <button
                    onClick={() => {
                      changeStatus(item._id, step.type, !step.status);
                    }}
                    className={joinCssClassNames(
                      step.status ? "bg-green-500" : "bg-red-500",
                      "w-1/4 py-1 text-white"
                    )}
                  >
                    change status{" "}
                  </button>
                </div>
              </li>
            ))}
          </ul>
        )}
    </li>
  );
};

export default SearchPreview;
