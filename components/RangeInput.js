import { Range } from "react-range";
import { useEffect, useState } from "react";
import { useFormData } from "../context";

export default function RangeInput({
  input: { onChange, value },
  select,
  label1,
  label2,
}) {
  const [values, setValues] = useState([0]);
  const { data } = useFormData();

  useEffect(() => {
    if (data.moods && data.moods[select]) setValues(data.moods[select]);
  }, []);

  useEffect(() => {
    onChange(values);
  }, [values]);

  return (
    <Range
      step={1}
      min={-100}
      max={100}
      values={values}
      onChange={(values) => {
        setValues(values);
      }}
      renderTrack={({ props, children }) => (
        <div className="grid grid-cols-12 items-center">
          <p className="col-span-3 lg:col-span-1 text-left text-sm">{label1}</p>
          <div
            {...props}
            className="col-span-6 lg:col-span-10 h-3 pr-2 my-4 mx-5 bg-gray-100 rounded-md"
          >
            {children}
          </div>
          <p className="col-span-3 lg:col-span-1 text-left text-sm">{label2}</p>
        </div>
      )}
      renderThumb={({ props }) => (
        <div
          {...props}
          className="w-5 h-5 transform translate-x-10 bg-BE-purple rounded-full focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
        />
      )}
    />
  );
}
