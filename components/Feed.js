import {
  CheckIcon,
  ClockIcon,
  ThumbUpIcon,
  DownloadIcon,
} from "@heroicons/react/solid";
import { useEffect, useState } from "react";
import { packageEnums } from "../constants/enum";
import joinCssClassNames from "../utils/joinCssClassNames";

export default function Feed({ result }) {
  const [disabled, setDisabled] = useState(true);

  useEffect(() => {
    if (result && result.steps)
      result.steps.forEach((item) => {
        if (
          (item.status && item.type === packageEnums.coreStatus.artworkReady) ||
          item.type === packageEnums.addonStatus.ready
        )
          setDisabled(false);
      });
  }, [result]);

  function getIcon(name, status) {
    if (name === packageEnums.addonStatus.pending) {
      return (
        <ThumbUpIcon className="w-5 h-5 text-BE-purple" aria-hidden="true" />
      );
    } else if (
      name === packageEnums.coreStatus.started ||
      name === packageEnums.addonStatus.started
    ) {
      return (
        <CheckIcon className="w-5 h-5 text-BE-purple" aria-hidden="true" />
      );
    } else if (
      name === packageEnums.coreStatus.directionSubmitted ||
      name === packageEnums.coreStatus.directionChosen ||
      name === packageEnums.coreStatus.feedbackSubmitted ||
      name === packageEnums.addonStatus.inProgress
    ) {
      return (
        <ClockIcon className="w-5 h-5 text-BE-purple" aria-hidden="true" />
      );
    } else if (
      name === packageEnums.coreStatus.artworkReady ||
      name === packageEnums.addonStatus.ready
    ) {
      return (
        <DownloadIcon className="w-5 h-5 text-BE-purple" aria-hidden="true" />
      );
    }
  }

  return (
    <section
      aria-labelledby="timeline-title"
      className="lg:col-start-3 lg:col-span-1"
    >
      <div className="bg-white px-4 py-5 shadow sm:rounded-lg sm:px-6">
        <h2 id="timeline-title" className="text-lg font-medium text-gray-900">
          Timeline
        </h2>

        {/* Activity Feed */}
        <div className="mt-6 flow-root">
          <ul role="list" className="-mb-8">
            {result.steps &&
              result.steps.length > 0 &&
              result.steps.map((item, itemIdx) => {
                return (
                  <li
                    key={itemIdx}
                    className={joinCssClassNames(
                      item.status ? "opacity-100" : "opacity-50"
                    )}
                  >
                    <div className="relative pb-8">
                      {itemIdx !== result.steps.length - 1 ? (
                        <span
                          className="absolute top-4 left-4 -ml-px h-full w-0.5 bg-gray-200"
                          aria-hidden="true"
                        />
                      ) : null}
                      <div className="relative flex space-x-3">
                        <div>
                          <span
                            className={joinCssClassNames(
                              "h-8 w-8 rounded-full flex items-center justify-center ring-4 ring-BE-purple"
                            )}
                          >
                            {getIcon(item.type)}
                          </span>
                        </div>
                        <div className="min-w-0 flex-1 pt-1.5 flex justify-between space-x-4">
                          <div>
                            <p className="text-sm text-gray-500">{item.type}</p>
                          </div>
                          <div className="text-right text-sm whitespace-nowrap text-gray-500">
                            <p>{item.date}</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                );
              })}
          </ul>
        </div>
        <div className="mt-6 flex flex-col justify-stretch">
          <a
            download="Final Assets"
            href="/packages/1.zip"
            disabled={disabled}
            type="button"
            className={joinCssClassNames(
              disabled
                ? "bg-opacity-50 cursor-not-allowed pointer-events-none"
                : "bg-opacity-100 hover:bg-opacity-90 cursor-pointer",
              "inline-flex items-center justify-center px-4 py-2 border border-transparent text-sm font-medium rounded-md shadow-sm text-white bg-BE-purple focus:outline-none"
            )}
          >
            Download
          </a>
        </div>
      </div>
    </section>
  );
}
