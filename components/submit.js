import { useEffect, useState } from "react";
import { useFormData } from "../context";
import { Payment } from "./forms";
import Modal from "./modal";
import SubmitBox from "./SubmitBox";
import Pagination from "./forms/pagination";

export default function Submit({ stepNumber, prevFormStep }) {
  const { data } = useFormData();
  const [open, setOpen] = useState(false);
  const [socials, setSocials] = useState([]);

  function openState(state) {
    setOpen(state);
  }

  useEffect(() => {
    console.log(data);
    var array = [];

    for (const x in data) {
      if (x.includes("socialUrl")) {
        // get the number from the string
        let number = x.match(/\d+/)[0];

        // create an object
        var item = {};
        // add urlName and selectName to object and push them to array
        item["urlName"] = data[`socialUrl${number}`];
        item["selectName"] = data[`socialSelect${number}`];

        array.push(item);
      }
    }

    console.log(array);
    // set socials to created array
    if (array.length > 0) {
      setSocials(array);
    } else setSocials(null);
  }, []);

  return (
    <>
      <div className="min-h-full py-16 sm:px-6 lg:px-8 w-5/6 mx-auto">
        <h1 className="block font-semibold text-4xl text-gray-700 mb-5">
          Your brief
        </h1>
        <div className="space-y-3 mb-24 grid grid-cols-1 lg:grid-cols-2 justify-center">
          <div>
            <p className="font-bold mt-3 text-BE-purple text-2xl">
              <span className="mr-2"> &#8226; </span> Brand Foundation
            </p>
            <div className="grid grid-cols-2 gap-4 border-l-2 border-gray-300 pl-5 pt-5 ml-1">
              <SubmitBox label="Brand Name:" value={data.name} />
              {data.slogan && <SubmitBox label="Slogan:" value={data.slogan} />}
              <SubmitBox
                label="Business Field:"
                value={data.businessCategory.name}
              />
              <SubmitBox label="Specific Field:" value={data.specificField} />
              <SubmitBox
                label="Brand Description:"
                value={data.companyPurpose}
                classNames="col-span-2"
              />
              <SubmitBox
                label="Pain Point:"
                value={data.painPoint}
                classNames="col-span-2"
              />
              <SubmitBox
                label="Big Idea:"
                value={data.bigIdea}
                classNames="col-span-2"
              />
              {data.currentIdentity && (
                <SubmitBox
                  label="Current Identity:"
                  value={data.currentIdentity}
                />
              )}
              {data.website && (
                <SubmitBox label="Website:" value={data.website} />
              )}
              {socials.length > 0 && (
                <SubmitBox
                  label="Social Presence:"
                  socialsArray={socials}
                  classNames="col-span-2"
                />
              )}

              <SubmitBox label="Logo Type:" value={data.style.name} />

              {data.territory && (
                <SubmitBox label="Logo Style:" value={data.territory.name} />
              )}
              <SubmitBox
                label="Color Mixture:"
                value={data.colorMixture.name}
              />
              {data.colorMixture.name === "One-colour Identity" ? (
                <SubmitBox
                  label="Identity Color:"
                  color={data.identityColor.color}
                />
              ) : null}
              {data.colorMixture.name === "Two-colour Identity" ? (
                <>
                  <SubmitBox
                    label="Two Color Primary:"
                    color={data.twoColorPrimary.color}
                  />
                  <SubmitBox
                    label="Two Color Accent:"
                    value={data.twoColorAccent.custom ? "choose for me" : null}
                    color={
                      data.twoColorAccent.custom
                        ? null
                        : data.twoColorAccent.color
                    }
                  />
                </>
              ) : null}
              {data.colorMixture.name === "Multi-colour Identity" && (
                <SubmitBox
                  label="Multi Color Identity:"
                  color={data.multiColorPrimary.color}
                />
              )}
            </div>
          </div>

          {data.packages.includes("2") && (
            <div>
              <p className="font-bold mt-5 flex text-BE-purple text-2xl">
                <span className="mr-2"> &#8226; </span> Branded Pitch Deck
                Template
              </p>
              <div className="border-l-2 border-gray-300 text-gray-400 pl-5 pt-5 ml-1">
                &#8212;
              </div>
            </div>
          )}
          {data.packages.includes("3") && (
            <div>
              <p className="font-bold mt-3 text-BE-purple text-2xl flex">
                <span className="mr-2"> &#8226; </span> Branded Landing Page
                Template
              </p>
              <div className="grid lg:auto-cols-auto lg:grid-flow-col items-center gap-2 border-l-2 border-gray-300 pl-5 pt-5 ml-1">
                {data.lptemplate &&
                  data.lptemplate.map((item, index) => (
                    <SubmitBox
                      key={index}
                      label={item}
                      checkIcon
                      classNames="shadow p-2 flex justify-center"
                    />
                  ))}
              </div>
            </div>
          )}
          {data.packages.includes("4") && (
            <div>
              <p className="font-bold mt-5 text-BE-purple text-2xl flex">
                <span className="mr-2"> &#8226; </span> Branded Social Media and
                Assets Template
              </p>
              <div className="border-l-2 border-gray-300 text-gray-400 pl-5 pt-5 ml-1">
                &#8212;
              </div>
            </div>
          )}
          {data.packages.includes("5") && (
            <div>
              <p className="font-bold mt-3 text-BE-purple text-2xl flex">
                <span className="mr-2"> &#8226; </span> Branded Shopify E-store
                Theme
              </p>
              <div className="grid grid-cols-2 gap-4 border-l-2 border-gray-300 pl-5 pt-5 ml-1">
                <SubmitBox
                  label="Business Model:"
                  valuesArray={data.shopifyBusinessModel}
                />
                <SubmitBox
                  label="Number of Brands:"
                  value={data.brandsNumber}
                />
                <SubmitBox
                  label="Number of Products:"
                  value={data.productsNumber}
                />
                <SubmitBox
                  label="Target Market:"
                  value={data.shopifyDescription}
                />
              </div>
            </div>
          )}
        </div>
        <Pagination
          stepNumber={stepNumber}
          confirmPayment={() => setOpen(true)}
          prevFormStep={prevFormStep}
        />
      </div>
      <Modal open={open} openState={openState}>
        <Payment />
      </Modal>
    </>
  );
}
