import { connect } from "react-redux";
import { useEffect } from "react";
import { getBrands, getPackages } from "../redux/actions/authActions";
import { bindActionCreators } from "redux";
import router from "next/router";

const BrandsList = (props) => {
  useEffect(() => {
    if (props.authentication.token) {
      props.getBrands({ token: props.authentication.token }, "brands");
      props.getPackages({ token: props.authentication.token }, "packages");
    }
  }, [props.authentication.token]);

  function upperCaseTrim(string) {
    return string.toUpperCase().replace(/\s+/g, "");
  }

  const getInitials = (name) => {
    let nameSplited = name.split(" ");

    const firstInitial = nameSplited[0].charAt(0);
    const secondInitial = nameSplited[1] ? nameSplited[1].charAt(0) : "";

    return `${firstInitial} ${secondInitial}`;
  };

  return (
    <>
      <ul
        role="list"
        className="grid grid-cols-2 gap-x-4 gap-y-8 sm:grid-cols-3 sm:gap-x-6 lg:grid-cols-4 xl:gap-x-8"
      >
        {props.authentication.brands &&
          props.authentication.brands.length > 0 &&
          props.authentication.brands.map((brand, index) => (
            <li
              onClick={() => {
                router.push({
                  pathname: "/dashboard/packages",
                  query: { name: brand },
                });
              }}
              key={index}
              className="relative cursor-pointer rounded-md shadow"
            >
              <div className="group h-48 flex w-full aspect-w-10 aspect-h-7 rounded-lg focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-offset-gray-100 focus-within:ring-indigo-500 overflow-hidden">
                <div className="m-auto px-10 py-9 rounded-full flex justify-center items-center bg-gray-100">
                  <p>{getInitials(brand)} </p>
                </div>
                <button
                  type="button"
                  className="absolute inset-0 focus:outline-none"
                >
                  <span className="sr-only">View details for {brand}</span>
                </button>
              </div>
              <p className="mt-2 pl-2 block text-lg font-bold text-BE-purple truncate pointer-events-none">
                {brand}
              </p>
              <ul className="list list-disc list-inside text-gray-600 pl-2 mb-3">
                {props.authentication.packages &&
                  props.authentication.packages.length > 0 &&
                  props.authentication.packages
                    .sort((a, b) =>
                      a.type > b.type ? -1 : b.type > a.type ? 1 : 0
                    )
                    .map((tier) => {
                      if (
                        upperCaseTrim(tier.brandName) === upperCaseTrim(brand)
                      )
                        return <li className="list-item">{tier.name} </li>;
                    })}
              </ul>
            </li>
          ))}
      </ul>
    </>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    getBrands: bindActionCreators(getBrands, dispatch),
    getPackages: bindActionCreators(getPackages, dispatch),
  };
};
const mapStateToProps = (state) => {
  return {
    authentication: state.authentication,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(BrandsList);
