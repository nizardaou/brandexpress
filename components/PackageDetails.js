import { PaperClipIcon } from "@heroicons/react/solid";
import { selectDirection, updateFeedbacks } from "../redux/actions/authActions";
import joinCssClassNames from "../utils/joinCssClassNames";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { useEffect, useState } from "react";
import Modal from "./modal";

const PackageDetails = ({
  result,
  user,
  selectDirection,
  updateFeedbacks,
  authentication,
}) => {
  const [userSelected, setUserSelected] = useState(false);
  const [selectedAttachment, setSelectedAttachmet] = useState({});
  const [open, setOpen] = useState(false);

  function openState(state) {
    setOpen(state);
  }
  useEffect(() => {
    if (!result || !result.attachments) return;
    let filteredAttachments =
      result.attachments.filter((item) => item.selected).length > 0;
    setUserSelected(filteredAttachments);
  }, [result]);

  const handleClick = (attachment, packageID, expressUserID) => {
    //calls api to change selected property of attachment to true
    selectDirection(
      {
        attachment,
        packageID,
        expressUserID,
      },
      "select direction"
    );
    let secondFeedback = {
      expressUserID: result.expressUserID,
      packageID: result._id,
      input: true,
      body: `Great! you have selected "${attachment.description}" would you like to add any feedback (make sure to include all details as you are entitled for one revision only)`,
      orientation: "left",
      // form with contain 2 buttons, one that allows user to submit a feedback, and one the proceeds that allows him to proceed with not feedback
      formButtons: [
        {
          id: 1,
          buttonText: "Proceed with no feedback",
          value: "proceed",
          buttonClass:
            "inline-flex items-center justify-center px-4 py-2 border border-transparent text-sm font-medium rounded-md shadow-sm text-white bg-BE-purple hover:bg-opacity-90 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500",
        },
        {
          id: 2,
          buttonText: "Submit Feedback",
          value: "submit",
          buttonClass:
            "inline-flex ml-auto items-center justify-center px-4 py-2 border border-transparent text-sm font-medium rounded-md shadow-sm text-white bg-BE-purple hover:bg-opacity-90 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500",
        },
      ],
      actionButtons: [],
    };
    updateFeedbacks(
      { details: secondFeedback, token: authentication.token },
      "update feedbacks"
    );

    setOpen(false);
  };
  return (
    <section aria-labelledby="applicant-information-title">
      <div className="bg-white shadow sm:rounded-lg">
        <div className="px-4 py-5 sm:px-6">
          <h2
            id="applicant-information-title"
            className="text-lg leading-6 font-medium text-gray-900"
          >
            Package Information
          </h2>
          <p className="mt-1 max-w-2xl text-sm text-gray-500">
            Personal details and application.
          </p>
        </div>
        <div className="border-t border-gray-200 px-4 py-5 sm:px-6">
          <dl className="grid grid-cols-1 gap-x-4 gap-y-8 sm:grid-cols-2">
            <div className="sm:col-span-1">
              <dt className="text-sm font-medium text-gray-500">
                Application for
              </dt>

              <dd className="mt-1 text-sm text-gray-900">{result.name}</dd>
            </div>
            <div className="sm:col-span-1">
              <dt className="text-sm font-medium text-gray-500">
                Email address
              </dt>
              {user && (
                <dd className="mt-1 text-sm text-gray-900">{user.email}</dd>
              )}
            </div>
            <div className="sm:col-span-1">
              <dt className="text-sm font-medium text-gray-500">
                Package price
              </dt>
              <dd className="mt-1 text-sm text-gray-900">999 AED</dd>
            </div>
            <div className="sm:col-span-1">
              <dt className="text-sm font-medium text-gray-500">Phone</dt>
              {user && (
                <dd className="mt-1 text-sm text-gray-900">{user.phone}</dd>
              )}
            </div>
            <div className="sm:col-span-2">
              <dt className="text-sm font-medium text-gray-500">About</dt>
              <dd className="mt-1 text-sm text-gray-900">
                {result.description}
              </dd>
            </div>
            <div className="sm:col-span-2">
              <dt className="text-sm font-medium text-gray-500">Attachments</dt>
              <dd className="mt-1 text-sm text-gray-900">
                {result.attachments && result.attachments.length > 0 ? (
                  <ul
                    role="list"
                    className="border border-gray-200 rounded-md divide-y divide-gray-200"
                  >
                    {result.attachments.map((attachment, index) => {
                      let notSelected =
                        attachment.type === "direction" &&
                        !attachment.selected &&
                        userSelected;

                      return (
                        <li
                          key={index}
                          className={joinCssClassNames(
                            notSelected ? "opacity-50" : "opacity-100",
                            "pl-3 pr-4 py-3 flex items-center justify-between text-sm"
                          )}
                        >
                          <div className="w-0 flex-1 flex items-center">
                            <PaperClipIcon
                              className="flex-shrink-0 h-5 w-5 text-gray-400"
                              aria-hidden="true"
                            />
                            <span className="ml-2 flex-1 w-0 truncate">
                              {attachment.description}
                            </span>
                          </div>
                          <div className="ml-4 flex-shrink-0">
                            <button
                              onClick={() => {
                                setSelectedAttachmet(attachment);
                                setOpen(true);
                              }}
                              disabled={userSelected}
                              className={joinCssClassNames(
                                userSelected
                                  ? "cursor-not-allowed"
                                  : "cursor-pointer",
                                "mx-5 text-blue-600 hover:text-blue-500"
                              )}
                            >
                              select this direction
                            </button>
                            <a
                              download={attachment.description}
                              href={attachment.link}
                              className="font-medium text-blue-600 hover:text-blue-500"
                            >
                              Preview
                            </a>
                          </div>
                        </li>
                      );
                    })}
                  </ul>
                ) : (
                  <p className="text-center">Nothing is ready yet</p>
                )}
              </dd>
            </div>
          </dl>
        </div>
      </div>
      <Modal open={open} openState={openState}>
        <div className="flex flex-col justify-center items-center">
          <p className="w-3/4">
            {`are you sure you want to choose ${selectedAttachment.description} ?`}
          </p>
          <button
            className="bg-BE-purple rounded-md text-white w-1/2 mt-5"
            onClick={() =>
              handleClick(selectedAttachment, result._id, result.expressUserID)
            }
          >
            Yes
          </button>
        </div>
      </Modal>
    </section>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    selectDirection: bindActionCreators(selectDirection, dispatch),
    updateFeedbacks: bindActionCreators(updateFeedbacks, dispatch),
  };
};

const mapStateToProps = (state) => {
  return {
    authentication: state.authentication,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PackageDetails);
