import joinCssClassNames from "../utils/joinCssClassNames";
import { updateFeedbacks } from "../redux/actions/authActions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { useState } from "react";
import moment from "moment";
const Feedback = ({
  feedback,
  updateFeedbacks,
  expressUserID,
  packageID,
  token,
  disabled,
}) => {
  const [body, setBody] = useState("");
  const [userAction, setUserAction] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    let thirdFeedback = {
      expressUserID: expressUserID,
      packageID: packageID,
      input: false,
      body: "thank you will get to you as soon as possible",
      orientation: "left",
      formButtons: [],
      actionButtons: [],
      userInput: { body, userAction },
    };

    updateFeedbacks({ details: thirdFeedback, token }, "update feedbacks");
  };

  const getInitials = (name) => {
    let nameSplited = name.split(" ");

    const firstInitial = nameSplited[0].charAt(0);
    const secondInitial = nameSplited[1] ? nameSplited[1].charAt(0) : "";

    return `${firstInitial}  ${secondInitial}`;
  };

  const getDateDiff = (date) => {
    var now = moment(new Date()); //todays date
    var end = date; // another date
    var duration = moment.duration(now.diff(end));
    var days = duration.asDays();
    let daysInteger = Math.round(days);
    if (daysInteger === 0) {
      return "today";
    } else if (daysInteger === 1) {
      return "yesterday";
    } else return `${daysInteger} days ago`;
  };
  return (
    <li>
      <div className="flex space-x-3">
        {feedback.orientation === "left" && feedback.expressUserID && (
          <div className="flex-shrink-0">
            {feedback.expressUserID.imageUrl ? (
              <img
                src={feedback.expressUserID.imageUrl}
                className="h-8 w-8 rounded-full bg-BE-purple text-white font-Poppins flex items-center justify-center"
              />
            ) : (
              <div className="h-8 w-8 p-1 rounded-full bg-BE-purple text-white font-Poppins flex items-center justify-center">
                {getInitials("Brand Express")}
              </div>
            )}
          </div>
        )}
        <div className="w-full">
          <div
            className={joinCssClassNames(
              feedback.orientation === "right" ? "text-right" : "text-left",
              "text-sm"
            )}
          >
            <p className="font-medium text-gray-900">Brand Express Team</p>
          </div>
          {/* if feebdack is an input show body on top */}
          {feedback.input && (
            <span
              className={joinCssClassNames(
                feedback.orientation === "right" ? "text-right" : "text-left",
                "text-gray-500 font-medium flex-grow"
              )}
            >
              {feedback.body}
            </span>
          )}
          {/* if feebdack type is input => show text area, else show body */}
          {feedback.input ? (
            <form onSubmit={handleSubmit} className="mt-1">
              <div>
                <label htmlFor="body" className="sr-only">
                  Feedback
                </label>
                <textarea
                  id="body"
                  name="body"
                  rows={3}
                  onChange={(e) => setBody(e.target.value)}
                  className="shadow-sm block w-full focus:ring-blue-500 focus:border-blue-500 sm:text-sm border border-gray-300 rounded-md"
                  placeholder="Add a note"
                  defaultValue={""}
                />
              </div>
              {feedback.formButtons && feedback.formButtons.length > 0 && (
                <div
                  className={joinCssClassNames(
                    feedback.orientation === "right"
                      ? "justify-start"
                      : "justify-end",
                    "flex mt-5"
                  )}
                >
                  {feedback.formButtons.map((button, index) => {
                    return (
                      <button
                        name="action"
                        type="submit"
                        key={index}
                        disabled={disabled}
                        className={joinCssClassNames(
                          disabled ? "cursor-not-allowed" : "cursor-pointer",
                          button.buttonClass
                        )}
                        onClick={() => {
                          setUserAction(button.value);
                        }}
                      >
                        {button.buttonText}
                      </button>
                    );
                  })}
                </div>
              )}
            </form>
          ) : (
            <div
              className={joinCssClassNames(
                feedback.orientation === "right" ? "text-right" : "text-left",
                "mt-1 text-sm text-gray-700"
              )}
            >
              <p>{feedback.body}</p>
            </div>
          )}
          <div
            className={joinCssClassNames(
              feedback.orientation === "right" ? "flex-row-reverse" : "",
              "mt-2 text-sm space-x-2 flex "
            )}
          >
            {!feedback.input && (
              <span
                className={joinCssClassNames(
                  feedback.orientation === "right" ? "text-right" : "text-left",
                  "text-gray-500 font-medium flex-grow"
                )}
              >
                {getDateDiff(feedback.date)}
              </span>
            )}
          </div>
        </div>
        {feedback.orientation === "right" && feedback.expressUserID && (
          <div className="flex-shrink-0">
            {feedback.expressUserID.imageUrl ? (
              <img
                src={feedback.expressUserID.imageUrl}
                className="h-8 w-8 rounded-full bg-BE-purple text-white font-Poppins flex items-center justify-center"
              />
            ) : (
              <div className="h-8 w-8 rounded-full bg-BE-purple text-white font-Poppins flex items-center justify-center">
                {getInitials("Brand Express")}
              </div>
            )}
          </div>
        )}
      </div>
      {/* map through an array of buttons in the feedback */}
      <div className="flex space-x-4 mt-3">
        {feedback.actionButtons &&
          feedback.actionButtons.map((button, index) => {
            return (
              <button key={index} className={button.buttonClass}>
                {button.buttonText}
              </button>
            );
          })}
      </div>
    </li>
  );
};
const mapDispatchToProps = (dispatch) => {
  return {
    updateFeedbacks: bindActionCreators(updateFeedbacks, dispatch),
  };
};

export default connect(null, mapDispatchToProps)(Feedback);
