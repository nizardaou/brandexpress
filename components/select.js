import { Fragment, useEffect, useState } from "react";
import { Listbox, Transition } from "@headlessui/react";
import { CheckIcon, SelectorIcon } from "@heroicons/react/solid";
import { useFormData } from "../context";
import joinCssClassNames from "../utils/joinCssClassNames";

const Select = ({
  input: { onChange, value },
  label,
  options,
  select,
  changeDirection,
  ...rest
}) => {
  const [selected, setSelected] = useState({});
  const { data } = useFormData();

  useEffect(() => {
    if (!select) return;
    if (select === "colorMixture") {
      setSelected(options[0]);
    } else {
      setSelected(options[4]);
    }
  }, [select]);

  useEffect(() => {
    onChange(selected);

    if (select === "colorMixture") {
      changeDirection(selected.name);
    }
  }, [selected]);

  useEffect(() => {
    if (data[select]) setSelected(data[select]);
  }, [data]);

  return (
    <Listbox value={selected} onChange={setSelected}>
      {({ open }) => (
        <>
          <div className="mt-1 relative">
            <Listbox.Button className="bg-white relative w-full border border-gray-300 rounded-md shadow-sm pl-3 pr-10 py-2 text-left cursor-default focus:outline-none sm:text-sm">
              <span className="block truncate">{selected.name}</span>
              <span className="absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
                <SelectorIcon
                  className="h-5 w-5 text-gray-400"
                  aria-hidden="true"
                />
              </span>
            </Listbox.Button>

            <Transition
              show={open}
              as={Fragment}
              leave="transition ease-in duration-100"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <Listbox.Options className="mt-1 w-full bg-white shadow-lg max-h-60 overflow-y-scroll rounded-md py-1 text-base ring-1 ring-black ring-opacity-5  focus:outline-none sm:text-sm">
                {options.map((option) => (
                  <Listbox.Option
                    key={option.id}
                    className={({ active }) =>
                      joinCssClassNames(
                        active ? "text-white bg-BE-purple" : "text-gray-900",
                        "cursor-default select-none relative py-2 pl-3 pr-9"
                      )
                    }
                    value={option}
                  >
                    {({ selected, active }) => (
                      <>
                        <span
                          className={joinCssClassNames(
                            selected ? "font-semibold" : "font-normal",
                            "block truncate"
                          )}
                        >
                          {option.name}
                        </span>

                        {selected ? (
                          <span
                            className={joinCssClassNames(
                              active ? "text-white" : "text-BE-purple",
                              "absolute inset-y-0 right-0 flex items-center pr-4"
                            )}
                          >
                            <CheckIcon className="h-5 w-5" aria-hidden="true" />
                          </span>
                        ) : null}
                      </>
                    )}
                  </Listbox.Option>
                ))}
              </Listbox.Options>
            </Transition>
          </div>
        </>
      )}
    </Listbox>
  );
};

export default Select;
