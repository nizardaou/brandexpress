import { Fragment, useEffect, useState } from "react";
import { Transition, Popover } from "@headlessui/react";
import { connect } from "react-redux";

import { MenuIcon, XIcon } from "@heroicons/react/outline";
import Modal from "./modal";
import axios from "axios";
import router from "next/router";

const Nav = ({ isAuthenticated, pageTitle }) => {
  const [open, setOpen] = useState(false);

  function openState(state) {
    setOpen(state);
  }

  const [email, setEmail] = useState("");
  const [success, setSuccess] = useState(false);

  const handleSubmit = async (e) => {
    e.preventDefault();
    axios
      .post("/api/user/login", { email: email })
      .then((response) => {
        if (response.data === "success") setSuccess(true);
      })
      .catch((err) => alert("invalid credentials"));
  };
  return (
    <>
      <Popover className=" bg-white shadow sticky top-0 z-50 font-Poppins">
        <div className="w-5/6 lg:max-w-7xl mx-auto">
          <div className="flex justify-between items-center py-4 md:justify-start md:space-x-10">
            <div className="flex justify-start lg:w-0 lg:flex-1">
              <a href="/">
                <span className="sr-only">Brand Express</span>
                <img
                  className="h-16 sm:h-20"
                  src="https://res.cloudinary.com/brandexpress/image/upload/v1635503512/logos/BE-logo_br5nlz.png"
                  alt="Brand Express Logo"
                />
              </a>
            </div>
            {pageTitle !== "On Boarding" && (
              <div className="-mr-2 -my-2 md:hidden">
                <Popover.Button className="bg-white rounded-md p-2 inline-flex items-center justify-center text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none">
                  <span className="sr-only">Open menu</span>
                  <MenuIcon className="h-6 w-6" aria-hidden="true" />
                </Popover.Button>
              </div>
            )}

            <div className="hidden md:flex items-center justify-end md:flex-1 lg:w-0">
              {/* <button
                onClick={() => {
                  isAuthenticated ? router.push("/dashboard") : setOpen(true);
                }}
                className="ml-8 whitespace-nowrap inline-flex items-center justify-center px-4 py-2 border border-transparent text-base font-medium text-BE-purple hover:bg-opacity-90"
              >
                {isAuthenticated ? "Go to Dashboard" : "Login"}
              </button> */}

              {pageTitle !== "On Boarding" && (
                <a
                  href="/onboarding"
                  className="ml-8 whitespace-nowrap inline-flex items-center justify-center px-4 py-2 border border-transparent rounded-md shadow-sm text-base font-medium text-white bg-BE-purple hover:bg-opacity-90"
                >
                  Get Started
                </a>
              )}
            </div>
          </div>
        </div>

        <Transition
          as={Fragment}
          enter="duration-200 ease-out"
          enterFrom="opacity-0 scale-95"
          enterTo="opacity-100 scale-100"
          leave="duration-100 ease-in"
          leaveFrom="opacity-100 scale-100"
          leaveTo="opacity-0 scale-95"
        >
          <Popover.Panel
            focus
            className="absolute top-0 inset-x-0 z-10 p-2 transition transform origin-top-right md:hidden"
          >
            <div className="rounded-lg shadow-lg ring-1 ring-black ring-opacity-5 bg-white divide-y-2 divide-gray-50">
              <div className="pt-5 pb-6 px-5">
                <div className="flex items-center justify-between">
                  <div>
                    <img
                      className="h-10 sm:h-12 w-auto"
                      src="https://res.cloudinary.com/brandexpress/image/upload/v1635503512/logos/BE-logo_br5nlz.png"
                      alt="Brand Express Logo"
                    />
                  </div>
                  <div className="-mr-2">
                    <Popover.Button className="bg-white rounded-md p-2 inline-flex items-center justify-center text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none">
                      <span className="sr-only">Close menu</span>
                      <XIcon className="h-6 w-6" aria-hidden="true" />
                    </Popover.Button>
                  </div>
                </div>
              </div>
              <div className="py-6 px-5 space-y-6">
                <div>
                  {/* <button
                    onClick={() => {
                      isAuthenticated
                        ? router.push("/dashboard")
                        : setOpen(true);
                    }}
                    className="w-full flex items-center justify-center px-4 py-2 border border-transparent rounded-md shadow-sm text-base font-medium text-BE-purple hover:bg-opacity-90"
                  >
                    {isAuthenticated ? "Go to Dashboard" : "Login"}
                  </button> */}

                  {pageTitle !== "On Boarding" && (
                    <a
                      href="/onboarding"
                      className="w-full flex items-center justify-center px-4 py-2 border border-transparent rounded-md shadow-sm text-base font-medium text-white bg-BE-purple hover:bg-opacity-90"
                    >
                      Get Started
                    </a>
                  )}
                </div>
              </div>
            </div>
          </Popover.Panel>
        </Transition>
      </Popover>
      <Modal open={open} openState={openState}>
        {success ? (
          <div className="my-6">
            We have sent you an email to login to your dashboard.
          </div>
        ) : (
          <div className="px-4 py-5 sm:p-6">
            <h3 className="text-lg leading-6 font-medium text-gray-900">
              Login to your account
            </h3>
            <div className="mt-2 max-w-xl text-sm text-gray-500">
              <p>Enter the email address you associated with your account.</p>
            </div>
            <form
              onSubmit={handleSubmit}
              className="mt-5 sm:flex sm:items-center"
            >
              <div className="w-full sm:max-w-xs">
                <label htmlFor="email" className="sr-only">
                  Email
                </label>
                <input
                  type="text"
                  name="email"
                  id="email"
                  onChange={(e) => setEmail(e.target.value)}
                  className="shadow-sm block w-full sm:text-sm border-gray-300 rounded-md"
                  placeholder="you@example.com"
                />
              </div>
              <button
                type="submit"
                className="mt-3 w-full inline-flex items-center justify-center px-4 py-2 border border-transparent shadow-sm font-medium rounded-md text-white bg-BE-purple hover:bg-opacity-90 focus:outline-none sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm"
              >
                Submit
              </button>
            </form>
          </div>
        )}
      </Modal>
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.authentication.isAuthenticated,
  };
};

export default connect(mapStateToProps, null)(Nav);
