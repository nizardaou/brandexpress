import { useRouter } from "next/dist/client/router";
import { useState } from "react";

const NewsLetter = () => {
  const router = useRouter();
  const [query, setQuery] = useState("");

  const handleParam = (setValue) => (e) => setValue(e.target.value);

  const handleSubmit = (e) => {
    e.preventDefault();
    router.push({
      pathname: "/onboarding",
      query: { brand: query },
    });
  };

  return (
    <div className="bg-gray-800 py-4 lg:py-0 fixed bottom-0 w-full z-10 font-Poppins">
      <div className="w-5/6 lg:max-w-7xl relative mx-auto py-2 lg:py-5 flex items-center">
        <div className="lg:w-0 hidden lg:block flex-1">
          <h2 className="text-3xl font-extrabold tracking-tight text-white lg:text-4xl">
            build your brand now
          </h2>
        </div>
        <div className=" lg:ml-8 w-full lg:w-5/12">
          <form onSubmit={handleSubmit} className="flex ">
            <label htmlFor="company" className="sr-only">
              Company Name
            </label>
            <input
              id="company"
              name="company"
              type="text"
              required
              value={query}
              onChange={handleParam(setQuery)}
              className="w-full focus:outline-none focus:ring-transparent focus:border-transparent px-3 lg:px-4 text-base py-3 border placeholder-gray-500 lg:max-w-lg rounded-l-md lg:rounded-md"
              placeholder="Your Brand Name"
            />
            <div className="lg:rounded-md shadow lg:ml-3 flex-shrink-0 w-5/12">
              <button
                type="submit"
                className="w-full flex items-center justify-center lg:px-3 xl:px-5 py-3 text-base border border-transparent font-medium rounded-r-md lg:rounded-md text-white bg-BE-purple hover:bg-opacity-90 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-indigo-500"
              >
                Get Started
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default NewsLetter;
