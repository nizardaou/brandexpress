const Hero = () => {
  return (
    <div className="relative bg-gray-50">
      <main className="lg:relative">
        <div className="mx-auto w-5/6 lg:max-w-7xl pt-16 pb-20 text-center lg:py-48 lg:text-left">
          <div className="pr-4 lg:w-1/2 sm:pr-8 xl:pr-16">
            <h1 className="text-4xl tracking-tight font-semibold text-gray-900 sm:text-5xl lg:text-6xl">
              <span className="block xl:inline">brand your startup</span>{" "}
              <span className="block  text-BE-purple xl:inline">
                for success
              </span>
            </h1>
            <p className="mt-3 max-w-md mx-auto text-lg text-gray-500 sm:text-xl md:mt-5 md:max-w-3xl">
              As a startup or small business, you have gone a long way to
              develop your idea and develop your business model. Now is the time
              to tell your story to the world through a compelling and fresh
              brand and to stand out from the crowd from the get-go. Our team
              wants to help you do just that, for prices that won’t break the
              bank.
            </p>
          </div>
        </div>
        <div className="relative w-full h-64 sm:h-72 md:h-96 lg:absolute lg:inset-y-0 lg:right-0 lg:w-1/2 lg:h-full">
          <img
            className="absolute inset-0 w-full h-full object-cover"
            src="https://res.cloudinary.com/brandexpress/image/upload/v1635503126/hero/hero_ya6hbx.jpg"
            alt=""
          />
        </div>
      </main>
    </div>
  );
};

export default Hero;
