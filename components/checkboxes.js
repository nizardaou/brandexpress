import { useEffect, useState } from "react";
import { useFormData } from "../context";
import joinCssClassNames from "../utils/joinCssClassNames";
import { DownloadIcon } from "@heroicons/react/solid";

export const VerticalCheckboxes = ({
  input: { onChange, value },
  label,
  options,
  select,
  ...rest
}) => {
  const [selected, setSelected] = useState([]);
  const { data } = useFormData();

  const handleChange = (e) => {
    let newSelected = selected.slice();
    if (
      e.target.checked &&
      e.target.value === options[options.length - 1].name
    ) {
      newSelected = [e.target.value];
    } else if (e.target.checked) {
      newSelected.push(e.target.value);
    } else {
      newSelected.splice(selected.indexOf(e.target.value), 1);
    }
    setSelected(newSelected);
  };

  useEffect(() => {
    onChange(selected);
  }, [selected]);

  useEffect(() => {
    if (data[select]) setSelected(data[select]);
  }, []);

  return (
    <fieldset>
      <p className="text-sm mb-10">Select one business model at least.</p>
      <div className="border-t border-b border-gray-200 divide-y divide-gray-200">
        {options.map((option, optionIdx) => {
          let disabled =
            selected.includes(options[options.length - 1].name) &&
            option.name !== options[options.length - 1].name;

          return (
            <div
              key={optionIdx}
              className={joinCssClassNames(
                disabled ? "opacity-50" : "",
                "relative flex items-start py-4"
              )}
            >
              <div className="min-w-0 flex-1 text-sm">
                <label
                  htmlFor={`option-${option.id}`}
                  className="font-medium text-gray-700 select-none"
                >
                  {option.name}
                </label>
              </div>
              <div className="ml-3 flex items-center h-5">
                <input
                  id={`option-${option.id}`}
                  name={`option-${option.id}`}
                  type="checkbox"
                  onChange={handleChange}
                  disabled={disabled}
                  value={option.name}
                  checked={selected.includes(option.name)}
                  className="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded"
                />
              </div>
            </div>
          );
        })}
      </div>
    </fieldset>
  );
};

export const CheckBoxes = ({
  input: { onChange, value },
  label,
  options,
  select,
  description,
  ...rest
}) => {
  const [selected, setSelected] = useState(
    select === "packages" ? ["1"] : ["Hero", "Footer"]
  );
  const { data } = useFormData();

  const handleChange = (e) => {
    let newSelected = selected.slice();
    if (e.target.checked) {
      newSelected.push(e.target.value);
    } else {
      newSelected.splice(selected.indexOf(e.target.value), 1);
    }
    setSelected(newSelected);
  };

  useEffect(() => {
    onChange(selected);
  }, [selected]);

  useEffect(() => {
    if (data[select]) setSelected(data[select]);
  }, []);

  return (
    <fieldset>
      {select === "lptemplate" && (
        <p className="text-sm font-Poppins mb-10">
          Select the components you’d like to include. You can choose up to{" "}
          {8 - selected.length} components.
        </p>
      )}
      <div className="mt-4 grid grid-cols-1 md:grid-cols-2 lg:grid-cols-2 xl:grid-cols-3 gap-10">
        {options.map((option, optionIdx) => {
          let checked =
            select === "packages"
              ? (selected.includes(option.id.toString()) &&
                  selected.includes("1")) ||
                selected.includes("Hero") ||
                selected.includes("Footer")
              : selected.includes(option.name);
          let disabled =
            select === "packages"
              ? option.id === 1
              : (!checked && selected.length === 8) ||
                option.name === "Hero" ||
                option.name === "Footer";
          return (
            <div
              key={optionIdx}
              className={joinCssClassNames(
                checked && disabled && select === "packages"
                  ? "border-BE-purple opacity-70"
                  : checked
                  ? "border-BE-purple"
                  : "border-transparent",
                disabled && select !== "packages" ? "opacity-50" : "",
                !(checked && disabled && select === "packages") &&
                  option.action === "input"
                  ? "hover:border-BE-purple"
                  : "",
                "relative shadow flex rounded-3xl border-2 overflow-hidden"
              )}
            >
              <div className="min-w-0 flex-1 text-sm">
                <label
                  htmlFor={`option${option.id}`}
                  className="font-medium text-gray-700 select-none flex h-full flex-col"
                >
                  <div className="flex-1">
                    <div className="relative rounded-xl">
                      {option.price && (
                        <span
                          className={joinCssClassNames(
                            checked && disabled && select === "packages"
                              ? "bg-BE-purple"
                              : "bg-BE-purple",
                            " text-white font-normal absolute top-5 px-4 py-2 left-0 rounded-r-md text-lg"
                          )}
                        >
                          AED {option.price}
                        </span>
                      )}

                      {option.banner && (
                        <span
                          className={joinCssClassNames(
                            `py-1.5 px-4 ${option.textColor} absolute border-BE-purple border-2 ${option.backgroundColor} text-xs font-semibold uppercase tracking-wide text-white transform w-3/4 lg:w-1/2 left-0 -bottom-3 right-0 mx-auto text-center rounded-full`
                          )}
                        >
                          {option.banner}
                        </span>
                      )}
                      <img
                        className={joinCssClassNames(
                          option.action === "input"
                            ? "cursor-pointer"
                            : "cursor-auto",
                          "w-full"
                        )}
                        src={option.src}
                      />
                    </div>
                    <div
                      className={joinCssClassNames(
                        select === "packages" ? "pb-0" : "pb-7"
                      )}
                    >
                      <p
                        className={joinCssClassNames(
                          "font-bold text-lg px-7 pt-7"
                        )}
                      >
                        {option.name}
                      </p>
                      {option.description && (
                        <ul
                          className={joinCssClassNames(
                            select === "packages"
                              ? "list-inside list-disc"
                              : "",
                            "mt-1 px-7  list "
                          )}
                        >
                          {option.description.map((item, index) => (
                            <li key={index} className="text-gray-700 list-item">
                              <span className="text-gray-900">{item}</span>
                            </li>
                          ))}
                        </ul>
                      )}
                    </div>
                  </div>
                  {select === "packages" && option.action === "input" && (
                    <div className="w-full p-7 flex flex-col items-center space-y-2">
                      {/* <a
                        className={joinCssClassNames(
                          checked && disabled && select === "packages"
                            ? "text-white bg-BE-purple"
                            : checked
                            ? "bg-white text-BE-purple"
                            : "bg-BE-purple text-white",
                          "text-sm px-1 py-3 lg:py-2 text-center shadow-md rounded-full w-full"
                        )}
                      >
                        {checked ? "Selected" : "Select Package"}
                      </a> */}
                      {option.preview && (
                        <a
                          href={`/packages/${option.id}.pdf`}
                          download={option.name}
                          className=" text-BE-purple flex justify-center text-sm px-1 py-3 lg:py-1 w-full"
                        >
                          More Details
                        </a>
                      )}
                    </div>
                  )}
                  {select === "packages" && option.action === "email" && (
                    <div className="w-full p-7">
                      <a
                        href="mailto:team@brandexpress.ae"
                        className="text-BE-purple w-full inline-block text-sm px-1 py-3 lg:py-1 text-center rounded-full"
                      >
                        Get in Touch
                      </a>
                    </div>
                  )}
                </label>
              </div>
              {option.action === "input" && (
                <div className="flex items-start justify-start h-5 absolute right-5 top-5">
                  <input
                    id={`option${option.id}`}
                    name={`option${option.id}`}
                    type="checkbox"
                    value={select === "packages" ? option.id : option.name}
                    checked={checked}
                    disabled={disabled}
                    onChange={handleChange}
                    className={joinCssClassNames(
                      checked && disabled && select === "packages"
                        ? "text-BE-purple"
                        : "text-BE-purple",
                      "focus:ring-indigo-500 h-7 w-7 border-gray-300 rounded"
                    )}
                  />
                </div>
              )}
            </div>
          );
        })}
      </div>
    </fieldset>
  );
};
