import { Carousel } from "react-responsive-carousel";

const portfolios = [
  {
    src: "https://res.cloudinary.com/brandexpress/image/upload/v1635503290/slider/visual-1_wucgke.jpg",
    srcMobile:
      "https://res.cloudinary.com/brandexpress/image/upload/v1635503354/slider/mobile/visual-1_ytqpst.jpg",
  },
  {
    src: "https://res.cloudinary.com/brandexpress/image/upload/v1635503290/slider/visual-2_h7urdh.jpg",
    srcMobile:
      "https://res.cloudinary.com/brandexpress/image/upload/v1635503356/slider/mobile/visual-2_yl7zct.jpg",
  },
  {
    src: "https://res.cloudinary.com/brandexpress/image/upload/v1635503291/slider/visual-3_ietzd2.jpg",
    srcMobile:
      "https://res.cloudinary.com/brandexpress/image/upload/v1635503358/slider/mobile/visual-3_ir70m7.jpg",
  },
  {
    src: "https://res.cloudinary.com/brandexpress/image/upload/v1635503292/slider/visual-4_qasyq4.jpg",
    srcMobile:
      "https://res.cloudinary.com/brandexpress/image/upload/v1635503354/slider/mobile/visual-4_c98qhs.jpg",
  },
  {
    src: "https://res.cloudinary.com/brandexpress/image/upload/v1635503292/slider/visual-5_ojctdl.jpg",
    srcMobile:
      "https://res.cloudinary.com/brandexpress/image/upload/v1635503357/slider/mobile/visual-5_belf5o.jpg",
  },
  {
    src: "https://res.cloudinary.com/brandexpress/image/upload/v1635503292/slider/visual-6_zndunf.jpg",
    srcMobile:
      "https://res.cloudinary.com/brandexpress/image/upload/v1635503357/slider/mobile/visual-6_pogik2.jpg",
  },
  {
    src: "https://res.cloudinary.com/brandexpress/image/upload/v1635503292/slider/visual-7_jjhfzj.jpg",
    srcMobile:
      "https://res.cloudinary.com/brandexpress/image/upload/v1635503360/slider/mobile/visual-7_t8regz.jpg",
  },
  {
    src: "https://res.cloudinary.com/brandexpress/image/upload/v1635503292/slider/visual-8_xroh3p.jpg",
    srcMobile:
      "https://res.cloudinary.com/brandexpress/image/upload/v1635503358/slider/mobile/visual-8_uo4tkg.jpg",
  },
  {
    src: "https://res.cloudinary.com/brandexpress/image/upload/v1635503292/slider/visual-9_tfvuhm.jpg",
    srcMobile:
      "https://res.cloudinary.com/brandexpress/image/upload/v1635503360/slider/mobile/visual-9_nn5olt.jpg",
  },
];

export const FadeSlider = () => {
  return (
    <div className="hidden md:block">
      <div className="lg:max-w-7xl  w-5/6 mx-auto pt-16 px-4 sm:px-6 lg:pt-24  lg:px-0 ">
        <h2 className="text-4xl mb-12 font-semibold text-gray-900 sm:text-5xl sm:leading-none sm:tracking-tight lg:text-6xl">
          showcase
        </h2>
      </div>
      <Carousel
        autoPlay
        infiniteLoop
        showStatus={false}
        showThumbs={false}
        showArrows={false}
        stopOnHover={false}
      >
        {portfolios.map((portfolio, index) => (
          <img key={index} src={portfolio.src} />
        ))}
      </Carousel>
    </div>
  );
};

export const FadeSliderMobile = () => {
  return (
    <div className="block md:hidden">
      <div className="lg:max-w-7xl  w-5/6 mx-auto pt-16 px-4 sm:px-6 lg:pt-24  lg:px-0 ">
        <h2 className="text-4xl mb-12 font-semibold text-gray-900 sm:text-5xl sm:leading-none sm:tracking-tight lg:text-6xl">
          showcase
        </h2>
      </div>
      <Carousel
        autoPlay
        infiniteLoop
        showStatus={false}
        showThumbs={false}
        showArrows={false}
        stopOnHover={false}
      >
        {portfolios.map((portfolio, index) => (
          <img key={index} src={portfolio.srcMobile} />
        ))}
      </Carousel>
    </div>
  );
};
