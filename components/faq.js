import { Disclosure } from "@headlessui/react";
import { ChevronDownIcon } from "@heroicons/react/outline";
import joinCssClassNames from "../utils/joinCssClassNames";

const faqs = [
  {
    question: "How are you different from other branding agencies?",
    answer:
      "We’re a team of tech-driven people who have been working with startups and major brands in the UAE and MENA region for over 15 years, so we know what works. We’re driven by our passion for technology and new ideas, and want to see innovative startups succeed - no matter what. That’s why our services are some of the most affordable in the region.",
  },
  ,
  {
    question: "What is a core package and what are the add-ons?",
    answer:
      "Our packages are designed to give you more flexibility and choice. \nWith the core package, you’ll receive two different directions for your branding basics: your logo, brand book, visual elements, and a branded presentation template. \nDepending on your branding needs, you can also choose to include any of our additional packages (add-ons): a branded landing page, a branded pitch deck template, and/or branded social media assets.",
  },
  {
    question: "How long should I expect to wait for my branding to be done?",
    answer:
      "Our team will start working on your request as soon as we receive it, and you’ll only have to wait up to 5 working days to see the final results.",
  },
  {
    question: "How can I keep track of the work and give feedback?",
    answer:
      "You can access your dashboard at any time. There you’ll find the timeline along with a discussion section for your feedback and comments.",
  },
  {
    question: "How many revisions are included in each package?",
    answer:
      "Our brand foundation package includes two design options according to the information you give us. We are currently flexible regarding the number of requested revisions as long as it’s within an acceptable and fair range.",
  },
];

const FAQ = () => {
  return (
    <div className="bg-gray-50">
      <div className="lg:max-w-7xl w-5/6 mx-auto py-12 px-4 sm:py-16 sm:px-6 lg:px-8">
        <div className="max-w-3xl mx-auto divide-y-2 divide-gray-200">
          <h2 className="text-center text-4xl font-semibold text-gray-900 sm:text-5xl lg:text-6xl">
            frequently asked questions
          </h2>
          <dl className="mt-6 space-y-6 divide-y divide-gray-200">
            {faqs.map((faq) => (
              <Disclosure as="div" key={faq.question} className="pt-6">
                {({ open }) => (
                  <>
                    <dt className="text-lg">
                      <Disclosure.Button className="text-left w-full flex justify-between items-start text-gray-400">
                        <span className="font-medium text-gray-900">
                          {faq.question}
                        </span>
                        <span className="ml-6 h-7 flex items-center">
                          <ChevronDownIcon
                            className={joinCssClassNames(
                              open ? "-rotate-180" : "rotate-0",
                              "h-6 w-6 transform"
                            )}
                            aria-hidden="true"
                          />
                        </span>
                      </Disclosure.Button>
                    </dt>
                    <Disclosure.Panel as="dd" className="mt-2 pr-12">
                      <p className="text-base text-gray-500">{faq.answer}</p>
                    </Disclosure.Panel>
                  </>
                )}
              </Disclosure>
            ))}
          </dl>
        </div>
      </div>
    </div>
  );
};

export default FAQ;
