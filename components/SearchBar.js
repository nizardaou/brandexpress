import { CheckIcon, XIcon } from "@heroicons/react/outline";
import axios from "axios";
import { useState } from "react";

import SearchPreview from "./SearchPreview";

const SearchBar = ({ results, keyword, updateField, modify }) => {
  const [type, setType] = useState("brands");
  //renders our results using the SearchPreview component
  var updateText = async (text) => {
    setType("packages");
    updateField("keyword", text, false);
    let response = await axios.get(`/api/admin/packages?package=${text}`);
    updateField("results", response.data, false);
  };

  var cancelSearch = () => {
    setType("brands");
    updateField("keyword", "");
  };

  var renderResults = results.map((result, index) => {
    return (
      <SearchPreview
        key={index}
        modify={modify}
        updateField={updateField}
        type={type}
        updateText={updateText}
        index={index}
        item={result}
        keyword={keyword}
      />
    );
  });

  const handleSubmit = async (e) => {
    e.preventDefault();
    setType("packages");
    let response = await axios.get(`/api/admin/packages?package=${keyword}`);
    updateField("results", response.data, false);
  };
  return (
    <div className="w-1/2 mx-auto">
      <form onSubmit={handleSubmit}>
        <label
          htmlFor="search"
          className="block text-sm font-medium text-gray-700"
        >
          Search for brands
        </label>
        <div className="mt-1 relative flex items-center">
          <input
            type="text"
            name="keyword"
            id="search"
            placeholder="Search"
            value={keyword}
            onChange={(e) => {
              if (type === "packages") setType("brands");
              updateField("keyword", e.target.value);
            }}
            className="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full pr-12 sm:text-sm border-gray-300 rounded-md"
          />
          <div className="absolute inset-y-0 right-0 flex py-1.5 pr-1.5">
            <button
              type="submit"
              className="inline-flex items-center rounded px-2 text-sm font-sans font-medium"
            >
              <CheckIcon className="h-5 w-5 text-BE-purple" />
            </button>
            <button
              type="button"
              onClick={cancelSearch}
              className="inline-flex items-center rounded px-2 text-sm font-sans font-medium"
            >
              <XIcon className="h-5 w-5 text-BE-purple" />
            </button>
          </div>
        </div>
      </form>

      {results.length > 0 ? (
        <div className="flow-root mt-6 bg-white px-2 py-2">
          <ul role="list" className="-my-5 divide-y divide-gray-200">
            {renderResults}
          </ul>
        </div>
      ) : null}
    </div>
  );
};

export default SearchBar;
