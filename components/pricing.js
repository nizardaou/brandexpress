import { CheckIcon, ChevronDownIcon } from "@heroicons/react/outline";
import joinCssClassNames from "../utils/joinCssClassNames";
import { useState } from "react";

import { Carousel } from "react-responsive-carousel";
import { ChevronLeftIcon, ChevronRightIcon } from "@heroicons/react/solid";
const pricing = {
  tiers: [
    [
      {
        tierID: 1,
        title: "Brand Foundation",
        price: 2750,
        banner: "REQUIRED",
        description:
          "This core package includes the basics of your branding needs.",
        features: [
          "Logo & Brand mark",
          "Brand Book & Visual Elements",
          "Branded Presentation Template",
        ],
        cta: "Monthly billing",
        actionText: "More Details",
        action: "download",
        addon: false,
        preview: true,
        backgroundColor: "bg-gray-700",
        textColor: "text-white",
        bannerBackgroundColor: "bg-BE-pink",
      },
      {
        tierID: 5,
        title: "Branded Shopify E-store Theme",
        price: 3750,
        banner: "ADD-ON",
        description:
          "Get your own branded Shopify e-store theme, customized as per your brand guidelines.",
        features: [
          "Branded e-store homepage",
          "Branded product & collection page",
          "Branded about & contact page",
          "Branded shopify blog",
          "Theme installation",
        ],

        cta: "Monthly billing",
        actionText: "More Details",
        action: "download",
        addon: true,
        preview: false,
        backgroundColor: "bg-white",
        textColor: "text-black",
        bannerBackgroundColor: "bg-BE-purple",
      },
      {
        tierID: 4,
        title: "Branded Social Media Assets & Posts Template",
        price: 550,
        banner: "ADD-ON",
        description:
          "Create a seamless look and feel for your brand across social media.",
        features: ["Branded social media visual templates"],
        cta: "Monthly billing",
        actionText: "More Details",
        action: "download",
        addon: true,
        preview: true,
        backgroundColor: "bg-white",
        textColor: "text-black",
        bannerBackgroundColor: "bg-BE-purple",
      },
    ],
    [
      {
        tierID: 3,
        title: "Branded Landing Page Template",
        price: 750,
        banner: "ADD-ON",
        description:
          "Make your brand stand out with a landing page customized for your brand.",
        features: ["Branded one-page website template"],
        cta: "Monthly billing",
        actionText: "More Details",
        action: "download",
        addon: true,
        preview: true,
        backgroundColor: "bg-white",
        textColor: "text-black",
        bannerBackgroundColor: "bg-BE-purple",
      },
      {
        tierID: 2,
        title: "Branded Pitch Deck Template",
        price: 950,
        banner: "ADD-ON",
        description: "For when you want to wow during your investor meetings.",
        features: ["Branded investor pitch deck template"],
        cta: "Monthly billing",
        actionText: "More Details",
        action: "download",
        addon: true,
        preview: true,
        backgroundColor: "bg-white",
        textColor: "text-black",
        bannerBackgroundColor: "bg-BE-purple",
      },
      {
        tierID: 6,
        title: "Looking for something more specific?",
        price: "CUSTOM",
        banner: "CUSTOM",
        description:
          "Let us know what you’re looking for. We’ll be happy to help.",
        features: ["Tailored work designed around your specific brand needs "],
        cta: "Monthly billing",
        actionText: "Get in Touch",
        action: "email",
        addon: false,
        preview: true,
        backgroundColor: "bg-BE-purple",
        textColor: "text-white",
        bannerBackgroundColor: "bg-white",
        bannerTextColor: "text-BE-purple",
        bannerBorderColor: "border-BE-purple border-2",
      },
    ],
  ],
};

const Pricing = () => {
  return (
    <div className="w-5/6 lg:max-w-7xl mx-auto py-24 px-0 bg-white sm:px-0 lg:px-0">
      <h2 className="text-4xl mb-12 font-semibold text-gray-900 sm:text-5xl sm:leading-none sm:tracking-tight lg:text-6xl ">
        pricing plans
      </h2>

      {/* Tiers */}

      {pricing.tiers.map((slides) => (
        <div className="space-y-12 lg:space-y-0 py-5 px-5 lg:px-10 lg:grid lg:grid-cols-3 lg:gap-x-8 lg:gap-y-12">
          {slides.map((tier, index) => {
            return (
              <div
                key={tier.title}
                className={joinCssClassNames(
                  ` ${tier.backgroundColor} ${tier.textColor} relative p-8 border border-gray-200 rounded-2xl shadow-xl flex flex-col`
                )}
              >
                <div className="flex-1">
                  <div className="h-auto lg:h-80 xl:h-44">
                    <h3
                      className={joinCssClassNames(
                        tier.addon ? "text-black" : "text-white",
                        "text-xl font-semibold"
                      )}
                    >
                      {tier.title}
                    </h3>
                    <p className="mt-2">{tier.description}</p>
                  </div>
                  <p
                    className={joinCssClassNames(
                      `absolute top-0 py-1.5 px-4 ${tier.bannerBackgroundColor} ${tier.bannerTextColor} ${tier.bannerBorderColor} rounded-full text-xs font-semibold uppercase tracking-wide text-white transform -translate-y-1/2`
                    )}
                  >
                    {tier.banner + " " + "PACKAGE"}
                  </p>
                  <p
                    className={joinCssClassNames(
                      tier.addon ? "text-BE-purple" : "text-white",
                      "mt-10 mb-10 lg:mt-4 lg:mb-0 flex items-baseline"
                    )}
                  >
                    <span className="text-3xl xl:text-4xl font-extrabold tracking-tight">
                      {tier.action === "download" ? "AED" : ""} {tier.price}
                    </span>
                  </p>
                  {/* Feature list */}
                  <p className="mt-0 lg:mt-8 cursor-pointer">
                    what's included?{" "}
                  </p>

                  <ul role="list" className="mt-6 space-y-2">
                    {tier.features.map((feature) => (
                      <li key={feature} className="flex">
                        <CheckIcon
                          className={joinCssClassNames(
                            tier.addon ? "text-BE-purple" : "text-white",
                            "flex-shrink-0 w-6 h-6"
                          )}
                          aria-hidden="true"
                        />
                        <span className="ml-3">{feature}</span>
                      </li>
                    ))}
                  </ul>
                </div>
                {tier.preview && (
                  <a
                    href={
                      tier.action === "download"
                        ? `/packages/${tier.tierID}.pdf`
                        : "mailto: team@brandexpress.ae"
                    }
                    download={tier.action === "download" ? tier.title : false}
                    className={joinCssClassNames(
                      tier.addon ? "text-BE-purple" : "text-white",
                      "mt-8 block w-full py-3 px-6 border border-transparent rounded-md text-center font-medium"
                    )}
                  >
                    {tier.actionText}
                  </a>
                )}
              </div>
            );
          })}
        </div>
      ))}
    </div>
  );
};

export default Pricing;
