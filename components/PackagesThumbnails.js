import { connect } from "react-redux";
import { useEffect, useState } from "react";
import { getPackages } from "../redux/actions/authActions";
import { bindActionCreators } from "redux";
import { useRouter } from "next/router";
import {
  coreName,
  addon1Name,
  addon2Name,
  addon3Name,
  addon4Name,
  addon1Image,
  addon2Image,
  addon3Image,
  addon4Image,
  coreImage,
} from "../constants/packages";
import { ClockIcon } from "@heroicons/react/outline";
import joinCssClassNames from "../utils/joinCssClassNames";
import Breadcrumb from "./BreadCrumb";
import { packageEnums } from "../constants/enum";

const PackagesThumbnails = ({
  authentication,
  getPackages,
  breadCrumb,
  find,
}) => {
  let router = useRouter();
  let [pages, setPages] = useState([]);
  const [filter, setFilter] = useState();
  const [coreDelivered, setCoreDelivered] = useState([]);

  useEffect(() => {
    if (authentication.token) {
      getPackages({ token: authentication.token }, "packages");
    }
  }, [authentication.token]);

  useEffect(() => {
    if (!router.isReady) return;

    let filter = router.query.name;

    if (!filter) {
      setFilter(find);
      return;
    }

    setFilter(filter);
  }, [router.isReady, find]);

  useEffect(() => {
    if (!filter) return;
    let breadCrumb = [];

    breadCrumb.push({
      name: filter,
      href: `/dashboard/packages?name=${filter}`,
      current: true,
    });
    setPages(breadCrumb);
  }, [filter]);

  useEffect(() => {
    if (!authentication.packages) return;
    // check if core package is delivered and set state to true
    authentication.packages.forEach((tier) => {
      if (
        tier.status === packageEnums.coreStatus.artworkReady &&
        tier.type === "CORE"
      ) {
        setCoreDelivered((prevValues) => [tier.brandName, ...prevValues]);
        // setCoreDelivered({ brandName: tier.brandName, status: true });
      }
    });
  }, [authentication.packages]);

  useEffect(() => {
    console.log(coreDelivered);
  }, [coreDelivered]);

  function sortFilter(array) {
    return array
      .sort((a, b) => (a.type > b.type ? -1 : b.type > a.type ? 1 : 0))
      .filter((item) => {
        if (!filter) return item;
        return (
          upperCaseTrim(item.brandName) === upperCaseTrim(filter) ||
          upperCaseTrim(item.name) === upperCaseTrim(filter)
        );
      });
  }

  function upperCaseTrim(string) {
    return string.toUpperCase().replace(/\s+/g, "");
  }

  function getPackagePic(name) {
    if (name === coreName) {
      return coreImage;
    } else if (name === addon1Name) {
      return addon1Image;
    } else if (name === addon2Name) {
      return addon2Image;
    } else if (name === addon3Name) {
      return addon3Image;
    } else if (name === addon4Name) {
      return addon4Image;
    } else return "";
  }

  function getColor(name) {
    if (name === packageEnums.addonStatus.pending) {
      return "bg-gray-200 text-gray-600";
    } else if (
      name === packageEnums.coreStatus.started ||
      name === packageEnums.addonStatus.started
    ) {
      return "bg-blue-200 text-blue-600";
    } else if (
      name === packageEnums.coreStatus.directionSubmitted ||
      name === packageEnums.coreStatus.directionChosen ||
      name === packageEnums.coreStatus.feedbackSubmitted ||
      name === packageEnums.addonStatus.inProgress
    ) {
      return "bg-yellow-200 text-yellow-600";
    } else if (
      name === packageEnums.coreStatus.artworkReady ||
      name === packageEnums.addonStatus.ready
    ) {
      return "bg-purple-200 text-BE-purple";
    }
  }

  return (
    <>
      {breadCrumb && <Breadcrumb pages={pages} />}
      {authentication.packages &&
      sortFilter(authentication.packages).length > 0 ? (
        <ul
          role="list"
          className="grid grid-cols-2 gap-x-4 gap-y-8 sm:grid-cols-3 sm:gap-x-6 lg:grid-cols-4 xl:gap-x-8 mt-8"
        >
          {sortFilter(authentication.packages).map((tier, index) => {
            let enabledPackage =
              coreDelivered.includes(tier.brandName) || tier.type === "CORE";
            return (
              <li
                onClick={() => {
                  enabledPackage
                    ? router.push({
                        pathname: "/dashboard/packageinfo",
                        query: {
                          name: tier.brandName,
                          package: tier.name,
                        },
                      })
                    : "";
                }}
                key={index}
                className={joinCssClassNames(
                  enabledPackage ? "cursor-pointer" : "cursor-not-allowed",
                  "relative shadow rounded-md flex flex-col"
                )}
              >
                <div className="group block w-full aspect-w-10 aspect-h-7 rounded-lg bg-gray-100 focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-offset-gray-100 focus-within:ring-indigo-500 overflow-hidden">
                  <div className="flex justify-center items-center h-auto">
                    <img
                      src={getPackagePic(tier.name)}
                      className={joinCssClassNames(
                        enabledPackage ? "opacity-100" : "opacity-50"
                      )}
                    />
                    <ClockIcon
                      className={joinCssClassNames(
                        enabledPackage ? "hidden" : "",
                        "h-24 w-24 absolute text-gray-700"
                      )}
                    />
                  </div>
                  <button
                    type="button"
                    className="absolute inset-0 focus:outline-none"
                  >
                    <span className="sr-only">
                      View details for {tier.name}
                    </span>
                  </button>
                </div>
                <div
                  className={joinCssClassNames(
                    enabledPackage ? "opacity-100" : "opacity-50",
                    "flex-grow pl-5"
                  )}
                >
                  <p className="mt-2 block text-sm font-medium text-gray-900 truncate pointer-events-none">
                    Brand Name: {tier.brandName}
                  </p>
                  <p className="block text-sm font-medium text-gray-900 truncate pointer-events-none whitespace-normal">
                    Package: {tier.name}
                  </p>
                </div>
                <p
                  className={`${getColor(
                    tier.status
                  )} mx-auto my-3 inline-block px-2 py-1 text-sm font-medium text-center rounded-md pointer-events-none`}
                >
                  {tier.status}
                </p>
              </li>
            );
          })}
        </ul>
      ) : (
        <p className="text-center my-5">There is nothing to show here...</p>
      )}
    </>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    getPackages: bindActionCreators(getPackages, dispatch),
  };
};
const mapStateToProps = (state) => {
  return {
    authentication: state.authentication,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PackagesThumbnails);
