import React, { useState } from "react";
import { CardElement, useStripe, useElements } from "@stripe/react-stripe-js";
import axios from "axios";
import { useFormData } from "../context";
import { useEffect } from "react";
import { useRouter } from "next/dist/client/router";
import * as packagesDetails from "../constants/packages";

import { setCookie } from "../utils/cookie";
import joinCssClassNames from "../utils/joinCssClassNames";

const CARD_OPTIONS = {
  iconStyle: "solid",
  style: {
    base: {
      iconColor: "#8835FD",
      color: "#000",
      fontWeight: 500,
      fontFamily: "Roboto, Open Sans, Segoe UI, sans-serif",
      fontSize: "16px",
      fontSmoothing: "antialiased",
      ":-webkit-autofill": {
        color: "#FFC416",
      },
      "::placeholder": {
        color: "#8835FD",
      },
    },
    invalid: {
      iconColor: "#DC2626",
      color: "#DC2626",
    },
  },
};

const CardField = ({ onChange }) => (
  <div>
    <label className="block text-sm font-medium text-gray-700">
      Card details
    </label>
    <div className="mt-1">
      <CardElement
        className="shadow-sm px-1 lg:px-3 py-2 focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md text-gray-700 border-2"
        options={CARD_OPTIONS}
        onChange={onChange}
      />
    </div>
  </div>
);

const Field = ({
  label,
  id,
  type,
  placeholder,
  required,
  autoComplete,
  value,
  onChange,
}) => (
  <div>
    <label htmlFor={id} className="block text-sm font-medium text-gray-700">
      {label}
    </label>
    <div className="mt-1">
      <input
        id={id}
        type={type}
        placeholder={placeholder}
        required={required}
        autoComplete={autoComplete}
        value={value}
        onChange={onChange}
        className="shadow-sm px-1 lg:px-3 py-2 focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
      />
    </div>
  </div>
);

const SubmitButton = ({ processing, error, children, disabled }) => (
  <button
    className={`mx-auto w-full mt-4 whitespace-nowrap px-4 py-2 border border-transparent rounded-md shadow-sm text-base font-medium text-white bg-BE-purple hover:bg-opacity-90 ${
      error ? "SubmitButton--error" : ""
    }`}
    type="submit"
    disabled={processing || disabled}
  >
    {processing ? "Processing..." : children}
  </button>
);

const ErrorMessage = ({ successMessage, children }) => (
  <div
    className={joinCssClassNames(
      successMessage ? "text-green-600" : "text-red-600 ",
      "mt-2 font-normal text-sm"
    )}
    role="alert"
  >
    {children}
  </div>
);

const StripeCheckoutForm = () => {
  const { data } = useFormData();
  const [amount, setAmount] = useState(0);
  const [initialAmount, setInitialAmount] = useState(0);
  const stripe = useStripe();
  const elements = useElements();
  const [error, setError] = useState(null);
  const [cardComplete, setCardComplete] = useState(false);
  const [processing, setProcessing] = useState(false);
  const [totalDiscount, setTotalDiscount] = useState(0);
  const [paymentMethod, setPaymentMethod] = useState(null);
  const [coupon, setCoupon] = useState({
    key: "",
    status: false,
    text: null,
  });
  const [prices, setPrices] = useState([]);
  const [billingDetails, setBillingDetails] = useState({
    email: "",
    phone: "",
    name: "",
  });

  let router = useRouter();
  useEffect(() => {
    let prices = [];
    let amount = 0;

    let discount = 0;

    // check packages conditions to calculate total amount
    if (data.packages.includes("1")) {
      amount = coupon.status
        ? amount + Math.round(2750 - 2750 * 0.25)
        : amount + 2750;
      discount = discount + 2750 * 0.25;
      prices.push({ name: packagesDetails.coreName, price: 2750 });
    }
    if (data.packages.includes("5")) {
      amount = coupon.status
        ? amount + Math.round(3750 - 3750 * 0.25)
        : amount + 3750;
      discount = discount + 3750 * 0.25;
      prices.push({
        name: packagesDetails.addon4Name,
        price: 3750,
      });
    }
    if (data.packages.includes("4")) {
      amount = coupon.status
        ? amount + Math.round(550 - 550 * 0.25)
        : amount + 550;
      discount = discount + 550 * 0.25;
      prices.push({
        name: packagesDetails.addon3Name,
        price: 550,
      });
    }
    if (data.packages.includes("3")) {
      amount = amount + 750;
      prices.push({ name: packagesDetails.addon2Name, price: 750 });
    }

    if (data.packages.includes("2")) {
      amount = amount + 950;
      prices.push({ name: packagesDetails.addon1Name, price: 950 });
    }

    setTotalDiscount(discount);
    setAmount(amount);
    setPrices(prices);
  }, [data.packages, coupon]);

  useEffect(() => {
    let initialAmount = 0;
    prices.forEach((item) => {
      initialAmount = initialAmount + item.price;
    });

    setInitialAmount(initialAmount);
  }, [prices]);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const { error, paymentMethod } = await stripe.createPaymentMethod({
      type: "card",
      card: elements.getElement(CardElement),
      billing_details: billingDetails,
    });

    if (error) {
      console.log(error);
      elements.getElement("card").focus();
      return;
    }

    if (cardComplete) {
      setProcessing(true);
    }

    if (!error) {
      // get packages selected from data state in context
      let packs = data.packages;

      let dataArray = [];

      packs.forEach((item) => {
        if (item === "1") {
          dataArray.push({
            name: packagesDetails.coreName,
            description: packagesDetails.coreDescription,
            brandName: data.name,
            wizardResponseData: {
              domain: data.domain,
              slogan: data.slogan,
              companyType: data.businessCategory.name,
              companyMood: data.moods,
              description: data.description,
              logoStyle: data.style.name,
              logoTerritory: data.territory ? data.territory.name : "",
              primaryColor: data.primary,
              secondaryColor: data.secondary,
            },
            type: "CORE",
            price: 2750,
          });
        }
        if (item === "2") {
          dataArray.push({
            name: packagesDetails.addon1Name,
            description: packagesDetails.addon1Description,
            brandName: data.name,
            wizardResponseData: {
              pitchTemplate: data.pitchtemplate,
            },
            type: "ADDON",
            price: 950,
          });
        }
        if (item === "3") {
          dataArray.push({
            name: packagesDetails.addon2Name,
            description: packagesDetails.addon2Description,
            brandName: data.name,
            wizardResponseData: { landingTemplate: data.lptemplate },
            type: "ADDON",
            price: 750,
          });
        }
        if (item === "4") {
          dataArray.push({
            name: packagesDetails.addon3Name,
            description: packagesDetails.addon3Description,
            brandName: data.name,
            wizardResponseData: {
              mediaTemplate: data.mediatemplate,
            },
            type: "ADDON",
            price: 550,
          });
        }
        if (item === "5") {
          dataArray.push({
            name: packagesDetails.addon4Name,
            description: packagesDetails.addon4Description,
            brandName: data.name,
            wizardResponseData: {
              businessModel: data.shopifyBusinessModel,
              brandsNumber: data.brandsNumber,
              productsNumber: data.productsNumber,
              shopifyDescription: data.shopifyDescription,
            },
            type: "ADDON",
            price: 3750,
          });
        }
      });

      try {
        const { id } = paymentMethod;
        const response = await axios.post("/api/stripe", {
          userDetails: billingDetails,
          packagesDetails: dataArray,
          amount: amount + Math.round(amount * 0.05),
          id: id,
        });
        if (response.data.success) {
          const token = await axios.post("/api/user/token", {
            userID: response.data.message,
          });

          setCookie("token", token.data);
          setProcessing(false);
          setPaymentMethod(paymentMethod);
          router.push("/thankyou");
        } else {
          setError({ message: response.data.message });
          setProcessing(false);
        }
      } catch (error) {
        console.log("error", error);
      }
    } else {
      console.log(error.message);
    }
  };

  const handleChange = (e) => {
    let newKey = e.target.value;
    setCoupon((prevValues) => ({
      ...prevValues,
      key: newKey,
    }));
  };

  return (
    <>
      <div className="grid grid-cols-1 lg:grid-cols-2 lg:gap-5 lg:p-5 font-bold text-xs sm:text-sm lg:text-base">
        <div className="flex flex-col space-y-4 my-5 order-2 lg:order-1">
          <h1 className="hidden lg:block text-2xl sm:text-3xl lg:text-4xl tracking-tight font-semibold text-gray-700">
            Payment details
          </h1>
          <form onSubmit={handleSubmit}>
            <fieldset className="space-y-4">
              <Field
                label="Name"
                id="name"
                type="text"
                placeholder="Jane Doe"
                required
                autoComplete="name"
                value={billingDetails.name}
                onChange={(e) => {
                  setBillingDetails({
                    ...billingDetails,
                    name: e.target.value,
                  });
                }}
              />
              <Field
                label="Email"
                id="email"
                type="email"
                placeholder="janedoe@example.com"
                required
                autoComplete="email"
                value={billingDetails.email}
                onChange={(e) => {
                  setBillingDetails({
                    ...billingDetails,
                    email: e.target.value,
                  });
                }}
              />
              <Field
                label="Phone"
                id="phone"
                type="tel"
                placeholder="(971) 555-0123"
                required
                autoComplete="tel"
                value={billingDetails.phone}
                onChange={(e) => {
                  setBillingDetails({
                    ...billingDetails,
                    phone: e.target.value,
                  });
                }}
              />

              <div className="flex flex-col w-full">
                <label
                  htmlFor="coupon"
                  className="block text-sm font-medium text-gray-700"
                >
                  Have a discount coupon?
                </label>
                <div className="mt-1 flex flex-col lg:flex-row space-y-4 lg:space-y-0 lg:space-x-4">
                  <input
                    id="coupon"
                    type="text"
                    value={coupon.key}
                    placeholder="Coupon code"
                    onChange={handleChange}
                    className="shadow-sm px-1 w-full lg:w-auto flex-grow lg:px-3 py-2 focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm border-gray-300 rounded-md"
                  />
                  <button
                    type="button"
                    className="text-white bg-gray-500 text-base py-2 px-5 rounded-md hover:bg-opacity-90"
                    value={coupon.key}
                    onClick={() => {
                      if (
                        coupon.key === process.env.NEXT_PUBLIC_PAYMENT_COUPON
                      ) {
                        setCoupon({
                          key: coupon.key,
                          status: true,
                          text: "Discount applied successfully",
                        });
                      } else
                        setCoupon({
                          key: "",
                          status: false,
                          text: "Your coupon code is invalid",
                        });
                    }}
                  >
                    Apply Coupon
                  </button>
                </div>

                {coupon.text && (
                  <ErrorMessage successMessage={coupon.status ? true : false}>
                    {coupon.text}
                  </ErrorMessage>
                )}
              </div>
              <CardField
                onChange={(e) => {
                  setError(e.error);
                  setCardComplete(e.complete);
                }}
              />
            </fieldset>
            {error && <ErrorMessage>{error.message}</ErrorMessage>}
            <SubmitButton
              processing={processing}
              error={error}
              disabled={!stripe}
            >
              Pay Now
            </SubmitButton>
          </form>
        </div>
        <h1 className="lg:hidden text-2xl sm:text-3xl lg:text-4xl tracking-tight font-semibold text-gray-700">
          Payment details
        </h1>
        <div className="divide-y-2 order-1 lg:order-2">
          <div className="px-2 py-5 flex bg-gray-50 text-gray-700 rounded-md shadow-md flex-col space-y-4 h-64 lg:h-96 overflow-y-auto lg:px-5">
            {prices.map((item, index) => (
              <div
                key={index}
                className="flex bg-white items-center space-x-4 rounded-lg border-2 border-gray-200 p-3 lg:p-5 w-full h-16 lg:h-auto"
              >
                <p className="w-2/3 lg:w-96">{item.name}</p>
                <p className="font-bold text-right w-1/3">
                  {"AED " + item.price}
                </p>
              </div>
            ))}
          </div>
          <div className="lg:pl-5 my-5 flex justify-end space-y-4 lg:px-5 pt-5">
            <div className="flex-grow divide-y-2 space-y-2">
              <div className="font-normal">
                <p className="flex justify-between">
                  Subtotal: <span> AED {initialAmount}</span>
                </p>
                {coupon.status && (
                  <p className="flex justify-between text-green-600">
                    Discount: <span> AED {Math.round(totalDiscount)}</span>
                  </p>
                )}
                <p className="flex justify-between">
                  VAT (5%): <span>AED {Math.round(amount * 0.05)}</span>
                </p>
              </div>
              <p className="flex justify-between text-BE-purple font-bold pt-2">
                Total: <span> AED {amount + Math.round(amount * 0.05)}</span>
              </p>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default StripeCheckoutForm;
