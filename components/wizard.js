import { useFormData } from "../context";
import { useEffect, useState } from "react";

export const ProgressBar = () => {
  let { data, submitCounter } = useFormData();

  const [total, setTotal] = useState();

  useEffect(() => {
    let totalSteps =
      8 + data.packages.includes("3") * 1 + data.packages.includes("5") * 4;
    setTotal(totalSteps);
  }, [data.packages]);

  function generateArray() {
    var array = [];
    for (var i = 0; i < submitCounter - 1; i++) {
      array.push({
        name: `Step ${i}`,
        href: "#",
        status: "complete",
      });
    }
    for (var i = submitCounter; i < total + 1; i++) {
      array.push({
        name: `Step ${i}`,
        href: "#",
        status: `${i === submitCounter ? "current" : "upcomming"}`,
      });
    }
    return array;
  }
  return (
    <nav
      className="flex mx-auto items-center justify-center"
      aria-label="Progress"
    >
      <p className="text-sm font-medium">
        Step {submitCounter} of {total}
      </p>
      <ol role="list" className="ml-8 hidden lg:flex items-center space-x-5">
        {generateArray().map((step) => (
          <li key={step.name}>
            {step.status === "complete" ? (
              <a
                href={step.href}
                className="block w-2.5 h-2.5 bg-BE-purple rounded-full hover:bg-indigo-900"
              >
                <span className="sr-only">{step.name}</span>
              </a>
            ) : step.status === "current" ? (
              <a
                href={step.href}
                className="relative flex items-center justify-center"
                aria-current="step"
              >
                <span className="absolute w-5 h-5 p-px flex" aria-hidden="true">
                  <span className="w-full h-full rounded-full bg-indigo-200" />
                </span>
                <span
                  className="relative block w-2.5 h-2.5 bg-BE-purple rounded-full"
                  aria-hidden="true"
                />
                <span className="sr-only">{step.name}</span>
              </a>
            ) : (
              <a
                href={step.href}
                className="block w-2.5 h-2.5 bg-gray-200 rounded-full hover:bg-gray-400"
              >
                <span className="sr-only">{step.name}</span>
              </a>
            )}
          </li>
        ))}
      </ol>
    </nav>
  );
};

export default function Wizard({ children, currentStep }) {
  return <div>{currentStep < 14 && <>{children}</>}</div>;
}
