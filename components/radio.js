import { useEffect, useState } from "react";
import { RadioGroup } from "@headlessui/react";
import { useFormData } from "../context";
import joinCssClassNames from "../utils/joinCssClassNames";

export const VerticalRadio = ({
  input: { onChange, value },
  label,
  options,
  select,
  ...rest
}) => {
  const [selected, setSelected] = useState();
  const { data } = useFormData();

  useEffect(() => {
    onChange(selected);
  }, [selected]);

  useEffect(() => {
    if (data[select]) setSelected(data[select]);
  }, []);

  return (
    <fieldset>
      <div className="border-t border-b border-gray-200 divide-y divide-gray-200">
        {options.map((option, optionIdx) => (
          <div key={optionIdx} className="relative flex items-start py-4">
            <div className="min-w-0 flex-1 text-sm">
              <label
                htmlFor={`option-${option.id}`}
                className="font-medium text-gray-700 select-none"
              >
                {option.name}
              </label>
            </div>
            <div className="ml-3 flex items-center h-5">
              <input
                id={`option-${option.id}`}
                onChange={(e) => setSelected(e.target.value)}
                name={select}
                type="radio"
                value={option.name}
                defaultChecked={option.name === data[select]}
                className="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300"
              />
            </div>
          </div>
        ))}
      </div>
    </fieldset>
  );
};

export const Radio = ({
  input: { onChange, value },
  label,
  options,
  select,
  lgGrid,
  smGrid,
  ...rest
}) => {
  const [selected, setSelected] = useState();
  const { data } = useFormData();

  useEffect(() => {
    onChange(selected);
  }, [selected]);

  useEffect(() => {
    if (data[select]) setSelected(data[select]);
  }, []);

  return (
    <RadioGroup value={selected} onChange={setSelected}>
      <RadioGroup.Label className="sr-only">Style</RadioGroup.Label>
      <div className={`mt-4 grid ${lgGrid} ${smGrid}`}>
        {options.map((option, index) => (
          <RadioGroup.Option
            key={index}
            value={option}
            className={({ checked }) =>
              joinCssClassNames(
                checked ? "border-BE-purple" : "",
                "relative block shadow rounded-md overflow-hidden hover:border-BE-purple border-2 border-transparent bg-white p-0 cursor-pointer"
              )
            }
          >
            <>
              <div className="flex items-center">
                <div className="text-sm w-full">
                  <RadioGroup.Description as="div" className="text-gray-500">
                    <div className="border-transparent rounded-lg">
                      <img className="w-full" src={option.image} />
                    </div>
                  </RadioGroup.Description>
                </div>
              </div>
              {option.name && (
                <RadioGroup.Label
                  as="p"
                  className="font-bold text-lg text-gray-900 pl-5"
                >
                  {option.name}
                </RadioGroup.Label>
              )}
              {option.symbol && (
                <RadioGroup.Description
                  as="p"
                  className="text-sm pb-2 pl-5 mb-2"
                >
                  {option.symbol}
                </RadioGroup.Description>
              )}
            </>
          </RadioGroup.Option>
        ))}
      </div>
    </RadioGroup>
  );
};
