const steps = [
  {
    title: "1- Creative Brief",
    color: "text-BE-purple",
    description:
      "Take a few minutes to answer briefing questions through our simple wizard. Then proceed to payment.",

    imageUrl:
      "https://res.cloudinary.com/brandexpress/image/upload/v1635503180/cards/2_opwhga.jpg",
  },
  {
    title: "2- Review the designs",
    color: "text-BE-pink",
    description:
      "We’ll send you the work within 5 working days. Now it is your turn to review the work and send us your feedback.",
    imageUrl:
      "https://res.cloudinary.com/brandexpress/image/upload/v1635503179/cards/1_azmvf0.jpg",
  },
  {
    title: "3- Download the work",
    color: "text-BE-blue",
    description:
      "We will process your feedback to produce the final files and assets of all the products that you had selected.",
    imageUrl:
      "https://res.cloudinary.com/brandexpress/image/upload/v1637427918/cards/3_r6rc3f.jpg",
  },
];

const Cards = () => {
  return (
    <div className="relative bg-gray-50 pt-16 pb-20 px-4 sm:px-6 lg:pt-24 lg:pb-28 lg:px-0">
      <div className="absolute inset-0">
        <div className="h-1/3 sm:h-2/3" />
      </div>
      <div className="relative w-5/6 lg:max-w-7xl mx-auto">
        <div className="text-left">
          <h2 className="text-4xl font-semibold text-gray-900 sm:text-5xl sm:leading-none sm:tracking-tight lg:text-6xl ">
            how it works
          </h2>
        </div>
        <div className="mt-12 max-w-lg mx-auto grid gap-5 lg:grid-cols-3 lg:max-w-none">
          {steps.map((step) => (
            <div
              key={step.title}
              className={`flex flex-col rounded-lg shadow-lg overflow-hidden`}
            >
              <div className="flex-shrink-0">
                <img
                  className="h-48 w-full object-cover"
                  src={step.imageUrl}
                  alt=""
                />
              </div>
              <div className="flex-1 p-6 flex flex-col justify-between">
                <div className="flex-1">
                  <div className="block mt-2">
                    <p className={`text-xl font-semibold ${step.color}`}>
                      {step.title}
                    </p>
                    <p className="mt-3 text-base text-gray-500">
                      {step.description}
                    </p>
                  </div>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Cards;
