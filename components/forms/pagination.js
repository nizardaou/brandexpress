import { ProgressBar } from "../wizard";
const Pagination = ({ stepNumber, prevFormStep, confirmPayment }) => (
  <div className=" bg-white shadow-inverse fixed bottom-0 left-0 right-0">
    <div className="w-5/6 lg:max-w-7xl mx-auto flex py-2 lg:py-5 flex-row">
      {stepNumber > 0 && stepNumber <= 13 && (
        <button
          onClick={prevFormStep}
          className="sm:w-32 w-4/12 whitespace-nowrap inline-flex items-center justify-center px-0 md:px-4 py-2 border border-transparent rounded-md shadow-sm text-base font-medium text-white bg-BE-purple hover:bg-opacity-90"
        >
          Previous
        </button>
      )}
      {stepNumber <= 13 && stepNumber >= 1 && <ProgressBar />}
      {stepNumber < 13 && (
        <button
          type="submit"
          className="sm:w-32 ml-auto w-4/12 whitespace-nowrap inline-flex items-center justify-center px-0 md:px-4 py-2 border border-transparent rounded-md shadow-sm text-base font-medium text-white bg-BE-purple hover:bg-opacity-90"
        >
          Next
        </button>
      )}
      {stepNumber === 13 && (
        <button
          onClick={confirmPayment}
          className="sm:w-32 w-4/12 whitespace-nowrap inline-flex items-center justify-center px-0 md:px-4 py-2 border border-transparent rounded-md shadow-sm text-base font-medium text-white bg-BE-purple hover:bg-opacity-90"
        >
          Confirm
        </button>
      )}
    </div>
  </div>
);

export default Pagination;
