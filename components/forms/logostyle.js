import { Form, Field } from "react-final-form";
import { useFormData } from "../../context";
import { Radio } from "../radio";

import Pagination from "./pagination";

const validate = (values) => {
  const errors = {};
  if (!values.style) {
    errors.style = "Style required";
  }

  return errors;
};

const logoStyles = [
  {
    name: "Emblem",
    symbol: "Symbolic Logo",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1635505524/onboarding/logostyles/t5_nwlcrw.jpg",
    alt: "",
  },
  {
    name: "Wordmark",
    symbol: "Typographic Logo",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1635505524/onboarding/logostyles/t6_p43mrv.jpg",
    alt: "",
  },
];

export default function LogoStyle({
  nextFormStep,
  prevFormStep,
  stepNumber,
  packageName,
}) {
  const { setFormValues } = useFormData();

  const onSubmit = (values) => {
    setFormValues(values);

    nextFormStep();
  };

  return (
    <Form
      initialValues={{
        style: {
          name: "",
          symbol: "",
          image: "",
          alt: "",
        },
      }}
      onSubmit={onSubmit}
      validate={validate}
      render={({ handleSubmit }) => (
        <form onSubmit={handleSubmit}>
          <div className="min-h-full bg-white flex flex-col justify-start pb-10 sm:px-6 lg:px-8">
            <div className="mt-8 mx-auto w-5/6 lg:max-w-7xl">
              <div className="bg-white py-8 px-4 sm:px-10">
                <div className="space-y-6">
                  <div>
                    <h1 className="block font-semibold text-4xl text-gray-700 mb-5">
                      {packageName}
                    </h1>
                    <h2 className="block mb-2 font-semibold text-xl text-BE-purple">
                      Logo type
                    </h2>
                    <label
                      htmlFor="style"
                      className="block text-sm font-medium text-gray-700"
                    >
                      Which type works best for your brand?
                    </label>

                    <div className="mt-6">
                      <Field
                        name="style"
                        select="style"
                        options={logoStyles}
                        lgGrid="lg:grid-cols-4 gap-10"
                        smGrid="grid-cols-1 gap-10"
                        component={Radio}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <Pagination prevFormStep={prevFormStep} stepNumber={stepNumber} />
        </form>
      )}
    />
  );
}
