import { Form, Field } from "react-final-form";
import { useFormData } from "../../context";
import { VerticalCheckboxes } from "../checkboxes";
import Pagination from "./pagination";
const Error = ({ name }) => (
  <Field
    name={name}
    subscription={{ touched: true, error: true }}
    render={({ meta: { touched, error } }) =>
      touched && error ? (
        <span className="text-red-600 text-sm mt-2 block">{error}</span>
      ) : null
    }
  />
);

const validate = (values) => {
  const errors = {};

  if (values.shopifyBusinessModel && values.shopifyBusinessModel.length === 0) {
    errors.shopifyBusinessModel = "Select at least one business model";
  }

  return errors;
};

export default function ShopifyCheckboxes({
  packageName,
  nextFormStep,
  prevFormStep,
  stepNumber,
  options,
  heading,
  label,
  name,
}) {
  const { setFormValues, data } = useFormData();

  const onSubmit = (values) => {
    setFormValues(values);

    nextFormStep();
  };

  return (
    <Form
      onSubmit={onSubmit}
      validate={validate}
      render={({ handleSubmit }) => (
        <form onSubmit={handleSubmit}>
          <div className="min-h-full bg-white flex justify-center sm:px-6 lg:px-8 mb-20">
            <div className="mt-8 w-5/6 lg:max-w-7xl mx-auto">
              <div className="bg-white px-0 sm:px-10 py-8 lg:w-1/2 mx-auto">
                <div className="space-y-6">
                  <div>
                    <h1 className="block font-semibold text-4xl text-gray-700 mb-5">
                      {packageName}
                    </h1>
                    <h2 className="block mb-2 font-semibold text-xl text-BE-purple">
                      {heading}
                    </h2>
                    <label
                      htmlFor={heading}
                      className="block text-sm font-medium text-gray-700"
                    >
                      {label}
                    </label>

                    <div>
                      <Field
                        name={name}
                        select={name}
                        component={VerticalCheckboxes}
                        options={options}
                      />
                      <Error name={name} />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <Pagination prevFormStep={prevFormStep} stepNumber={stepNumber} />
        </form>
      )}
    />
  );
}
