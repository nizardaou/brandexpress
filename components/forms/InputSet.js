import { MinusIcon, PlusIcon } from "@heroicons/react/solid";
import { useEffect, useState } from "react";
import { Form, Field } from "react-final-form";
import { useFormData } from "../../context";
import Pagination from "./pagination";

const Error = ({ name }) => (
  <Field
    name={name}
    subscription={{ touched: true, error: true }}
    render={({ meta: { touched, error } }) =>
      touched && error ? (
        <span className="text-red-600 text-sm mt-2 block">{error}</span>
      ) : null
    }
  />
);

export default function InputSet({
  packageName,
  nextFormStep,
  prevFormStep,
  details,
  heading,
  stepNumber,
}) {
  const { setFormValues, deleteFormValues, data } = useFormData();
  const [newValues, setNewValues] = useState([]);
  const [highestIndex, setHighestIndex] = useState(0);
  const [dynamicInput, setdynamicInput] = useState([
    {
      urlName: "socialUrl0",
      selectName: "socialSelect0",
    },
  ]);

  const [socialAdded, setSocialAdded] = useState(false);

  useEffect(() => {
    var array = [];
    let largestNumber = 0;
    for (const x in data) {
      if (x.includes("socialUrl")) {
        // get the number from the string
        let number = x.match(/\d+/)[0];
        // get largest number
        if (number >= largestNumber) largestNumber = number;
        // create an object
        var item = {};
        // add urlName and selectName to object and push them to array
        item["urlName"] = `socialUrl${number}`;
        item["selectName"] = `socialSelect${number}`;

        array.push(item);
      }
    }
    // set dynamic input to array and highest index to largest number
    if (array.length > 0) {
      setdynamicInput(array);
      setHighestIndex(Number(largestNumber) + Number(1));
    }
  }, []);

  useEffect(() => {
    // add a dynamic input after highest index increments
    if (socialAdded)
      setdynamicInput((prevSocials) => {
        return [
          ...prevSocials,
          {
            urlName: `socialUrl${highestIndex}`,
            selectName: `socialSelect${highestIndex}`,
          },
        ];
      });
  }, [highestIndex]);

  function addSocial() {
    setHighestIndex(Number(highestIndex) + Number(1));
    setSocialAdded(true);
  }

  function removeSocial(id, number) {
    // delete from local state
    setdynamicInput((prevSocials) => {
      return prevSocials.filter((socialItem, index) => {
        return index !== id;
      });
    });

    // delete global state
    if (data[`socialUrl${number}`]) {
      deleteFormValues(`socialUrl${number}`, `socialSelect${number}`);
    }
  }

  const onSubmit = (values) => {
    // iterate over submitted values and delete those that aren't in dynamic input
    for (const x in values) {
      if (
        x.includes("socialUrl") &&
        !dynamicInput.find((item) => item.urlName === x)
      ) {
        delete values[x];
      }
      if (
        x.includes("socialSelect") &&
        !dynamicInput.find((item) => item.selectName === x)
      ) {
        delete values[x];
      }
    }

    // set global state to filtered values
    setFormValues(values);

    nextFormStep();
  };

  useEffect(() => {
    var key = "name";
    let values = details
      .filter(function (value) {
        return value.hasOwnProperty(key); // Get only elements, which have such a key
      })
      .map(function (value) {
        return value[key]; // Extract the values only
      });

    setNewValues(values);
  }, []);

  return (
    <Form
      initialValues={{
        companyPurpose: data.companyPurpose,
        painPoint: data.painPoint,
        bigIdea: data.bigIdea,
        currentIdentity: data.currentIdentity,
        website: data.website,
        shopifyDescription: data.shopifyDescription,
      }}
      onSubmit={onSubmit}
      validate={(values) => {
        const errors = {};

        if (!values.bigIdea && newValues.includes("bigIdea"))
          errors.bigIdea = "Required";
        if (!values.companyPurpose && newValues.includes("companyPurpose"))
          errors.companyPurpose = "Required";
        if (!values.painPoint && newValues.includes("painPoint"))
          errors.painPoint = "Required";
        if (
          !values.shopifyDescription &&
          newValues.includes("shopifyDescription")
        )
          errors.shopifyDescription = "Required";
        return errors;
      }}
      render={({ handleSubmit }) => (
        <form onSubmit={handleSubmit}>
          <div className="min-h-full bg-white flex justify-center sm:px-6 lg:px-8 mb-20">
            <div className="mt-8 w-5/6 lg:max-w-7xl mx-auto">
              <div className="bg-white px-0 sm:px-10 py-8 mx-auto">
                <div className="space-y-6">
                  <div>
                    <h1 className="block font-semibold text-4xl text-gray-700 mb-5">
                      {packageName}
                    </h1>
                    <h2 className="block mb-2 font-semibold text-xl text-BE-purple">
                      {heading}
                    </h2>
                    <div className="space-y-6">
                      {details.map((input, index) => (
                        <div key={index}>
                          <label
                            htmlFor={input.name}
                            className="block text-sm font-medium text-gray-700"
                          >
                            {input.title}
                          </label>
                          <div className="mt-6">
                            <Field
                              id={input.name}
                              name={input.name}
                              type={input.type}
                            >
                              {(props) => (
                                <div>
                                  {input.type === "textarea" ? (
                                    <Field
                                      id={input.name}
                                      name={input.name}
                                      type={input.type}
                                    >
                                      {(props) => (
                                        <textarea
                                          rows="10"
                                          className="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                                          placeholder={input.placeholder}
                                          {...props.input}
                                        />
                                      )}
                                    </Field>
                                  ) : input.dropDown ? (
                                    <div className="space-y-2 flex flex-col">
                                      {dynamicInput.map((item, index) => {
                                        return (
                                          <div key={index}>
                                            <div className="flex space-x-2">
                                              <Field
                                                id={item.selectName}
                                                name={item.selectName}
                                                initialValue={
                                                  data[item.selectName]
                                                    ? data[item.selectName]
                                                    : "twitter"
                                                }
                                              >
                                                {(props) => (
                                                  <select
                                                    className="appearance-none w-1/3 px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                                                    {...props.input}
                                                  >
                                                    <option>twitter</option>
                                                    <option>facebook</option>
                                                    <option>instagram</option>
                                                    <option>youtube</option>
                                                  </select>
                                                )}
                                              </Field>
                                              <Field
                                                id={item.urlName}
                                                name={item.urlName}
                                                type="url"
                                                initialValue={
                                                  data[item.urlName]
                                                    ? data[item.urlName]
                                                    : ""
                                                }
                                              >
                                                {(props) => (
                                                  <input
                                                    placeholder="Ex: http://facebook.com/BrandExpress.AE"
                                                    className="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                                                    {...props.input}
                                                  />
                                                )}
                                              </Field>
                                              {dynamicInput.length > 1 && (
                                                <MinusIcon
                                                  onClick={() =>
                                                    removeSocial(
                                                      index,
                                                      item.urlName.match(
                                                        /\d+/
                                                      )[0]
                                                    )
                                                  }
                                                  className="w-10 h-10 cursor-pointer text-BE-purple"
                                                />
                                              )}
                                            </div>

                                            {index + 1 ===
                                              dynamicInput.length && (
                                              <div className="flex justify-center mt-4 text-BE-purple">
                                                <button
                                                  type="button"
                                                  onClick={() =>
                                                    addSocial(index + 1)
                                                  }
                                                  className="cursor-pointer"
                                                >
                                                  Add new
                                                </button>
                                              </div>
                                            )}
                                          </div>
                                        );
                                      })}
                                    </div>
                                  ) : (
                                    <Field
                                      id={input.name}
                                      name={input.name}
                                      type={input.type}
                                    >
                                      {(props) => (
                                        <input
                                          className="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                                          placeholder={input.placeholder}
                                          {...props.input}
                                        />
                                      )}
                                    </Field>
                                  )}
                                </div>
                              )}
                            </Field>
                            <Error name={input.name ? input.name : ""} />
                          </div>
                        </div>
                      ))}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <Pagination prevFormStep={prevFormStep} stepNumber={stepNumber} />
        </form>
      )}
    />
  );
}
