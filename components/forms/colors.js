import { Form, Field } from "react-final-form";
import { useFormData } from "../../context";
import Pagination from "./pagination";
import { useEffect, useState } from "react";
import { SketchPicker } from "react-color";

import Select from "../select";
import joinCssClassNames from "../../utils/joinCssClassNames";

const options = [
  {
    id: 1,
    name: "One-colour Identity",
  },
  {
    id: 2,
    name: "Two-colour Identity",
  },
  {
    id: 3,
    name: "Multi-colour Identity",
  },
];

const required = (value) =>
  value && (value.color || value.custom) ? undefined : "Required";

const Error = ({ name }) => (
  <Field
    name={name}
    subscription={{ touched: true, error: true }}
    render={({ meta: { touched, error } }) =>
      touched && error ? (
        <span className="text-red-600 text-sm mt-2 block">{error}</span>
      ) : null
    }
  />
);

function SketchExample({ input: { onChange, value }, color, text }) {
  const { data } = useFormData();
  const [state, setState] = useState({
    text: undefined,
    displayColorPicker: false,
    color: undefined,
    custom: false,
  });

  useEffect(() => {
    onChange(state);
  }, [state]);

  useEffect(() => {
    if (data[color] && (data[color].color || data[color].custom)) {
      setState({
        text: data[color].custom ? text : undefined,
        displayColorPicker: false,
        color: data[color].color,
        custom: data[color].custom,
      });
    } else {
      setState((prevState) => ({
        ...prevState,
        color: undefined,
        text: text,
      }));
    }
  }, []);

  const handleClick = () => {
    setState((prevState) => ({
      ...prevState,
      displayColorPicker: !state.displayColorPicker,
    }));
  };

  const handleClose = () => {
    setState((prevState) => ({ ...prevState, displayColorPicker: false }));
  };

  const handleChange = (color) => {
    setState((prevState) => ({
      ...prevState,
      color: color.rgb,
      text: undefined,
      custom: false,
    }));
  };

  return (
    <div>
      <div className="space-x-0 space-y-5 lg:space-y-0 lg:space-x-5 flex flex-col lg:flex-row">
        <div
          style={{
            boxShadow: "0 0 0 1px rgba(0,0,0,.1)",
          }}
          className={joinCssClassNames(
            state.color ? "border-BE-purple" : "border-transparent",
            "cursor-pointer inline-block bg-white shadow rounded-md overflow-hidden hover:border-BE-purple border-2 p-1"
          )}
          onClick={handleClick}
        >
          <div
            style={
              state.color
                ? {
                    background: `rgba(${state.color.r}, ${state.color.g}, ${state.color.b}, ${state.color.a})`,
                  }
                : {
                    background: "#fff",
                  }
            }
            className="flex justify-center items-center rounded-sm w-full h-48 lg:w-52"
          >
            <p className="text-center text-BE-purple">
              {state.text ? state.text : null}
            </p>
          </div>
        </div>

        {color === "twoColorAccent" && (
          <div
            onClick={() =>
              setState({
                color: undefined,
                text: text,
                displayColorPicker: false,
                custom: true,
              })
            }
            style={{
              boxShadow: "0 0 0 1px rgba(0,0,0,.1)",
            }}
            className={joinCssClassNames(
              state.custom ? "border-BE-purple" : "border-transparent",
              "cursor-pointer inline-block bg-white shadow rounded-md overflow-hidden hover:border-BE-purple border-2 p-1"
            )}
          >
            <div className="flex justify-center items-center rounded-sm w-full h-48 lg:w-52">
              <p className="text-center text-BE-purple"> Choose for me </p>
            </div>
          </div>
        )}
      </div>
      {state.displayColorPicker ? (
        <div className="absolute z-10">
          <div className="fixed inset-0" onClick={handleClose} />
          <SketchPicker color={state.color} onChange={handleChange} />
        </div>
      ) : null}
    </div>
  );
}

export default function Colors({
  heading,
  nextFormStep,
  prevFormStep,
  stepNumber,
  packageName,
}) {
  const { setFormValues, data } = useFormData();
  const [direction, setDirection] = useState("One-colour Identity");

  const onSubmit = (values) => {
    setFormValues(values);
    nextFormStep();
  };

  useEffect(() => {
    if (data.colorMixture) setDirection(data.colorMixture);
  }, []);

  function changeDirection(value) {
    setDirection(value);
  }

  return (
    <Form
      onSubmit={onSubmit}
      render={({ handleSubmit }) => (
        <form onSubmit={handleSubmit}>
          <div className="min-h-full bg-white flex justify-center sm:px-6 lg:px-8 mb-96">
            <div className="mt-8 w-5/6 lg:max-w-7xl mx-auto">
              <div className="bg-white px-4 sm:px-10 py-8">
                <div>
                  <h1 className="block font-semibold text-4xl text-gray-700 mb-5">
                    {packageName}
                  </h1>
                  <h2 className="block mb-2 font-semibold text-xl text-BE-purple">
                    {heading}
                  </h2>
                  <p className="block  text-sm font-medium text-gray-700">
                    Select one or more colors that we should focus on.
                  </p>
                  <div className="flex flex-col sm:flex-row justify-between">
                    <div className="mx-auto w-full lg:max-w-7xl">
                      <div className="mt-6">
                        <Field
                          name="colorMixture"
                          select="colorMixture"
                          component={Select}
                          options={options}
                          changeDirection={changeDirection}
                        />
                        <div className="grid grid-cols-1 lg:grid-cols-2 mt-6 gap-y-10">
                          {direction === "One-colour Identity" && (
                            <div className="flex flex-col">
                              <Field
                                validate={required}
                                name="identityColor"
                                color="identityColor"
                                text="Pick your identity colour"
                                component={SketchExample}
                              />
                              <Error name="identityColor" />
                            </div>
                          )}
                          {direction === "Two-colour Identity" && (
                            <>
                              <div className="flex flex-col">
                                <Field
                                  validate={required}
                                  name="twoColorPrimary"
                                  color="twoColorPrimary"
                                  text="Pick your primary colour"
                                  component={SketchExample}
                                />
                                <Error name="twoColorPrimary" />
                              </div>
                              <div className="flex flex-col">
                                <Field
                                  validate={required}
                                  name="twoColorAccent"
                                  color="twoColorAccent"
                                  text="Pick your accent colour"
                                  component={SketchExample}
                                />
                                <Error name="twoColorAccent" />
                              </div>
                            </>
                          )}
                          {direction === "Multi-colour Identity" && (
                            <div className="flex flex-col">
                              <Field
                                validate={required}
                                name="multiColorPrimary"
                                color="multiColorPrimary"
                                text="Pick your primary colour"
                                component={SketchExample}
                              />
                              <Error name="multiColorPrimary" />
                            </div>
                          )}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <Pagination prevFormStep={prevFormStep} stepNumber={stepNumber} />
        </form>
      )}
    />
  );
}
