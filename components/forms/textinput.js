import { Form, Field } from "react-final-form";
import { useFormData } from "../../context";
import Pagination from "./pagination";

const validate = (values) => {
  const errors = {};
  if (!values.name) errors.name = "Required";

  return errors;
};
const Error = ({ name }) => (
  <Field
    name={name}
    subscription={{ touched: true, error: true }}
    render={({ meta: { touched, error } }) =>
      touched && error ? (
        <span className="text-red-600 text-sm mt-2 block">{error}</span>
      ) : null
    }
  />
);

export default function TextInput({
  packageName,
  nextFormStep,
  prevFormStep,
  details,
  heading,
  stepNumber,
}) {
  const { data, setFormValues } = useFormData();

  const onSubmit = (values) => {
    setFormValues(values);

    nextFormStep();
  };

  return (
    <Form
      initialValues={{
        name: data.name,
        slogan: data.slogan,
      }}
      onSubmit={onSubmit}
      validate={validate}
      render={({ handleSubmit }) => (
        <form onSubmit={handleSubmit}>
          <div className="min-h-full bg-white flex justify-center sm:px-6 lg:px-8 mb-20">
            <div className="mt-8 w-5/6 lg:max-w-7xl mx-auto">
              <div className="bg-white px-0 sm:px-10 py-8 lg:w-1/2 mx-auto">
                <h1 className="block font-semibold text-4xl text-gray-700 mb-5">
                  {packageName}
                </h1>
                <h2 className="block mb-2 font-semibold text-xl text-BE-purple">
                  {heading}
                </h2>
                <div className="space-y-6 ">
                  {details.map((input, index) => (
                    <div key={index}>
                      <label
                        htmlFor={input.name}
                        className="block text-sm font-medium text-gray-700"
                      >
                        {input.title}
                      </label>
                      <div className="mt-6">
                        <Field id={input.name} name={input.name} type="text">
                          {(props) => (
                            <div>
                              <input
                                className="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                                placeholder={input.placeholder}
                                {...props.input}
                              />
                            </div>
                          )}
                        </Field>
                        <Error name={input.name} />
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            </div>
          </div>
          <Pagination prevFormStep={prevFormStep} stepNumber={stepNumber} />
        </form>
      )}
    />
  );
}
