import { Form, Field } from "react-final-form";
import { useFormData } from "../../context";
import { CheckBoxes } from "../checkboxes";
import Pagination from "./pagination";

const validate = (values) => {
  const errors = {};
  if (!values.lptemplate) {
    errors.lptemplate = "Landing page template is required";
  }
  return errors;
};

const landingPageTemplates = [
  {
    id: 1,
    name: "Hero",
    description: [
      "The hero section is the first thing a visitor sees on your landing page.",
    ],
    action: "input",
    src: "https://res.cloudinary.com/brandexpress/image/upload/v1635505567/onboarding/landing-page-templates/_hero_jkrsbb.jpg",
  },
  {
    id: 2,
    name: "Blog",
    description: [
      "Choose this component if you plan on writing blog posts and/or news.",
    ],
    action: "input",
    src: "https://res.cloudinary.com/brandexpress/image/upload/v1635505567/onboarding/landing-page-templates/_blog_prqyat.jpg",
  },
  {
    id: 3,
    name: "Feature",
    description: [
      "Highlight elements of your business in a dedicated, prominent section.",
    ],
    action: "input",
    src: "https://res.cloudinary.com/brandexpress/image/upload/v1635505567/onboarding/landing-page-templates/_feature_pecb5m.jpg",
  },
  {
    id: 4,
    name: "Logo Clouds",
    description: [
      "Boost your credibility by featuring logos of companies you’ve collaborated with.",
    ],
    action: "input",
    src: "https://res.cloudinary.com/brandexpress/image/upload/v1635505567/onboarding/landing-page-templates/_logo-clouds_sfcv2v.jpg",
  },
  {
    id: 5,
    name: "Stats",
    description: ["Show visitors your impressive statistics and numbers."],
    action: "input",
    src: "https://res.cloudinary.com/brandexpress/image/upload/v1635505568/onboarding/landing-page-templates/_stats_koog91.jpg",
  },
  {
    id: 6,
    name: "Team",
    description: ["A section where you can put your people front and center."],
    action: "input",
    src: "https://res.cloudinary.com/brandexpress/image/upload/v1635505568/onboarding/landing-page-templates/_teams_pwdwxt.jpg",
  },
  {
    id: 7,
    name: "Pricing",
    description: ["Communicate your pricing plans clearly and concisely."],
    action: "input",
    src: "https://res.cloudinary.com/brandexpress/image/upload/v1635505568/onboarding/landing-page-templates/_pricing_witrhv.jpg",
  },
  {
    id: 8,
    name: "Q-A",
    description: ["A Q&A of your most frequently asked questions."],
    action: "input",
    src: "https://res.cloudinary.com/brandexpress/image/upload/v1635505568/onboarding/landing-page-templates/_q-a_su2wdt.jpg",
  },
  {
    id: 9,
    name: "Contact",
    description: [
      "Tell people how they can reach you, whether it’s by phone or by filling out a form.",
    ],
    action: "input",
    src: "https://res.cloudinary.com/brandexpress/image/upload/v1635505567/onboarding/landing-page-templates/_contact_wziavj.jpg",
  },
  {
    id: 10,
    name: "Signup",
    description: [
      "If you plan on sending a newsletter, add a signup call-to-action section.",
    ],
    action: "input",
    src: "https://res.cloudinary.com/brandexpress/image/upload/v1635505568/onboarding/landing-page-templates/_news-letter_imfx3c.jpg",
  },
  {
    id: 11,
    name: "Footer",
    description: ["The footer holds all the important links in your website."],
    action: "input",
    src: "https://res.cloudinary.com/brandexpress/image/upload/v1635505567/onboarding/landing-page-templates/_footer_tbeakg.jpg",
  },
];

export default function PitchDeck({
  nextFormStep,
  prevFormStep,
  stepNumber,
  packageName,
}) {
  const { setFormValues } = useFormData();

  const onSubmit = (values) => {
    setFormValues(values);

    nextFormStep();
  };

  return (
    <Form
      onSubmit={onSubmit}
      validate={validate}
      render={({ handleSubmit }) => (
        <form onSubmit={handleSubmit}>
          <div className="min-h-full bg-white flex flex-col justify-start sm:px-6 lg:px-8 mb-20">
            <div className="mt-8 lg:max-w-7xl w-5/6 mx-auto">
              <div className="bg-white py-8 px-4 sm:px-10">
                <div className="space-y-6">
                  <div>
                    <h1 className="block font-semibold text-4xl text-gray-700 mb-5">
                      {packageName}
                    </h1>
                    <h2 className="block mb-2 font-semibold text-xl text-BE-purple">
                      Page components
                    </h2>
                    <div>
                      <Field
                        name="lptemplate"
                        description="Select the components you’d like to include."
                        select="lptemplate"
                        options={landingPageTemplates}
                        component={CheckBoxes}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <Pagination prevFormStep={prevFormStep} stepNumber={stepNumber} />
        </form>
      )}
    />
  );
}
