import { Form, Field } from "react-final-form";
import { useFormData } from "../../context";
import { Radio } from "../radio";
import Pagination from "./pagination";

const validate = (values) => {
  const errors = {};
  if (!values.territory) {
    errors.territory = "Territory required";
  }
  return errors;
};

export default function Territory({
  packageName,
  nextFormStep,
  prevFormStep,
  stepNumber,
  logoTerritories,
}) {
  const { setFormValues } = useFormData();

  const onSubmit = (values) => {
    setFormValues(values);

    nextFormStep();
  };

  return (
    <Form
      initialValues={{
        territory: {
          name: "",
          image: "",
          alt: "",
        },
      }}
      onSubmit={onSubmit}
      validate={validate}
      render={({ handleSubmit }) => (
        <form onSubmit={handleSubmit}>
          <div className="min-h-full bg-white flex flex-col justify-start pb-10 sm:px-6 lg:px-8">
            <div className="mt-8 mx-auto w-5/6 lg:max-w-7xl">
              <div className="bg-white py-8 px-4 sm:px-10">
                <div className="space-y-6">
                  <div>
                    <h1 className="block font-semibold text-4xl text-gray-700 mb-5">
                      {packageName}
                    </h1>
                    <h2 className="block mb-2 font-semibold text-xl text-BE-purple">
                      Logo style
                    </h2>
                    <label
                      htmlFor="territory"
                      className="block text-sm font-medium text-gray-700"
                    >
                      Pick the style of your preference
                    </label>
                    <div className="mt-6">
                      <Field
                        name="territory"
                        select="territory"
                        options={logoTerritories}
                        lgGrid="lg:grid-cols-4 gap-10"
                        smGrid="grid-cols-1 gap-10"
                        component={Radio}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <Pagination prevFormStep={prevFormStep} stepNumber={stepNumber} />
        </form>
      )}
    />
  );
}
