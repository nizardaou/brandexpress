import { Form, Field } from "react-final-form";
import { useFormData } from "../../context";
import Pagination from "./pagination";
import RangeInput from "../RangeInput";
import companyMood from "../../constants/companymood";
export default function CompanyMood({
  packageName,
  nextFormStep,
  prevFormStep,
  stepNumber,
  heading,
  label,
  name,
}) {
  const { setFormValues, data } = useFormData();

  const onSubmit = (moods) => {
    let submittedValues = { moods };

    setFormValues(submittedValues);

    nextFormStep();
  };

  return (
    <Form
      onSubmit={onSubmit}
      render={({ handleSubmit }) => (
        <form onSubmit={handleSubmit}>
          <div className="min-h-full bg-white flex justify-center sm:px-6 lg:px-8 mb-20">
            <div className="mt-8 w-5/6 lg:max-w-7xl mx-auto">
              <div className="bg-white px-0 sm:px-10 py-8">
                <div className="space-y-6">
                  <div>
                    <h1 className="block font-semibold text-4xl text-gray-700 mb-5">
                      {packageName}
                    </h1>
                    <h2 className="block mb-2 font-semibold text-xl text-BE-purple">
                      {heading}
                    </h2>
                    <label
                      htmlFor={heading}
                      className="block text-sm font-medium text-gray-700"
                    >
                      {label}
                    </label>

                    <div className="mt-6">
                      {companyMood.map((mood, index) => (
                        <Field
                          key={index}
                          name={mood.name}
                          select={mood.name}
                          label1={mood.label1}
                          label2={mood.label2}
                          component={RangeInput}
                        />
                      ))}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <Pagination prevFormStep={prevFormStep} stepNumber={stepNumber} />
        </form>
      )}
    />
  );
}
