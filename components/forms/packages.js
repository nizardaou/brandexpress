import { Form, Field } from "react-final-form";
import { useFormData } from "../../context";
import { CheckBoxes } from "../checkboxes";
import Pagination from "./pagination";

const Error = ({ name }) => (
  <Field
    name={name}
    subscription={{ touched: true, error: true }}
    render={({ meta: { touched, error } }) =>
      touched && error ? (
        <span className="text-red-600 text-sm mt-2 block">{error}</span>
      ) : null
    }
  />
);

const validate = (values) => {
  const errors = {};

  if (values.packages && values.packages.length === 0) {
    errors.packages = "Core Package is required";
  }

  return errors;
};

const packages = [
  {
    id: 1,
    name: "Brand Foundation",
    description: [
      "Logo & Brand mark",
      "Brand Book & Visual Elements",
      "Branded Presentation Template",
    ],

    price: 2750,
    banner: "REQUIRED PACKAGE",
    src: "https://res.cloudinary.com/brandexpress/image/upload/v1635510301/onboarding/packages/1_p6uyqn.jpg",
    preview: true,
    action: "input",
    backgroundColor: "bg-BE-purple",
    textColor: "text-white",
  },
  {
    id: 5,
    name: "Branded Shopify E-store Theme",
    description: [
      "Branded e-store homepage",
      "Branded product & collection page",
      "Branded about & contact page",
      "Branded shopify blog",
      "Theme installation",
    ],
    price: 3750,
    banner: "ADD-ON PACKAGE",
    src: "https://res.cloudinary.com/brandexpress/image/upload/v1635510301/onboarding/packages/4_ahc5ka.jpg",
    preview: false,
    action: "input",
    backgroundColor: "bg-BE-purple",
    textColor: "text-white",
  },
  {
    id: 4,
    name: "Branded Social Media Assets & Posts Template",
    description: ["Branded social media visual templates"],
    price: 550,
    banner: "ADD-ON PACKAGE",
    src: "https://res.cloudinary.com/brandexpress/image/upload/v1635510301/onboarding/packages/4_ahc5ka.jpg",
    preview: true,
    action: "input",
    backgroundColor: "bg-BE-purple",
    textColor: "text-white",
  },
  {
    id: 3,
    name: "Branded Landing Page Template",
    description: ["Branded one-page website template"],
    price: 750,
    banner: "ADD-ON PACKAGE",
    src: "https://res.cloudinary.com/brandexpress/image/upload/v1635510301/onboarding/packages/3_zjgxal.jpg",
    preview: true,
    action: "input",
    backgroundColor: "bg-BE-purple",
    textColor: "text-white",
  },
  {
    id: 2,
    name: "Branded Pitch Deck Template",
    description: ["Branded investor pitch deck template"],
    price: 950,
    banner: "ADD-ON PACKAGE",
    src: "https://res.cloudinary.com/brandexpress/image/upload/v1635510301/onboarding/packages/2_iul0cf.jpg",
    preview: true,
    action: "input",
    backgroundColor: "bg-BE-purple",
    textColor: "text-white",
  },
  {
    id: 6,
    name: "Looking for something more specific?",
    description: ["Tailored work designed around your specific brand needs"],
    banner: "CUSTOM PACKAGE",
    src: "https://res.cloudinary.com/brandexpress/image/upload/v1637756224/onboarding/packages/6_i08nrp.jpg",
    preview: false,
    action: "email",
    backgroundColor: "bg-white",
    textColor: "text-BE-purple",
  },
];

export default function Packages({ nextFormStep, stepNumber }) {
  const { setFormValues } = useFormData();

  const onSubmit = (values) => {
    setFormValues(values);

    nextFormStep();
  };

  return (
    <Form
      onSubmit={onSubmit}
      validate={validate}
      render={({ handleSubmit }) => (
        <form onSubmit={handleSubmit}>
          <div className="min-h-full bg-white flex flex-col justify-start sm:px-6 lg:px-8 mb-20">
            <div className="mt-8 mx-auto w-5/6 lg:max-w-7xl">
              <div className="bg-white py-8 px-4 sm:px-10">
                <div className="space-y-10">
                  <div>
                    <h2 className="block mb-2 font-semibold text-xl text-BE-purple">
                      Choose your packages
                    </h2>
                    <label
                      htmlFor="packages"
                      className="block text-sm font-medium text-gray-700"
                    >
                      First, choose what your brand needs. The core package is
                      required, and you can select as many add-ons as you’d
                      like.
                    </label>
                    <div className="mt-6">
                      <Field
                        name="packages"
                        select="packages"
                        options={packages}
                        component={CheckBoxes}
                      />
                      <Error name="packages" />
                    </div>
                  </div>

                  <div>
                    <Pagination nextFormStep={nextFormStep} stepNumber={stepNumber} />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
      )}
    />
  );
}
