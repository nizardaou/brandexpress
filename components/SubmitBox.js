import { CheckCircleIcon } from "@heroicons/react/solid";
import joinCssClassNames from "../utils/joinCssClassNames";

export default function SubmitBox({
  label,
  value,
  image,
  color,
  valuesArray,
  classNames,
  checkIcon,
  socialsArray,
}) {
  return (
    <div className={`${classNames} flex flex-row items-start space-x-4`}>
      <div
        className={joinCssClassNames(
          checkIcon ? "flex-row items-center" : "flex-col",
          "flex"
        )}
      >
        {checkIcon && <CheckCircleIcon className="h-5 w-5 mr-1" />}
        <span
          className={joinCssClassNames(
            checkIcon ? "mb-0" : "mb-1",
            "font-bold"
          )}
        >
          {label}
        </span>{" "}
        {value}
        {image && <img className="h-5 w-14" src={image} />}
        {color && (
          <div
            style={{
              background: `rgba(${color.r}, ${color.g}, ${color.b}, ${color.a})`,
            }}
            className="h-5 w-14"
          />
        )}
        {valuesArray &&
          valuesArray.map((value, index) => (
            <p className="capitalize" key={index}>
              {value}
            </p>
          ))}
        {socialsArray &&
          socialsArray.map((value, index) => (
            <div key={index} className="flex">
              <p className="capitalize">{value.selectName}: &nbsp;</p>
              <p className="capitalize"> {value.urlName}</p>
            </div>
          ))}
      </div>
    </div>
  );
}
