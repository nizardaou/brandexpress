import joinCssClassNames from "../utils/joinCssClassNames";
import Feedback from "./Feedback";

//component to render results
const FeedbackPreview = ({ item, type, index, updateText }) => {
  return (
    <li
      onClick={() => (type === "feedbacks" ? "" : updateText(item))}
      key={index}
      className={joinCssClassNames(
        type === "feedbacks" ? "" : "cursor-pointer",
        "py-5"
      )}
    >
      <div className="relative focus-within:ring-2 focus-within:ring-indigo-500">
        <h3 className="text-sm font-semibold text-gray-800">
          {/* Extend touch target to entire panel */}
          <span className="absolute inset-0" aria-hidden="true" />
          {type === "feedbacks" ? item.name : item}
        </h3>
        {type === "feedbacks" && item.expressUserID && (
          <p className="mt-1 text-sm line-clamp-2 text-BE-purple my-3">
            Package: {item.packageID.name}
          </p>
        )}
      </div>

      {type === "feedbacks" && (
        <ul role="list" className="space-y-8">
          <Feedback
            feedback={item}
            packageID={item.packageID}
            expressUserID={item.expressUserID}
          />
        </ul>
      )}
    </li>
  );
};

export default FeedbackPreview;
