import { Fragment, useState, useEffect } from "react";
import { Dialog, Menu, Transition } from "@headlessui/react";
import joinCssClassNames from "../../utils/joinCssClassNames";
import {
  ChatIcon,
  ClockIcon,
  CogIcon,
  DownloadIcon,
  HomeIcon,
  MenuAlt2Icon,
  PlusSmIcon,
  ViewGridIcon,
  XIcon,
} from "@heroicons/react/outline";
import { SearchIcon } from "@heroicons/react/solid";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  getUser,
  authenticate,
  deauthenticate,
} from "../../redux/actions/authActions";
import { useRouter } from "next/router";
import { getCookie } from "../../utils/cookie";

const admins = ["nizardaou@email.com", "waguhava@boximail.com"];

const sidebarNavigation = [
  {
    name: "Home",
    href: "/dashboard",
    icon: HomeIcon,
    current: false,
    validation: true,
  },
  {
    name: "My Brands",
    href: "/dashboard/brands",
    icon: ViewGridIcon,
    current: false,
    validation: true,
  },
  {
    name: "Profile",
    href: "/dashboard/profile",
    icon: CogIcon,
    current: false,
    validation: true,
  },
  {
    name: "Attachments",
    href: "/dashboard/attachments",
    icon: DownloadIcon,
    current: false,
    validation: false,
  },
  {
    name: "Steps",
    href: "/dashboard/steps",
    icon: ClockIcon,
    current: false,
    validation: false,
  },
  {
    name: "Feedbacks",
    href: "/dashboard/feedback",
    icon: ChatIcon,
    current: false,
    validation: false,
  },
];
const userNavigation = [
  { name: "Your Profile", action: "profile" },
  { name: "Sign out", action: "deauthenticate" },
];

const Dashboard = ({ children, ...props }) => {
  const [mobileMenuOpen, setMobileMenuOpen] = useState(false);

  let router = useRouter();
  const [initials, setInitials] = useState({
    firstInitial: "",
    secondInitial: "",
  });

  const [search, setSearch] = useState("");

  useEffect(() => {
    if (!router.isReady) return;

    let token = router.query.token;

    if (token) {
      props.authenticate({ token }, "login");
    } else if (!token && !getCookie("token")) {
      router.push("/");
    }
  }, [router.isReady, props.authentication.token]);

  useEffect(() => {
    sidebarNavigation.forEach((element) => {
      if (router.asPath == element.href) {
        element.current = true;
      }
    });
  }, [router.asPath]);

  useEffect(() => {
    if (props.authentication.token)
      props.getUser({ token: props.authentication.token }, "profile");
  }, [props.authentication.token]);

  useEffect(() => {
    if (props.authentication.user) {
      let nameSplited = props.authentication.user.name.split(" ");

      setInitials({
        firstInitial: nameSplited[0].charAt(0),
        secondInitial: nameSplited[1] ? nameSplited[1].charAt(0) : "",
      });
    }
  }, [props.authentication.user]);

  function handleClick(action) {
    if (action === "deauthenticate") {
      props.deauthenticate();
      router.push("/");
    } else if (action === "profile") {
      router.push("/dashboard/profile");
    }
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    router.push({
      pathname: "/dashboard/results",
      query: { search: search },
    });
  };
  return (
    <div className="h-screen bg-gray-50 flex overflow-hidden">
      {/* Narrow sidebar */}
      <div className="hidden w-28 bg-gray-700 overflow-y-auto md:block">
        <div className="w-full py-6 flex flex-col items-center">
          <div className="flex-shrink-0 flex items-center">
            <a href="/">
              <img
                className="h-8 w-auto"
                src="https://res.cloudinary.com/brandexpress/image/upload/v1635503512/logos/BE-logo-x_h6ao9i.png"
                alt="Brand Express"
              />
            </a>
          </div>
          <div className="flex-1 mt-6 w-full px-2 space-y-1">
            {sidebarNavigation.map((item) => {
              if (
                item.validation ||
                (props.authentication.user &&
                  admins.includes(props.authentication.user.email))
              )
                return (
                  <a
                    key={item.name}
                    href={item.href}
                    className={joinCssClassNames(
                      item.current
                        ? "bg-gray-800 text-white"
                        : "text-indigo-100 hover:bg-gray-800 hover:text-white",
                      "group w-full p-3 rounded-md flex flex-col items-center text-xs font-medium"
                    )}
                    aria-current={item.current ? "page" : undefined}
                  >
                    <item.icon
                      className={joinCssClassNames(
                        item.current
                          ? "text-white"
                          : "text-white group-hover:text-white",
                        "h-6 w-6"
                      )}
                      aria-hidden="true"
                    />
                    <span className="mt-2">{item.name}</span>
                  </a>
                );
            })}
          </div>
        </div>
      </div>

      {/* Mobile menu */}
      <Transition.Root show={mobileMenuOpen} as={Fragment}>
        <Dialog as="div" className="md:hidden" onClose={setMobileMenuOpen}>
          <div className="fixed inset-0 z-40 flex">
            <Transition.Child
              as={Fragment}
              enter="transition-opacity ease-linear duration-300"
              enterFrom="opacity-0"
              enterTo="opacity-100"
              leave="transition-opacity ease-linear duration-300"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <Dialog.Overlay className="fixed inset-0 bg-gray-600 bg-opacity-75" />
            </Transition.Child>
            <Transition.Child
              as={Fragment}
              enter="transition ease-in-out duration-300 transform"
              enterFrom="-translate-x-full"
              enterTo="translate-x-0"
              leave="transition ease-in-out duration-300 transform"
              leaveFrom="translate-x-0"
              leaveTo="-translate-x-full"
            >
              <div className="relative max-w-xs w-full bg-gray-800 pt-5 pb-4 flex-1 flex flex-col">
                <Transition.Child
                  as={Fragment}
                  enter="ease-in-out duration-300"
                  enterFrom="opacity-0"
                  enterTo="opacity-100"
                  leave="ease-in-out duration-300"
                  leaveFrom="opacity-100"
                  leaveTo="opacity-0"
                >
                  <div className="absolute top-1 right-0 -mr-14 p-1">
                    <button
                      type="button"
                      className="h-12 w-12 rounded-full flex items-center justify-center focus:outline-none focus:ring-2 focus:ring-white"
                      onClick={() => setMobileMenuOpen(false)}
                    >
                      <XIcon
                        className="h-6 w-6 text-white"
                        aria-hidden="true"
                      />
                      <span className="sr-only">Close sidebar</span>
                    </button>
                  </div>
                </Transition.Child>
                <div className="flex-shrink-0 px-4 flex items-center">
                  <a href="/">
                    <img
                      className="h-8 w-auto"
                      src="https://res.cloudinary.com/brandexpress/image/upload/v1635503512/logos/BE-logo-x_h6ao9i.png"
                      alt="Brand Express"
                    />
                  </a>
                </div>
                <div className="mt-5 flex-1 h-0 px-2 overflow-y-auto">
                  <nav className="h-full flex flex-col">
                    <div className="space-y-1">
                      {sidebarNavigation.map((item) => (
                        <a
                          key={item.name}
                          href={item.href}
                          className={joinCssClassNames(
                            item.current
                              ? "bg-gray-900 text-white"
                              : "text-indigo-100 hover:bg-gray-900 hover:text-white",
                            "group py-2 px-3 rounded-md flex items-center text-sm font-medium"
                          )}
                          aria-current={item.current ? "page" : undefined}
                        >
                          <item.icon
                            className={joinCssClassNames(
                              item.current
                                ? "text-white"
                                : "text-white group-hover:text-white",
                              "mr-3 h-6 w-6"
                            )}
                            aria-hidden="true"
                          />
                          <span>{item.name}</span>
                        </a>
                      ))}
                    </div>
                  </nav>
                </div>
              </div>
            </Transition.Child>
            <div className="flex-shrink-0 w-14" aria-hidden="true">
              {/* Dummy element to force sidebar to shrink to fit close icon */}
            </div>
          </div>
        </Dialog>
      </Transition.Root>

      {/* Content area */}
      <div className="flex-1 flex flex-col overflow-hidden">
        <header className="w-full">
          <div className="relative z-10 flex-shrink-0 h-16 bg-white border-b border-gray-200 shadow-sm flex">
            <button
              type="button"
              className="border-r border-gray-200 px-4 text-gray-500 focus:outline-none md:hidden"
              onClick={() => setMobileMenuOpen(true)}
            >
              <span className="sr-only">Open sidebar</span>
              <MenuAlt2Icon className="h-6 w-6" aria-hidden="true" />
            </button>
            <div className="flex-1 flex justify-between px-4 sm:px-6">
              <div className="flex-1 flex">
                <form onSubmit={handleSubmit} className="w-full flex md:ml-0">
                  <label htmlFor="search-field" className="sr-only">
                    Search for brands and packages
                  </label>
                  <div className="relative w-full text-gray-400 focus-within:text-gray-600">
                    <button
                      type="submit"
                      className="cursor-pointer absolute inset-y-0 left-0 flex items-center"
                    >
                      <SearchIcon
                        className="flex-shrink-0 h-5 w-5"
                        aria-hidden="true"
                      />
                    </button>
                    <input
                      name="search"
                      id="search"
                      onChange={(e) => {
                        setSearch(e.target.value);
                      }}
                      className="h-full w-full border-transparent py-2 pl-8 pr-3 text-base text-gray-900 placeholder-gray-500 focus:outline-none focus:ring-0 focus:border-transparent focus:placeholder-gray-400"
                      placeholder="Search for your brands and packages"
                    />
                  </div>
                </form>
              </div>
              <div className="ml-2 flex items-center space-x-4 sm:ml-6 sm:space-x-6">
                {/* Profile dropdown */}
                <Menu as="div" className="relative flex-shrink-0">
                  <div>
                    <Menu.Button className="bg-white rounded-full flex text-sm focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                      <span className="sr-only">Open user menu</span>
                      {props.authentication.user &&
                      props.authentication.user.imageUrl ? (
                        <img
                          src={props.authentication.user.imageUrl}
                          className="h-8 w-8 rounded-full bg-BE-purple text-white font-Poppins flex items-center justify-center"
                        />
                      ) : (
                        <div className="h-8 w-8 rounded-full bg-BE-purple text-white font-Poppins flex items-center justify-center">
                          {initials.firstInitial + " " + initials.secondInitial}
                        </div>
                      )}
                    </Menu.Button>
                  </div>
                  <Transition
                    as={Fragment}
                    enter="transition ease-out duration-100"
                    enterFrom="transform opacity-0 scale-95"
                    enterTo="transform opacity-100 scale-100"
                    leave="transition ease-in duration-75"
                    leaveFrom="transform opacity-100 scale-100"
                    leaveTo="transform opacity-0 scale-95"
                  >
                    <Menu.Items className="origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg py-1 bg-white ring-1 ring-black ring-opacity-5 focus:outline-none">
                      {userNavigation.map((item) => (
                        <Menu.Item key={item.name}>
                          {({ active }) => (
                            <button
                              onClick={() => handleClick(item.action)}
                              className={joinCssClassNames(
                                active ? "bg-gray-100" : "",
                                "block w-full text-left px-4 py-2 text-sm text-gray-700"
                              )}
                            >
                              {item.name}
                            </button>
                          )}
                        </Menu.Item>
                      ))}
                    </Menu.Items>
                  </Transition>
                </Menu>

                {/* <button
                  type="button"
                  className="flex bg-BE-purple p-1 rounded-full items-center justify-center text-white hover:bg-opacity-90 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                >
                  <PlusSmIcon className="h-6 w-6" aria-hidden="true" />
                  <span className="sr-only">Add file</span>
                </button> */}
              </div>
            </div>
          </div>
        </header>

        {/* Main content */}
        <main className="flex-1 relative overflow-y-auto focus:outline-none">
          <div className="py-6">
            <div className="max-w-7xl mx-auto px-4 sm:px-6 md:px-8">
              {/* Replace with your content */}
              {children}
              {/* /End replace */}
            </div>
          </div>
        </main>
      </div>
    </div>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    getUser: bindActionCreators(getUser, dispatch),
    authenticate: bindActionCreators(authenticate, dispatch),
    deauthenticate: bindActionCreators(deauthenticate, dispatch),
  };
};

const mapStateToProps = (state) => {
  return {
    authentication: state.authentication,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
