import Head from "next/head";
import Nav from "../nav";

const Layout = ({ children, title, description }) => (
  <div>
    <Head>
      <title>{title}</title>
      <meta charSet="utf-8" />
      <meta name="description" content={description} />
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
    </Head>
    <Nav pageTitle={title} />
    <div className="overflow-hidden font-Poppins"> {children}</div>
  </div>
);

export default Layout;
