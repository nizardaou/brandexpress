import { useEffect } from "react";
import Feedback from "./Feedback";
import { connect } from "react-redux";
import { getFeedbacks, updateFeedbacks } from "../redux/actions/authActions";
import { bindActionCreators } from "redux";

const Comments = ({
  result,
  user,
  authentication,
  getFeedbacks,
  updateFeedbacks,
}) => {
  useEffect(() => {
    if (authentication.token && result._id && authentication.feedbacks) {
      if (
        result.attachments.length === 2 &&
        authentication.feedbacks.length === 0
      ) {
        let firstFeedback = {
          expressUserID: result.expressUserID,
          packageID: result._id,
          input: false,
          body: "Please choose a direction above",
          orientation: "left",
          generalButton: false,
          formButtons: [],
          actionButtons: [],
        };
        updateFeedbacks(
          { details: firstFeedback, token: authentication.token },
          "update feedbacks"
        );
      }
    }
  }, [authentication.token, result, authentication.feedbacks]);

  useEffect(() => {
    if (!result) return;
    if (authentication.token && result._id) {
      getFeedbacks(
        { token: authentication.token, packageID: result._id },
        "feedbacks"
      );
    }
  }, [authentication.token, result]);

  return (
    authentication.feedbacks &&
    authentication.feedbacks.length > 0 && (
      <section aria-labelledby="notes-title">
        <div className="bg-white shadow sm:rounded-lg sm:overflow-hidden">
          <div className="divide-y divide-gray-200">
            <div className="px-4 py-5 sm:px-6">
              <h2
                id="notes-title"
                className="text-lg font-medium text-gray-900"
              >
                Feedback
              </h2>
            </div>
            <div className="px-4 py-6 sm:px-6">
              <ul role="list" className="space-y-8">
                {authentication.feedbacks.map((comment, index) => (
                  <Feedback
                    key={index}
                    feedback={comment}
                    packageID={result._id}
                    expressUserID={result.expressUserID}
                    token={authentication.token}
                    disabled={authentication.feedbacks.length > index + 1}
                  />
                ))}
              </ul>
            </div>
          </div>
        </div>
      </section>
    )
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    getFeedbacks: bindActionCreators(getFeedbacks, dispatch),
    updateFeedbacks: bindActionCreators(updateFeedbacks, dispatch),
  };
};
const mapStateToProps = (state) => {
  return {
    authentication: state.authentication,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Comments);
