import Dashboard from "../../components/Layouts/Dashboard";
import { connect } from "react-redux";
import PackagesThumbnails from "../../components/PackagesThumbnails";
import { packageEnums } from "../../constants/enum";

const dashboard = ({ authentication }) => {
  return (
    <Dashboard>
      {authentication.user && (
        <h1 className="text-3xl text-gray-600 font-bold">
          Welcome {authentication.user.name}
        </h1>
      )}
      <PackagesThumbnails />
    </Dashboard>
  );
};

const mapStateToProps = (state) => {
  return {
    authentication: state.authentication,
  };
};

export default connect(mapStateToProps, null)(dashboard);
