import Dashboard from "../../components/Layouts/Dashboard";
import PackagesThumbnails from "../../components/PackagesThumbnails";

const packages = () => {
  return (
    <Dashboard>
      <PackagesThumbnails breadCrumb />
    </Dashboard>
  );
};

export default packages;
