import Feed from "../../components/Feed";
import Dashboard from "../../components/Layouts/Dashboard";
import Comments from "../../components/Comments";
import PackageDetails from "../../components/PackageDetails";
import { connect } from "react-redux";
import { useEffect, useState } from "react";
import { getPackages } from "../../redux/actions/authActions";
import { bindActionCreators } from "redux";
import { useRouter } from "next/router";
import Breadcrumb from "../../components/BreadCrumb";

const PackageInfo = ({ authentication, getPackages }) => {
  let router = useRouter();
  const [filter, setFilter] = useState({ brandFilter: "", packageFilter: "" });
  const [result, setResult] = useState({});
  let [pages, setPages] = useState([]);

  useEffect(() => {
    if (!result) return;
    const { name, brandName } = result;

    // create breadCrumb array from result values
    let breadCrumb = [];
    breadCrumb.push({
      name: brandName,
      href: `/dashboard/packages?name=${brandName}`,
      current: false,
    });
    breadCrumb.push({
      name: name,
      href: `/dashboard/packageinfo?name=${brandName}&package=${name}`,
      current: true,
    });

    setPages(breadCrumb);
  }, [result]);

  useEffect(() => {
    if (authentication.token) {
      getPackages({ token: authentication.token }, "packages");
    }
  }, [authentication.token]);

  useEffect(() => {
    if (!router.isReady) return;

    let brandFilter = router.query.name;
    let packageFilter = router.query.package;

    setFilter({ brandFilter: brandFilter, packageFilter: packageFilter });
  }, [router.isReady]);

  useEffect(() => {
    if (!authentication.packages) return;
    setResult(authentication.packages.filter(handleArray)[0]);

    function handleArray(item) {
      return (
        item.name === filter.packageFilter &&
        item.brandName === filter.brandFilter
      );
    }
  }, [filter, authentication.packages]);
  return (
    <Dashboard>
      <div className="max-w-3xl sm:px-6">
        <Breadcrumb pages={pages} />
      </div>

      <div className="mt-8 max-w-3xl mx-auto grid grid-cols-1 gap-6 sm:px-6 lg:max-w-7xl lg:grid-flow-col-dense lg:grid-cols-3">
        <div className="space-y-6 lg:col-start-1 lg:col-span-2">
          {/* Description list*/}
          <PackageDetails
            result={result ? result : {}}
            user={authentication.user}
          />
          {/* Comments*/}
          <Comments result={result ? result : {}} user={authentication.user} />
        </div>
        <Feed result={result ? result : {}} />
      </div>
    </Dashboard>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    getPackages: bindActionCreators(getPackages, dispatch),
  };
};
const mapStateToProps = (state) => {
  return {
    authentication: state.authentication,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PackageInfo);
