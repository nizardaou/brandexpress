import SearchBar from "../../components/SearchBar";

import { useEffect, useState } from "react";
import Dashboard from "../../components/Layouts/Dashboard";
import axios from "axios";

const steps = () => {
  const [state, setState] = useState({
    data: [],
    keyword: "",
    results: [],
  });

  const matchName = (name, keyword) => {
    var keyLen = keyword.length;
    name = name.toLowerCase().substring(0, keyLen);
    if (keyword == "") return false;
    return name == keyword.toLowerCase();
  };

  useEffect(() => {
    const fetchData = async () => {
      let response = await axios.get("/api/admin/search");
      setState((prevValue) => {
        return {
          ...prevValue,
          data: response.data,
        };
      });
    };
    fetchData();
  }, []);

  const onSearch = (text) => {
    let { data } = state;

    //check to see if we found a match, if so, add it to results
    var results = data.filter((item) => true == matchName(item, text));

    //update state changes
    setState((prevValue) => {
      return {
        ...prevValue,
        results: results,
      };
    });
  };

  const updateField = (field, value, update = true) => {
    if (update) onSearch(value);
    setState((prevValue) => {
      return {
        ...prevValue,
        [field]: value,
      };
    });
  };

  return (
    <Dashboard>
      <SearchBar
        results={state.results}
        keyword={state.keyword}
        updateField={updateField}
        modify="steps"
      />
    </Dashboard>
  );
};

export default steps;
