import Dashboard from "../../components/Layouts/Dashboard";
import { connect } from "react-redux";
import { useEffect, useState } from "react";
import { updateUser } from "../../redux/actions/authActions";
import { bindActionCreators } from "redux";
import { getCloudinaryUrl } from "../../utils/getCloudinaryUrl";

const profile = ({ authentication, updateUser }) => {
  const [userDetails, setUserDetails] = useState({
    pendingEmail: "",
    phone: "",
    name: "",
    imageUrl: "",
  });

  const [image, setImage] = useState(null);
  const [preview, setPreview] = useState("");
  const [imageChanged, setImageChanged] = useState(false);

  useEffect(() => {
    if (authentication.user)
      setUserDetails({
        email: authentication.user.email,
        pendingEmail: authentication.user.email,
        phone: authentication.user.phone,
        name: authentication.user.name,
        imageUrl: authentication.user.imageUrl
          ? authentication.user.imageUrl
          : "",
      });
  }, [authentication.user]);

  useEffect(() => {
    if (!imageChanged) {
      setPreview(undefined);
      return;
    } else {
      const objectUrl = URL.createObjectURL(image);
      setPreview(objectUrl);
      // free memory when ever this component is unmounted
      return () => URL.revokeObjectURL(objectUrl);
    }
    // eslint-disable-next-line
  }, [image]);

  useEffect(() => {
    if (userDetails.imageUrl === "") return;
    updateUser({ userDetails, token: authentication.token }, "update user");
  }, [userDetails.imageUrl]);

  const onChangeImage = (e) => {
    setImage(e.target.files[0]);
    setImageChanged(true);
  };

  const handleChange = (e) => {
    const { name, value } = e.target;

    setUserDetails((prevValue) => {
      return {
        ...prevValue,
        [name]: value,
      };
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    let imageUrl;

    if (imageChanged) {
      imageUrl = await getCloudinaryUrl(image, userDetails.name);
    }
    setImageChanged(false);
    setUserDetails((prevValue) => {
      return {
        ...prevValue,
        imageUrl: imageUrl,
      };
    });
  };

  return (
    <Dashboard>
      {authentication.isAuthenticated && !authentication.loading && (
        <form
          onSubmit={handleSubmit}
          className="space-y-8 divide-y divide-gray-200"
        >
          <div className="space-y-8 divide-y divide-gray-200 sm:space-y-5">
            <div className="pt-8 space-y-6 sm:pt-10 sm:space-y-5">
              <div>
                <h3 className="text-lg leading-6 font-medium text-gray-900">
                  Personal Information
                </h3>
                <p className="mt-1 max-w-2xl text-sm text-gray-500">
                  Use a permanent address where you can receive mail.
                </p>
              </div>
              <div className="space-y-6 sm:space-y-5">
                <div className="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5">
                  <label
                    htmlFor="name"
                    className="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2"
                  >
                    Full Name
                  </label>
                  <div className="mt-1 sm:mt-0 sm:col-span-2">
                    <input
                      type="text"
                      name="name"
                      id="name"
                      onChange={handleChange}
                      autoComplete="given-name"
                      value={userDetails.name}
                      className="max-w-lg block w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:max-w-xs sm:text-sm border-gray-300 rounded-md"
                    />
                  </div>
                </div>

                <div className="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5">
                  <label
                    htmlFor="email"
                    className="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2"
                  >
                    Email address
                  </label>
                  <div className="mt-1 flex sm:mt-0 sm:col-span-2">
                    <input
                      id="email"
                      name="pendingEmail"
                      type="email"
                      autoComplete="email"
                      onChange={handleChange}
                      value={userDetails.pendingEmail}
                      className="block max-w-lg w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm border-gray-300 rounded-md"
                    />
                  </div>
                </div>

                <div className="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5">
                  <label
                    htmlFor="phone"
                    className="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2"
                  >
                    Phone Number
                  </label>
                  <div className="mt-1 sm:mt-0 sm:col-span-2">
                    <input
                      id="phone"
                      name="phone"
                      type="tel"
                      autoComplete="tel"
                      onChange={handleChange}
                      value={userDetails.phone}
                      className="block max-w-lg w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
                    />
                  </div>
                </div>

                <div className="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5">
                  <label
                    htmlFor="photo"
                    className="block text-sm font-medium text-gray-700"
                  >
                    Photo
                  </label>
                  <div className="mt-1 flex items-center">
                    <span className="h-12 w-12 rounded-full overflow-hidden bg-gray-100">
                      {preview || userDetails.imageUrl ? (
                        <img src={!preview ? userDetails.imageUrl : preview} />
                      ) : (
                        <svg
                          className="h-full w-full text-gray-300"
                          fill="currentColor"
                          viewBox="0 0 24 24"
                        >
                          <path d="M24 20.993V24H0v-2.996A14.977 14.977 0 0112.004 15c4.904 0 9.26 2.354 11.996 5.993zM16.002 8.999a4 4 0 11-8 0 4 4 0 018 0z" />
                        </svg>
                      )}
                    </span>
                    <input
                      type="file"
                      id="image"
                      name="image"
                      accept="image/*"
                      onChange={onChangeImage}
                      className="ml-5 bg-white py-2 px-3 border border-gray-300 rounded-md shadow-sm text-sm leading-4 font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="pt-5">
            <div className="flex justify-end">
              <button
                type="submit"
                className="ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-BE-purple hover:bg-opacity-90 focus:outline-none"
              >
                Save
              </button>
            </div>
          </div>
        </form>
      )}
    </Dashboard>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateUser: bindActionCreators(updateUser, dispatch),
  };
};
const mapStateToProps = (state) => {
  return {
    authentication: state.authentication,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(profile);
