import Dashboard from "../../components/Layouts/Dashboard";
import BrandsList from "../../components/BrandsList";

const packages = (props) => {
  return (
    <Dashboard>
      <h1 className="text-3xl text-gray-600 font-bold mb-5">My brands</h1>
      <BrandsList />
    </Dashboard>
  );
};

export default packages;
