import Dashboard from "../../components/Layouts/Dashboard";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import PackagesThumbnails from "../../components/PackagesThumbnails";

const results = () => {
  const [filter, setFilter] = useState("");

  let router = useRouter();
  useEffect(() => {
    if (!router.isReady) return;

    let filter = router.query.search;

    setFilter(filter);
  }, [router.isReady, router.query]);

  return (
    <Dashboard>
      <PackagesThumbnails find={filter} />
    </Dashboard>
  );
};

export default results;
