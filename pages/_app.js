import "tailwindcss/tailwind.css";
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import FormProvider from "../context";
import "../styles/stripe.css";
import { wrapper } from "../redux/store/store";
import { useEffect } from "react";
import { reauthenticate } from "../redux/actions/authActions";
import { getCookie } from "../utils/cookie";
import { useStore } from "react-redux";
import { GTM_ID, pageview } from "../utils/gtm";
import { useRouter } from "next/router";
import Script from "next/script";
import { hotjar } from "react-hotjar";

function MyApp({ Component, pageProps }) {
  let store = useStore();
  const router = useRouter();

  useEffect(() => {
    let token = getCookie("token");
    if (token) store.dispatch(reauthenticate(token));
    if (process.env.NODE_ENV === "production") hotjar.initialize(2684693, 6);
  }, []);

  useEffect(() => {
    if (process.env.NODE_ENV === "production") {
      router.events.on("routeChangeComplete", pageview);
      return () => {
        router.events.off("routeChangeComplete", pageview);
      };
    }
  }, [router.events]);
  return (
    <>
      {process.env.NODE_ENV === "production" && (
        <Script
          strategy="afterInteractive"
          dangerouslySetInnerHTML={{
            __html: `
            (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer', '${GTM_ID}');
          `,
          }}
        />
      )}
      <FormProvider>
        <Component {...pageProps} />
      </FormProvider>
    </>
  );
}

export default wrapper.withRedux(MyApp);
