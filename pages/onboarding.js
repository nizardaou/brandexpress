import { useEffect, useState } from "react";
import Wizard from "../components/wizard";
import {
  LogoStyle,
  Colors,
  Territory,
  CompanyType,
  TextInput,
  InputSet,
  Packages,
  LandingPage,
} from "../components/forms";
import Submit from "../components/submit";
import Layout from "../components/Layouts/Layout";
import { useFormData } from "../context";
import { useRouter } from "next/dist/client/router";
import companyCategories from "../constants/companycategories";
import ShopifyRadio from "../components/forms/ShopifyRadio";
import ShopifyCheckboxes from "../components/forms/ShopifyCheckboxes";

const textInputs = [
  {
    type: "text",
    name: "name",
    title: "What’s the name of your brand?",
    placeholder: "Ex: Awesome Brand",
  },
  {
    type: "text",
    name: "slogan",
    title: "What is your slogan? (Optional)",
    placeholder: "Ex: Play to learn",
  },
];

const inputSet1 = [
  {
    type: "textarea",
    name: "companyPurpose",
    title: "What is the purpose of your company?",
    placeholder: "Define the Company in a single declarative sentence.",
  },
  {
    type: "textarea",
    name: "painPoint",
    title: "Who is your customer and what is his pain point?",
    placeholder:
      "Describe -as briefly as possible- your core customer, what is his pain/problem and how does he solve it today.",
  },
];

const inputSet2 = [
  {
    type: "textarea",
    name: "bigIdea",
    title: "What is your Big Idea?",
    placeholder:
      "Describe your company disruptive / innovative / novel idea that makes your customer’s life better.",
  },
  {
    type: "text",
    name: "currentIdentity",
    title: "Current identity (Optional)",
    placeholder:
      "Please upload key assets of your current identity, current pitch deck, product images, etc.",
  },
  {
    type: "text",
    name: "website",
    title: "Do you have a website? (Optional)",
    placeholder: "Ex: https://brandexpress.ae",
  },
  {
    name: "socialPresence",
    dropDown: true,
  },
];

const inputSet3 = [
  {
    type: "textarea",
    name: "shopifyDescription",
    title: "Tell us more about your target market and audience.",
    placeholder:
      "Ex: I will sell fashion clothing to a young female audience based in the UAE.",
  },
];

const logoTerritories = [
  {
    name: "Modern",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1635505524/onboarding/logostyles/t1_gef61f.jpg",
    alt: "",
  },
  {
    name: "Classic",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1635505524/onboarding/logostyles/t2_go5nbr.jpg",
    alt: "",
  },
  {
    name: "Bold",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1635505524/onboarding/logostyles/t3_m9yfp6.jpg",
    alt: "",
  },
  {
    name: "Vintage",
    image:
      "https://res.cloudinary.com/brandexpress/image/upload/v1635505524/onboarding/logostyles/t4_pxskj5.jpg",
    alt: "",
  },
];

const businessModel = [
  { id: 1, name: "Business to Consumer (B2C)" },
  { id: 2, name: "Business to Business (B2B)" },
  { id: 3, name: "Business to Government (B2G)" },
  { id: 4, name: "Consumer to Consumer (C2C)" },
  { id: 5, name: "Consumer to Business (C2B)" },
  { id: 6, name: "I don’t know yet" },
];

const brandsNumber = [
  { id: 1, name: "One brand" },
  { id: 2, name: "Multiple brands" },
  { id: 3, name: "I don’t know yet" },
];

const productsNumber = [
  { id: 1, name: "One item" },
  { id: 2, name: "A few items" },
  { id: 3, name: "Dozens of items" },
  { id: 4, name: "Hundreds of items" },
  { id: 5, name: "I don’t know yet" },
];

const onboarding = () => {
  const { data, submitCounter, setSubmitCounter, setFormValues } =
    useFormData();
  const [formStep, setFormStep] = useState(0);
  const [coreNext, setCoreNext] = useState(null);
  const [shopifyNext, setShopifyNext] = useState(null);
  const [submitPrev, setSubmitPrev] = useState(null);
  const [landingPrev, setLandingPrev] = useState(null);

  const router = useRouter();

  useEffect(() => {
    setFormValues({ name: router.query.brand });
    router.push("/onboarding", undefined, { shallow: true });
  }, []);

  useEffect(() => {
    if (data.packages) {
      if (data.packages.includes("3") && data.packages.includes("5")) {
        setCoreNext(1);
        setLandingPrev(1);
        setSubmitPrev(1);
        setShopifyNext(1);
      } else if (data.packages.includes("3") && !data.packages.includes("5")) {
        setCoreNext(5);
        setLandingPrev(5);
        setSubmitPrev(1);
        setShopifyNext(1);
      } else if (!data.packages.includes("3") && data.packages.includes("5")) {
        setCoreNext(1);
        setLandingPrev(1);
        setSubmitPrev(2);
        setShopifyNext(2);
      } else {
        setSubmitPrev(6);
        setCoreNext(6);
        setShopifyNext(1);
        setLandingPrev(1);
      }
    }
  }, [data.packages]);

  const nextFormStep = (nextStep, step) => {
    window.scrollTo(0, 0);
    setSubmitCounter(submitCounter + step);
    setFormStep((currentStep) => currentStep + nextStep);
  };
  const prevFormStep = (prevStep, step) => {
    window.scrollTo(0, 0);
    setSubmitCounter(submitCounter - step);
    setFormStep((currentStep) => currentStep - prevStep);
  };

  // useEffect(() => {
  //   console.log("data", data);
  // }, [data]);

  return (
    <Layout title="On Boarding">
      <Wizard currentStep={formStep}>
        {formStep === 0 && (
          <Packages
            stepNumber={0}
            formStep={formStep}
            nextFormStep={() => nextFormStep(1, 1)}
          />
        )}
        {formStep === 1 && (
          <TextInput
            packageName="Brand Foundation"
            heading="Brand name and slogan"
            stepNumber={1}
            details={textInputs}
            nextFormStep={() => nextFormStep(1, 1)}
            prevFormStep={() => prevFormStep(1, 1)}
          />
        )}

        {formStep === 2 && (
          <CompanyType
            packageName="Brand Foundation"
            stepNumber={2}
            nextFormStep={() => nextFormStep(1, 1)}
            prevFormStep={() => prevFormStep(1, 1)}
            heading="Business category"
            label="Which sector or industry are you in? Select an option."
            name="businessCategory"
            otherName="specificField"
            options={companyCategories}
          />
        )}

        {formStep === 3 && (
          <InputSet
            packageName="Brand Foundation"
            stepNumber={3}
            details={inputSet1}
            nextFormStep={() => nextFormStep(1, 1)}
            prevFormStep={() => prevFormStep(1, 1)}
          />
        )}
        {formStep === 4 && (
          <InputSet
            packageName="Brand Foundation"
            stepNumber={4}
            currentStep={formStep === 4}
            details={inputSet2}
            nextFormStep={() => nextFormStep(1, 1)}
            prevFormStep={() => prevFormStep(1, 1)}
          />
        )}
        {formStep === 5 && (
          <LogoStyle
            packageName="Brand Foundation"
            stepNumber={5}
            nextFormStep={() => nextFormStep(1, 1)}
            prevFormStep={() => prevFormStep(1, 1)}
          />
        )}
        {formStep === 6 && (
          <Territory
            packageName="Brand Foundation"
            stepNumber={6}
            logoTerritories={logoTerritories}
            nextFormStep={() => nextFormStep(1, 1)}
            prevFormStep={() => prevFormStep(1, 1)}
          />
        )}
        {formStep === 7 && (
          <Colors
            packageName="Brand Foundation"
            stepNumber={7}
            nextFormStep={() => nextFormStep(coreNext, 1)}
            prevFormStep={() => prevFormStep(1, 1)}
            heading="Color style"
          />
        )}
        {formStep === 8 && data.packages.includes("5") && (
          <ShopifyCheckboxes
            packageName="Branded Shopify E-store Theme"
            heading="E-commerce business model"
            name="shopifyBusinessModel"
            title="How do you sell your products? Select at least one business model."
            stepNumber={8}
            options={businessModel}
            nextFormStep={() => nextFormStep(1, 1)}
            prevFormStep={() => prevFormStep(1, 1)}
          />
        )}
        {formStep === 9 && data.packages.includes("5") && (
          <ShopifyRadio
            packageName="Branded Shopify E-store Theme"
            heading="Tell us more about your e-store"
            name="brandsNumber"
            title="How many brands do you sell? Select one option."
            stepNumber={9}
            options={brandsNumber}
            nextFormStep={() => nextFormStep(1, 1)}
            prevFormStep={() => prevFormStep(1, 1)}
          />
        )}
        {formStep === 10 && data.packages.includes("5") && (
          <ShopifyRadio
            packageName="Branded Shopify E-store Theme"
            heading="Tell us more about your products"
            name="productsNumber"
            title="How many products do you sell? Select one option."
            stepNumber={10}
            options={productsNumber}
            nextFormStep={() => nextFormStep(1, 1)}
            prevFormStep={() => prevFormStep(1, 1)}
          />
        )}
        {formStep === 11 && data.packages.includes("5") && (
          <InputSet
            packageName="Branded Shopify E-store Theme"
            heading="Shop Description"
            stepNumber={11}
            details={inputSet3}
            nextFormStep={() => nextFormStep(shopifyNext, 1)}
            prevFormStep={() => prevFormStep(1, 1)}
          />
        )}
        {formStep === 12 && data.packages.includes("3") && (
          <LandingPage
            packageName="Branded Landing Page Template"
            stepNumber={12}
            nextFormStep={() => nextFormStep(1, 1)}
            prevFormStep={() => prevFormStep(landingPrev, 1)}
          />
        )}
        {formStep === 13 && (
          <Submit
            stepNumber={13}
            nextFormStep={() => nextFormStep(1, 1)}
            prevFormStep={() => prevFormStep(submitPrev, 1)}
          />
        )}
      </Wizard>
    </Layout>
  );
};

export default onboarding;
