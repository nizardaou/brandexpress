import Layout from "../components/Layouts/Layout";
import Hero from "../components/hero";
import Cards from "../components/cards";
import { FadeSliderMobile, FadeSlider } from "../components/fadeslider";
import Pricing from "../components/pricing";
import FAQ from "../components/faq";
import Footer from "../components/footer";
import NewsLetter from "../components/newsletter";

function Index() {
  return (
    <Layout
      title="Brand Express - Startup Branding Agency in the UAE"
      description="Brand Express is an affordable branding agency based in the UAE and specialized in branding for Arab and MENA startups and small businesses."
    >
      <Hero />
      <Cards />
      <FadeSlider />
      <FadeSliderMobile />
      <Pricing />
      <FAQ />
      <Footer />
      <NewsLetter />
    </Layout>
  );
}

export default Index;
