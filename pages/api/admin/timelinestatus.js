import Cors from "cors";
import initMiddleware from "../../../middleware/init-middleware";
import dbConnect from "../../../middleware/mongodb";
import Package from "../../../models/package";
import moment from "moment";
// Initialize the cors middleware
const cors = initMiddleware(
  // You can read more about the available options here: https://github.com/expressjs/cors#configuration-options
  Cors({
    // Only allow requests with GET, POST and OPTIONS
    methods: ["GET", "POST", "OPTIONS"],
  })
);

const handler = async (req, res) => {
  //initialize database
  await dbConnect();
  //initialize cors
  await cors(req, res);

  // check request method type
  if (req.method === "POST") {
    // check if request body is empty
    if (!req.body) {
      res.status(400);
      res.send("Request body is empty");
      return;
    }

    try {
      let { packageID, stepName, status } = req.body;

      let newStatus;

      if (status) {
        newStatus = stepName;

        // changed package status
        let update = { status: newStatus };
        await Package.updateOne(
          {
            _id: packageID,
          },
          { $set: update }
        ).exec();
      }

      // query packages such that package id is equal to packageID, and steps array contains object of type equal to stepName
      const currentDate = new Date();
      let submittionDate = moment(currentDate).format("MMM DD");

      let updatedStepID = await Package.updateOne(
        {
          _id: packageID,
          steps: { $elemMatch: { type: stepName } },
        },
        {
          $set: {
            "steps.$.status": status,
            "steps.$.date": status ? submittionDate : "",
          },
        }
      ).exec();

      res.status(200).send(updatedStepID);
    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server error");
    }
  }
};

export default handler;
