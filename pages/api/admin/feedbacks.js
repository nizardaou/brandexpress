import Cors from "cors";

import initMiddleware from "../../../middleware/init-middleware";
import dbConnect from "../../../middleware/mongodb";
const jwt = require("jsonwebtoken");
import Package from "../../../models/package";

import Feedback from "../../../models/feedback";

// Initialize the cors middleware
const cors = initMiddleware(
  // You can read more about the available options here: https://github.com/expressjs/cors#configuration-options
  Cors({
    // Only allow requests with GET, POST and OPTIONS
    methods: ["GET", "POST", "OPTIONS"],
  })
);

const handler = async (req, res) => {
  //initialize database
  await dbConnect();
  //initialize cors
  await cors(req, res);

  // check request method type
  if (req.method === "GET") {
    try {
      const { brandName } = req.query;
      const find = {
        brandName,
      };

      const results = await Package.find(find).exec();

      let result = results.map((result) => result._id);
      if (result && result.length > 0)
        Feedback.find({
          packageID: {
            $in: result,
          },
        })
          .populate("packageID", ["name"])
          .populate("expressUserID", ["name"])
          .exec(function (err, feedback) {
            if (!feedback) {
              return res.status(400).json({ msg: "no feedbacks" });
            }

            res.status(200).send(feedback);
          });
    } catch (err) {
      console.log(err);
      res.status(403);
      res.send("Invalid auth credentials.");
      return;
    }
  }
};

export default handler;
