import Cors from "cors";
import initMiddleware from "../../../middleware/init-middleware";
import dbConnect from "../../../middleware/mongodb";
import Package from "../../../models/package";

// Initialize the cors middleware
const cors = initMiddleware(
  // You can read more about the available options here: https://github.com/expressjs/cors#configuration-options
  Cors({
    // Only allow requests with GET, POST and OPTIONS
    methods: ["GET", "POST", "OPTIONS"],
  })
);

const handler = async (req, res) => {
  //initialize database
  await dbConnect();
  //initialize cors
  await cors(req, res);

  // check request method type
  if (req.method === "GET") {
    try {
      const results = await Package.find({ brandName: req.query.package });

      res.status(200).send(results);
    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server error");
    }
  }
};

export default handler;
