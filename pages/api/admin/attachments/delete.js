import Cors from "cors";
import initMiddleware from "../../../../middleware/init-middleware";
import dbConnect from "../../../../middleware/mongodb";
import Package from "../../../../models/package";

// Initialize the cors middleware
const cors = initMiddleware(
  // You can read more about the available options here: https://github.com/expressjs/cors#configuration-options
  Cors({
    // Only allow requests with GET, POST and OPTIONS
    methods: ["GET", "POST", "OPTIONS"],
  })
);

const handler = async (req, res) => {
  //initialize database
  await dbConnect();
  //initialize cors
  await cors(req, res);

  // check request method type
  if (req.method === "POST") {
    // check if request body is empty
    if (!req.body) {
      res.status(400);
      res.send("Request body is empty");
      return;
    }

    try {
      const { packageID, attachment } = req.body;

      console.log(packageID, attachment);

      // query packages such that package id is equal to packageID, and add push link and description to attachments
      let updatedPackage = await Package.updateOne(
        {
          _id: packageID,
        },
        { $pull: { attachments: attachment } }
      ).exec();

      res.status(200).send(updatedPackage);
    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server error");
    }
  }
};

export default handler;
