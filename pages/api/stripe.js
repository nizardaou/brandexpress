import Stripe from "stripe";
import Cors from "cors";
import initMiddleware from "../../middleware/init-middleware";
import expressUser from "../../models/expressUser";
import dbConnect from "../../middleware/mongodb";
import Package from "../../models/package";
import Payment from "../../models/payment";
import sendEmail from "../../utils/sendEmail";
const jwt = require("jsonwebtoken");
import { userEnums, paymentEnums, packageEnums } from "../../constants/enum";
import moment from "moment";
const path = require("path");
const fs = require("fs");
// import signup email template file
const signUpTemplateSource = fs.readFileSync(
  path.join(process.cwd(), "/emails/signup.hbs"),
  "utf8"
);

// import brandexpress notification email template file
const newBrandTemplateSource = fs.readFileSync(
  path.join(process.cwd(), "/emails/newBrand.hbs"),
  "utf8"
);

// Initialize the cors middleware
const cors = initMiddleware(
  // You can read more about the available options here: https://github.com/expressjs/cors#configuration-options
  Cors({
    // Only allow requests with GET, POST and OPTIONS
    methods: ["GET", "POST", "OPTIONS"],
  })
);

// create mew stripe payment handler
const stripe = new Stripe(process.env.STRIPE_SECRET_TEST);

const handler = async (req, res) => {
  //initialize database
  await dbConnect();
  //initialize cors
  await cors(req, res);

  // check request method type
  if (req.method === "POST") {
    // check if request body is empty
    if (!req.body) {
      res.status(400);
      res.send("Request body is empty");
      return;
    }
    let { userDetails, packagesDetails, amount, id } = req.body;

    // get email from userDetails
    const { email } = userDetails;

    try {
      // See if user exists
      let user = await expressUser.findOne({ email });

      if (user) {
        // if user exists send error
        res.json({
          message: "Email already in use.",
          success: false,
        });
        res.end();
        return;
      }

      await stripe.paymentIntents.create({
        amount: amount * 100,
        currency: "AED",
        description: "Brand Express",
        payment_method: id,
        confirm: true,
      });

      // handle request by creating fields for user, payment and packages
      let response = await handleRequest(userDetails, packagesDetails, res);
      // get id from response
      let userID = response._id;

      // response payment successful
      res.json({
        message: userID,
        success: true,
      });
      res.end();
      return;
    } catch (error) {
      console.log("error", error);
      res.json({
        message: `Payment failed: ${error.raw.message}`,
        success: false,
      });
      res.end();
    }
  }
};

const handleRequest = async (user, packages, res) => {
  try {
    let companyName = packages[0].brandName;
    // call register user function and return the userID
    let userID = await registerUser(user, companyName, res);

    if (userID) {
      // create packages record
      let packagesResponse = await createPackagesRecord(packages, userID, res);

      let amount = packagesResponse.length * 999;
      // create payment record
      await createPaymentRecord(packagesResponse, userID, amount, res);

      return userID;
    }
  } catch (error) {
    console.error(error);
    res.json({
      message: "Server Error",
      success: false,
    });
    res.end();
    return;
  }
};

// regiter user function definition
const registerUser = async (userDetails, companyName, res) => {
  const { name, email, phone } = userDetails;

  try {
    // See if user exists
    let user = await expressUser.findOne({ email });

    // insert user
    user = new expressUser({
      name,
      email,
      phone,
      status: userEnums.status.verified,
    });

    user.save();

    // create token expiration date
    const expirationDate = new Date();
    expirationDate.setHours(new Date().getHours() + 1);
    // Return jsonwebtoken
    const payload = {
      email,
      expirationDate,
    };

    jwt.sign(
      payload,
      process.env.JWT_TOKEN,
      { expiresIn: 360000 },
      async (err, token) => {
        if (err) throw err;
        const link = `${process.env.API}/api/user/account?token=${token}`;
        console.log(link);
        await sendEmail(link, name, companyName, email, signUpTemplateSource);
      }
    );

    return user._id;
  } catch (error) {
    console.log(error);
    res.json({
      message: "Server Error",
      success: false,
    });
    res.end();
    return;
  }
};

const createPackagesRecord = async (packagesDetails, userID, res) => {
  try {
    // create an empty packages array
    let packagesID = [];
    const currentDate = new Date();

    let submittionDate = moment(currentDate).format("MMM DD");

    const coreTimeline = [
      {
        type: packageEnums.coreStatus.started,
        status: true,
        date: submittionDate,
      },
      {
        type: packageEnums.coreStatus.directionSubmitted,
        status: false,
        date: "",
      },
      {
        type: packageEnums.coreStatus.directionChosen,
        status: false,
        date: "",
      },
      {
        type: packageEnums.coreStatus.feedbackSubmitted,
        status: false,
        date: "",
      },
      {
        type: packageEnums.coreStatus.artworkReady,
        status: false,
        date: "",
      },
    ];

    const addonTimeline = [
      {
        type: packageEnums.addonStatus.pending,
        status: true,
        date: submittionDate,
      },
      {
        type: packageEnums.addonStatus.started,
        status: false,
        date: "",
      },
      {
        type: packageEnums.addonStatus.inProgress,
        status: false,
        date: "",
      },
      {
        type: packageEnums.addonStatus.ready,
        status: false,
        date: "",
      },
    ];

    let allWizardData = [];
    let brandName;

    packagesDetails.forEach((item) => {
      brandName = item.brandName;
      allWizardData.push(item.wizardResponseData);
      let createdPackage = new Package({
        name: item.name,
        description: item.description,
        status:
          item.type === "CORE"
            ? packageEnums.coreStatus.started
            : packageEnums.addonStatus.pending,
        steps: item.type === "CORE" ? coreTimeline : addonTimeline,
        attachments: [],
        comments: [],
        brandName: item.brandName,
        expressUserID: userID,
        wizardResponseData: item.wizardResponseData,
        type: item.type,
        price: item.price,
      });

      createdPackage.save();

      packagesID.push(createdPackage._id);
    });

    // create an array that will contain all data
    let dataArray = [];

    // get wizard data and push them to an array and then join this array
    allWizardData.forEach((element) => {
      for (const [key, value] of Object.entries(element)) {
        if (key === "companyMood") {
          let moods = [];
          Object.entries(value).forEach((items) => {
            items.forEach((item) => {
              if (typeof item === "object") {
                item.forEach((mood) => moods.push(mood));
              } else {
                moods.push(item);
              }
            });
          });
          dataArray.push(`Company Moods: ${moods.toString()}`);
        } else dataArray.push(`${key}: ${value}`);
      }
    });

    // send email to admin containing form details
    await sendEmail(
      "",
      "",
      brandName,
      "team@brandexpress.ae",
      newBrandTemplateSource,
      dataArray.join("\r\n")
    );

    // return the array of packages
    return packagesID;
  } catch (error) {
    console.log(error);
    res.json({
      message: "Server Error",
      success: false,
    });
    res.end();
    return;
  }
};

const createPaymentRecord = async (packages, userID, amount, res) => {
  try {
    // create payment record from packages ids
    let payment = new Payment({
      packagesID: packages,
      expressUserID: userID,
      amount: amount,
      status: paymentEnums.status.completed,
    });
    payment.save();
    return payment._id;
  } catch (error) {
    console.log(error);
    res.json({
      message: "Server Error",
      success: false,
    });
    res.end();
    return;
  }
};

export default handler;
