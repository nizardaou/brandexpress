import Cors from "cors";
import initMiddleware from "../../../middleware/init-middleware";
import expressUser from "../../../models/expressUser";
import dbConnect from "../../../middleware/mongodb";
import sendEmail from "../../../utils/sendEmail";
const path = require("path");
const fs = require("fs");
// import email template file
const emailTemplateSource = fs.readFileSync(
  path.join(process.cwd(), "/emails/update-email.hbs"),
  "utf8"
);

const jwt = require("jsonwebtoken");

// Initialize the cors middleware
const cors = initMiddleware(
  // You can read more about the available options here: https://github.com/expressjs/cors#configuration-options
  Cors({
    // Only allow requests with GET, POST and OPTIONS
    methods: ["GET", "POST", "OPTIONS"],
  })
);

const handler = async (req, res) => {
  //initialize database
  await dbConnect();
  //initialize cors
  await cors(req, res);

  // check request method type
  if (req.method === "POST") {
    // check if request body is empty
    if (!("authorization" in req.headers)) {
      res.statusCode = 401;
      res.end("Authorization header missing");
    }

    try {
      // get email from token
      const auth = await req.headers.authorization;
      const bearer = auth.split(" ");
      const oldToken = bearer[1];

      let newProfile = req.body.userDetails;

      if (newProfile.pendingEmail !== newProfile.email) {
        // See if email is already used
        let user = await expressUser.findOne({
          email: newProfile.pendingEmail,
        });

        if (user) {
          res.status(400).json({
            message: "user already exists",
            success: false,
          });
          return;
        }
      }

      // query databases to find existing user and update based on new data
      const filter = { email: newProfile.email };

      const update = {
        $set: newProfile,
      };

      const options = { new: true };

      let updatedUser = await expressUser
        .findOneAndUpdate(filter, update, options)
        .exec();

      // create new jwt token and send with user data to redux action to update token and user
      let newEmail = updatedUser.pendingEmail;
      let oldEmail = updatedUser.email;
      let name = updatedUser.name;

      if (newEmail === oldEmail) {
        console.log("email didn't change");
        // new email is equal to old email => don't create a new token => send old token + update user details
        res.status(200).send({ token: oldToken, updatedUser });
      } else {
        console.log("email changed");
        // new email is different than old email and email not already used => create new token => send new token + update user details
        const expirationDate = new Date();
        expirationDate.setHours(new Date().getHours() + 1);
        // Return jsonwebtoken
        const payload = {
          email: newEmail,
          expirationDate,
        };

        jwt.sign(
          payload,
          process.env.JWT_TOKEN,
          { expiresIn: 360000 },
          async (err, token) => {
            if (err) throw err;
            const link = `${process.env.API}/api/user/update?token=${token}`;
            await sendEmail(link, name, "", newEmail, emailTemplateSource);
            res.status(200).send({ token: oldToken, updatedUser });
          }
        );
      }
    } catch (err) {
      console.error(err.message);
      res.status(500).json({
        message: "server error",
        success: false,
      });
    }
  }

  if (req.method === "GET") {
    const token = req.query.token;
    if (!token) {
      res.status(403);
      res.send("Can't verify user.");
      return;
    }
    let decoded;
    try {
      decoded = jwt.verify(token, process.env.JWT_TOKEN);
    } catch (err) {
      console.log(err);
      res.status(403);
      res.send("Invalid auth credentials.");

      return;
    }
    if (
      !decoded.hasOwnProperty("email") ||
      !decoded.hasOwnProperty("expirationDate")
    ) {
      res.status(403);
      res.send("Invalid auth credentials.");
      return;
    }
    const { email, expirationDate } = decoded;

    if (expirationDate < new Date()) {
      res.status(403);
      res.send("Token has expired.");
      return;
    }

    const filter = { pendingEmail: email };
    const update = { email: email };
    const options = {
      new: true,
    };

    const updatedUser = await expressUser
      .findOneAndUpdate(filter, update, options)
      .exec();

    res.status(200);
    res.redirect(`/dashboard/profile?token=${token}`);
  }
};

export default handler;
