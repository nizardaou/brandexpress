import Cors from "cors";
import initMiddleware from "../../../middleware/init-middleware";
import expressUser from "../../../models/expressUser";
import dbConnect from "../../../middleware/mongodb";
import sendEmail from "../../../utils/sendEmail";
const path = require("path");
const fs = require("fs");
// import email template file
const emailTemplateSource = fs.readFileSync(
  path.join(process.cwd(), "/emails/login.hbs"),
  "utf8"
);
const jwt = require("jsonwebtoken");

// Initialize the cors middleware
const cors = initMiddleware(
  // You can read more about the available options here: https://github.com/expressjs/cors#configuration-options
  Cors({
    // Only allow requests with GET, POST and OPTIONS
    methods: ["GET", "POST", "OPTIONS"],
  })
);

const handler = async (req, res) => {
  //initialize database
  await dbConnect();
  //initialize cors
  await cors(req, res);

  // check request method type
  if (req.method === "POST") {
    // check if request body is empty
    if (!req.body) {
      res.status(400);
      res.send("Request body is empty");
      return;
    }

    try {
      let email = req.body.email;
      // See if user exists
      let user = await expressUser.findOne({ email });

      if (!user) {
        res.status(400).send("invalid credentials");
        return;
      }

      let name = user.name;

      // Return jsonwebtoken
      const expirationDate = new Date();
      expirationDate.setHours(new Date().getHours() + 1);
      // Return jsonwebtoken
      const payload = {
        email,
        expirationDate,
      };

      jwt.sign(
        payload,
        process.env.JWT_TOKEN,
        { expiresIn: 360000 },
        async (err, token) => {
          if (err) throw err;
          
          const link = `${process.env.API}/api/user/account?token=${token}`;
          await sendEmail(link, name, "", email, emailTemplateSource);

          res.status(200).send("success");
        }
      );
    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server error");
    }
  }
};

export default handler;
