import Cors from "cors";
import initMiddleware from "../../../middleware/init-middleware";
import dbConnect from "../../../middleware/mongodb";
import Package from "../../../models/package";

// Initialize the cors middleware
const cors = initMiddleware(
  // You can read more about the available options here: https://github.com/expressjs/cors#configuration-options
  Cors({
    // Only allow requests with GET, POST and OPTIONS
    methods: ["GET", "POST", "OPTIONS"],
  })
);

const handler = async (req, res) => {
  //initialize database
  await dbConnect();
  //initialize cors
  await cors(req, res);

  // check request method type
  if (req.method === "POST") {
    // check if request body is empty
    if (!("authorization" in req.headers)) {
      res.statusCode = 401;
      res.end("Authorization header missing");
    }
    try {
      let { attachment, packageID, expressUserID } = req.body;

      console.log(packageID, attachment);

      // query packages such that package id is equal to packageID, and attachment description is equal to attachment description
      let updatedStepID = await Package.updateOne(
        {
          _id: packageID,
          attachments: { $elemMatch: { description: attachment.description } },
        },
        { $set: { "attachments.$.selected": true } }
      ).exec();

      let packages = await Package.find({ expressUserID }).exec();

      res.status(200).send(packages);
    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server error");
    }
  }
};

export default handler;
