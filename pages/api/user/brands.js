import Cors from "cors";

import initMiddleware from "../../../middleware/init-middleware";
import dbConnect from "../../../middleware/mongodb";
const jwt = require("jsonwebtoken");
import expressUser from "../../../models/expressUser";

import Package from "../../../models/package";

// Initialize the cors middleware
const cors = initMiddleware(
  // You can read more about the available options here: https://github.com/expressjs/cors#configuration-options
  Cors({
    // Only allow requests with GET, POST and OPTIONS
    methods: ["GET", "POST", "OPTIONS"],
  })
);

const handler = async (req, res) => {
  //initialize database
  await dbConnect();
  //initialize cors
  await cors(req, res);

  // check request method type
  if (req.method === "GET") {
    if (!("authorization" in req.headers)) {
      res.statusCode = 401;
      res.end("Authorization header missing");
    }

    try {
      const auth = await req.headers.authorization;
      const bearer = auth.split(" ");
      const token = bearer[1];
      let decoded = jwt.verify(token, process.env.JWT_TOKEN);
      const { email, expirationDate } = decoded;

      let user = await expressUser.findOne({ email });
      let userID = user._id;
      let brands = await Package.distinct("brandName", {
        expressUserID: userID,
      });
      res.status(200);

      res.send(brands);
    } catch (err) {
      console.log(err);
      res.status(403);
      res.send("Invalid auth credentials.");
      return;
    }
  }
};

export default handler;
