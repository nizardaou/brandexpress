import Cors from "cors";

import initMiddleware from "../../../middleware/init-middleware";
import dbConnect from "../../../middleware/mongodb";
const jwt = require("jsonwebtoken");
import expressUser from "../../../models/expressUser";
import moment from "moment";
import Feedback from "../../../models/feedback";
import Package from "../../../models/package";
import sendEmail from "../../../utils/sendEmail";
const path = require("path");
const fs = require("fs");
// import email template file
const emailTemplateSource = fs.readFileSync(
  path.join(process.cwd(), "/emails/feedback.hbs"),
  "utf8"
);

import { packageEnums } from "../../../constants/enum";

// Initialize the cors middleware
const cors = initMiddleware(
  // You can read more about the available options here: https://github.com/expressjs/cors#configuration-options
  Cors({
    // Only allow requests with GET, POST and OPTIONS
    methods: ["GET", "POST", "OPTIONS"],
  })
);

const handler = async (req, res) => {
  //initialize database
  await dbConnect();
  //initialize cors
  await cors(req, res);

  // check request method type
  if (req.method === "GET") {
    if (!("authorization" in req.headers)) {
      res.statusCode = 401;
      res.end("Authorization header missing");
    }

    try {
      const auth = await req.headers.authorization;
      const bearer = auth.split(" ");
      const token = bearer[1];
      let decoded = jwt.verify(token, process.env.JWT_TOKEN);
      const { email, expirationDate } = decoded;

      let user = await expressUser.findOne({ email });
      let userID = user._id;
      const find = {
        expressUserID: userID,
        packageID: req.query.packageID,
      };

      Feedback.find(find)
        .populate("expressUserID", ["name", "imageUrl"])
        .exec(function (err, results) {
          if (!results) {
            return res.status(400).json({ msg: "no feedbacks" });
          }

          res.send(results);
        });
    } catch (err) {
      console.log(err);
      res.status(403);
      res.send("Invalid auth credentials.");
      return;
    }
  }

  // check request method type
  if (req.method === "POST") {
    if (!("authorization" in req.headers)) {
      res.statusCode = 401;
      res.end("Authorization header missing");
    }
    try {
      // insert feedback
      let feedback = new Feedback(req.body.details);
      await feedback.save();

      Feedback.findById(feedback._id)
        .populate("expressUserID", ["name", "imageUrl", "_id"])
        .populate("packageID", ["brandName", "status"])
        .exec(async (err, feedback) => {
          if (!feedback) {
            return res.status(400).json({ msg: "no feedbacks" });
          }
          // get current package step
          let stepName = feedback.packageID.status;
          // get feedback id
          let expressUserID = feedback.expressUserID._id;

          if (feedback.userInput) {
            // create email body based on user button click on form
            const body =
              feedback.userInput.userAction === "submit"
                ? feedback.userInput.body
                : "User proceeded with no feedbacks on the direction he choosed";

            // send email to admin containing form details
            await sendEmail(
              "",
              "",
              feedback.packageID.brandName,
              "team@brandexpress.ae",
              emailTemplateSource,
              body
            );
          }

          // update status with feedback
          let packageID = feedback.packageID;
          let newStatus;

          // transform statuses enums into array
          let statusArray = Object.values(packageEnums.coreStatus);

          // get current status index
          let index = statusArray.findIndex((status) => status === stepName);

          // increment old status by one and set it to new status
          newStatus = statusArray[index + 1];

          // changed package status
          let update = { status: newStatus };
          await Package.updateOne(
            {
              _id: packageID,
            },
            { $set: update }
          ).exec();

          // query packages such that package id is equal to packageID, and steps array contains object of type equal to stepName
          const currentDate = new Date();
          let submittionDate = moment(currentDate).format("MMM DD");

          await Package.updateOne(
            {
              _id: packageID,
              steps: { $elemMatch: { type: newStatus } },
            },
            {
              $set: {
                "steps.$.status": true,
                "steps.$.date": submittionDate,
              },
            }
          ).exec();

          // get package list after updating
          let packagesList = await Package.find({ expressUserID }).exec();

          res.status(200).send({ feedback, packagesList });
        });
    } catch (err) {
      console.log(err);
      res.status(403);
      res.send("Invalid auth credentials.");
      return;
    }
  }
};

export default handler;
