import Cors from "cors";

import initMiddleware from "../../../middleware/init-middleware";
import dbConnect from "../../../middleware/mongodb";
const jwt = require("jsonwebtoken");

// Initialize the cors middleware
const cors = initMiddleware(
  // You can read more about the available options here: https://github.com/expressjs/cors#configuration-options
  Cors({
    // Only allow requests with GET, POST and OPTIONS
    methods: ["GET", "POST", "OPTIONS"],
  })
);

const handler = async (req, res) => {
  //initialize database
  await dbConnect();
  //initialize cors
  await cors(req, res);

  // check request method type
  if (req.method === "GET") {
    isAuthenticated(req, res);
  }
};

const isAuthenticated = (req, res) => {
  const token = req.query.token;
  if (!token) {
    res.status(403);
    res.send("Can't verify user.");
    return;
  }
  let decoded;
  try {
    decoded = jwt.verify(token, process.env.JWT_TOKEN);
  } catch (err) {
    console.log(err);
    res.status(403);
    res.send("Invalid auth credentials.");

    return;
  }
  if (
    !decoded.hasOwnProperty("email") ||
    !decoded.hasOwnProperty("expirationDate")
  ) {
    res.status(403);
    res.send("Invalid auth credentials.");
    return;
  }
  const { expirationDate } = decoded;
  if (expirationDate < new Date()) {
    res.status(403);
    res.send("Token has expired.");
    return;
  }

  res.status(200);
  res.redirect(`/dashboard?token=${token}`);
};

export default handler;
