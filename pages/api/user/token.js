import Cors from "cors";
import initMiddleware from "../../../middleware/init-middleware";
import expressUser from "../../../models/expressUser";
import dbConnect from "../../../middleware/mongodb";
let nodemailer = require("nodemailer");

const jwt = require("jsonwebtoken");

// Initialize the cors middleware
const cors = initMiddleware(
  // You can read more about the available options here: https://github.com/expressjs/cors#configuration-options
  Cors({
    // Only allow requests with GET, POST and OPTIONS
    methods: ["GET", "POST", "OPTIONS"],
  })
);

const handler = async (req, res) => {
  //initialize database
  await dbConnect();
  //initialize cors
  await cors(req, res);

  // check request method type
  if (req.method === "POST") {
    // check if request body is empty
    if (!req.body) {
      res.status(400);
      res.send("Request body is empty");
      return;
    }

    try {
      let userID = req.body.userID;
      // See if user exists
      let user = await expressUser.findById(userID);

      if (!user) {
        res.status(400).send("invalid credentials");
        return;
      }

      let email = user.email;

      // Return jsonwebtoken
      const expirationDate = new Date();
      expirationDate.setHours(new Date().getHours() + 1);
      // Return jsonwebtoken
      const payload = {
        email,
        expirationDate,
      };

      jwt.sign(
        payload,
        process.env.JWT_TOKEN,
        { expiresIn: 360000 },
        async (err, token) => {
          if (err) throw err;

          res.status(200).send(token);
        }
      );
    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server error");
    }
  }
};

export default handler;
