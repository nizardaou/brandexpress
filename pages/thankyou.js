import Layout from "../components/Layouts/Layout";

const thankyou = () => (
  <Layout>
    <div className="flex w-5/6 lg:max-w-7xl mx-auto items-center lg:items-start my-10">
      <div className="text-gray-500 w-3/4">
        <h1 className="text-black text-4xl sm:text-5xl  lg:text-6xl font-semibold">
          <span className="text-BE-purple"> Awesome!</span>
        </h1>
        <p className="my-10">
          We’ve got everything we need to get started on turning your vision
          into reality. Sit back, relax, and be on the lookout for updates
          through email.
        </p>
        <p>
          Didn't receive an email? Please let us know by{" "}
          <a
            className="text-base text-BE-purple font-medium"
            href="mailto:team@brandexpress.ae"
          >
            contacting our support team.
          </a>
        </p>
      </div>
    </div>
  </Layout>
);

export default thankyou;
