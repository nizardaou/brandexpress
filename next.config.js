module.exports = {
  async redirects() {
    return [
      {
        source: "/amp",
        destination: "/",
        permanent: false,
      },
    ];
  },
};
