## Running NEXT JS Project

1.  Create your project using npx create-next-app EXPRESS-SPRKL

1.  Navigate into the site’s directory and start it up.

    `npm run dev`

1.  open the source code and start editing

    Your site is now running at http://localhost:3000

    If you want to edit: open the EXPRESS-SPRKL directory in your code editor of choice and edit /pages/index.js. Save your changes and the browser will update in real time!

1.  Install tailwind css with postcss and autoprefixer:

    `npm install -D tailwindcss@latest postcss@latest autoprefixer@latest`

    then create tailwind.config.js and postcss.config.js with default config variables

1.  Install Headless UI library for React:

    `npm install @headlessui/react`

1.  Install tailwind forms extension:

    `npm install @@tailwindcss/forms`

1.  install react-responsive-carousel:

    `npm install react-responsive-carousel`
