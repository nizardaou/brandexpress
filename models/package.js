const mongoose = require("mongoose");
import { packageEnums } from "../constants/enum";

const packageSchema = new mongoose.Schema({
  expressUserID: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "expressUser",
  },
  name: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  brandName: {
    type: String,
    required: true,
  },
  wizardResponseData: { type: mongoose.Schema.Types.Mixed },
  status: {
    type: String,
    enum: Object.values(packageEnums.coreStatus).concat(
      Object.values(packageEnums.addonStatus)
    ),
    required: true,
  },
  type: {
    type: String,
    enum: ["CORE", "ADDON"],
  },
  steps: { type: Array, default: [] },
  attachments: { type: Array, default: [] },
  comments: { type: Array, default: [] },
  lastUpdate: {
    type: Date,
    default: Date.now,
  },
  date: {
    type: Date,
    default: Date.now,
  },
});

export default mongoose.models.Package ||
  mongoose.model("Package", packageSchema);
