const mongoose = require("mongoose");
import { paymentEnums } from "../constants/enum";

const paymentSchema = new mongoose.Schema({
  expressUserID: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "expressUser",
  },
  packagesID: [{ type: mongoose.Schema.Types.ObjectId, ref: "Package" }],
  status: {
    type: String,
    enum: Object.values(paymentEnums.status),
    required: true,
  },
  amount: { type: Number },
  date: {
    type: Date,
    default: Date.now,
  },
});

export default mongoose.models.Payment ||
  mongoose.model("Payment", paymentSchema);
