const mongoose = require("mongoose");

const feedbackSchema = new mongoose.Schema({
  expressUserID: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "expressUser",
  },
  packageID: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Package",
  },
  input: { type: Boolean, default: false },
  body: { type: String },
  orientation: { type: String, enum: ["right", "left"] },
  formButtons: { type: Array, default: [] },
  actionButtons: { type: Array, default: [] },
  userInput: { type: mongoose.Schema.Types.Mixed },
  date: {
    type: Date,
    default: Date.now,
  },
});

export default mongoose.models.Feedback ||
  mongoose.model("Feedback", feedbackSchema);
