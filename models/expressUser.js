const mongoose = require("mongoose");
import { userEnums } from "../constants/enum";

const expressUserSchema = new mongoose.Schema({
  name: {
    type: String,
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  pendingEmail: {
    type: String,
    default: "",
  },
  phone: {
    type: String,
    unique: true,
  },
  imageUrl: {
    type: String,
  },
  address: {
    type: String,
  },
  timezone: {
    type: String,
  },
  status: {
    type: String,
    enum: Object.values(userEnums.status),
    required: true,
  },
  date: {
    type: Date,
    default: Date.now,
  },
});

export default mongoose.models.expressUser ||
  mongoose.model("expressUser", expressUserSchema);
